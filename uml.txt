@startuml
entity "State" as state{
    *state_abbrev = String
    *total_pop = Integer
    *white_pop = Integer
    *hispanic_pop = Integer
    *asian_pop = Integer
    *black_pop = Integer
    *male_pop = Integer
    *female_pop = Integer
    *non_white_percentage = Float
    *state_compactness = Float
    *state_name = String
    *party_detailed = String
    *candidatevotes = Integer
    *totalvotes = Integer
    *MOV = Float
    *sq_miles = Integer
    *census_region = String
    *princeton_report = String
}

entity "Politician" as politician{
    *name = String
    *state_abbrev = String
    *district_num = Integer  
    *party_detailed = String 
    *age = Integer
    *years_in_office = Integer
    *phone_number = String
    *total_cash = Integer   
    *twitter_id = String
    *committees = String
    *first_elected =  String
    *missed_votes_pct = Float
    *facebook_id = String
    *bioguide_id = String
    *contact_site = String
    *youtube_id = String
    *votes_with_party_pct = Float
    *votesmart_id = String
    *birthdate = String
    *website = String
    *photo_url = String
    *gender = String
    *feccandid = String
    *office_addr = String
}

entity "District" as district{
    *state_abbrev = String
    *district_num = Integer
    *total_pop = Integer    
    *white_pop = Integer    
    *hispanic_pop = Integer
    *asian_pop = Integer
    *black_pop = Integer
    *male_pop = Integer
    *female_pop = Integer
    *non_white_percentage = Float
    *district_compactness = Float    
    *representative = String 
    *party_detailed = String
    *candidatevotes = Integer    
    *totalvotes = Integer
    *MOV = Float
    *sq_miles = Integer
}

left to right direction
politician "1" <--> "1" district
district "1..n" --> "1" state
politician "1..n" --o "1" state
@enduml