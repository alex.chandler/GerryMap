from GUI_Protocol_Driver import *
import random

# from unittest import TestCase
random.seed(30)
# Note to grader: Details of the tests are hidden in the GUI_Protocol_Driver
# Tests in guitests.py were made in an english like fashion to test functionality of website
import unittest


class Test_GUI(unittest.TestCase):
    def __init__(self, lower_API_count=True):
        self.lower_API_count = lower_API_count
        self.GPD = GUI_Protocol_Driver()

    # Feature Check: I can click on Fields in Navigation Menu
    def go_to_all_pages_in_nav_menu(self):
        for nav_page in self.GPD.nav_pages:
            self.GPD.go_to_page(nav_page)
        self.GPD.go_to_page("Home")

    def go_to_nav_pages_from_multiple_pages(self):
        list_nav_pages_shuffled = []
        for i in range(10):
            list_nav_pages_shuffled += random.sample(
                self.GPD.nav_pages, len(self.GPD.nav_pages)
            )
        for nav_page in list_nav_pages_shuffled:
            self.GPD.go_to_page(nav_page)
        self.GPD.go_to_page("Home")

    def check_for_devs(self, desired_devs):
        self.GPD.go_to_page("About")
        self.GPD.verify_existence_devs(desired_devs)

    def go_states(self):
        state_list = state_to_abbrev.keys()
        if self.lower_API_count:
            state_list = ["Alaska", "Arkansas", "Alabama"]
        elif self.lower_API_count:
            state_list = random.sample(state_list, 3)
        for state_name in state_list:
            self.GPD.go_to_page("States")
            found_state = self.GPD.click_state(state_name)
            if not found_state:
                raise Exception("Unable to find state: %s", state_name)
            self.GPD.go_to_page("States")

    # User Story: I can click on Districts from the District Page
    def go_districts(self):
        self.GPD.go_to_page("Districts")
        state_district_pairs = self.GPD.get_visible_districts()
        if self.lower_API_count:
            state_district_pairs = random.sample(state_district_pairs, 3)
        for state, district_num in state_district_pairs:
            found_district = self.GPD.click_district(state, district_num)
            if not found_district:
                raise Exception(
                    "Unable to find district %s for state %s" % (district_num, state)
                )
            self.GPD.go_to_page("Districts")

    def go_politicians(self):
        self.GPD.go_to_page("Politicians")
        politicians = self.GPD.get_visible_politicians()
        if self.lower_API_count:
            politicians = random.sample(politicians, 3)
        for politician_name in politicians:
            self.GPD.click_politician(politician_name)
            self.GPD.go_to_page("Politicians")

    def check_hyperlinks_on_about_page(self):
        self.GPD.go_to_page("About")
        self.GPD.go_documentation()
        self.GPD.go_to_url(self.GPD.base_url)
        self.GPD.go_to_page("About")
        self.GPD.go_to_gitlab()
        self.GPD.go_to_url(self.GPD.base_url)

    def check_state_tabs(self, desired_filters):
        self.GPD.go_to_page("States")
        found_filters = self.GPD.get_filter_names()
        for desired_filter in desired_filters:
            if desired_filter not in found_filters:
                raise ValueError("Missing desired filter: %s" % desired_filter)

    def check_district_tabs(self, desired_filters):
        self.GPD.go_to_page("Districts")
        found_filters = self.GPD.get_filter_names()
        for desired_filter in desired_filters:
            if desired_filter not in found_filters:
                raise ValueError("Missing desired filter: %s" % desired_filter)

    def check_politician_tabs(self, desired_filters):
        self.GPD.go_to_page("Politicians")
        found_filters = self.GPD.get_filter_names()
        for desired_filter in desired_filters:
            if desired_filter not in found_filters:
                raise ValueError("Missing desired filter: %s" % desired_filter)

    def check_splash_page(self):
        self.GPD.go_splash_page()
        # verify learn more link goes to about page
        self.GPD.click_learn_more_page()
        # check states, districts, and politician hyperlinks
        self.GPD.go_splash_page()
        self.GPD.click_states_page()
        self.GPD.go_splash_page()
        self.GPD.click_districts_page()
        self.GPD.go_splash_page()
        self.GPD.click_politicians_page()
        self.GPD.go_splash_page()

    def run_all_tests(self):
        # Test 1
        self.check_splash_page()
        # Test 2
        self.go_to_all_pages_in_nav_menu()
        # Test 3
        self.go_to_nav_pages_from_multiple_pages()
        # Test 4
        self.go_states()
        # Test 5
        self.go_districts()
        # Test 6
        self.go_politicians()
        # Test 7
        self.check_hyperlinks_on_about_page()
        # Test 8
        self.check_state_tabs(["Search", "Filter By", "Sort By"])
        # Test 9
        self.check_district_tabs(["Filter By", "Sort By", "Search"])
        # Test 10
        self.check_politician_tabs(["Filter By", "Sort By", "Search"])
        # Test 11
        self.check_for_devs(
            [
                "Helen Fang",
                "Alex L Chandler",
                "Joann Feng",
                "Akshay Sharma",
                "Ting-Yin Chang Chien",
            ]
        )


acceptance_tests = Test_GUI()
acceptance_tests.run_all_tests()
