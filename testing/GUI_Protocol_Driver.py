import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)
import pandas as pd
from selenium import webdriver
from backend.utils.state_abbreviations import state_to_abbrev
from selenium.webdriver.chrome.options import Options
import time

RUN_LOCAL = False
# Translate Language of Problem Domain to Interactions with SUT
class GUI_Protocol_Driver:
    def __init__(self):
        self.chromedriver_path = "/testing/chromedriver"
        # self.base_url = "http://gerrymap.com"
        self.base_url = "https://dev.gerrymap.com"
        self.start_browser()
        self.x_path_dict = self.create_xpath_dict()
        self.nav_pages = ["Home", "About", "States", "Districts", "Politicians", "FAQ"]
        self.driver.implicitly_wait(5)  # seconds
        self.politician_df = self.access_politican_df(
            "backend/created_data/data_for_backend_server/politician_df.csv"
        )

    def access_politican_df(self, rel_path_pol_df):
        politician_df = pd.read_csv(rel_path_pol_df)
        return politician_df

    def filter_states_by(self, desired_filter_specifications):
        all_filter_specifications = {
            "political_leaning": None,
            "pop": (None, None),
            "size": (None, None),
            "minority_perc": (None, None),
            "avg_compactness": (None, None),
        }
        # click filter_by button
        desired_x_path = "//button[@id='dropdown-filter-button']"
        filter_elem = self.get_element_by_x_path(desired_x_path)
        filter_elem.click()
        dem_check_box_xpath = "//label[normalize-space()='Democrat']"
        rep_check_box_xpath = "//label[normalize-space()='Republican']"
        for key in desired_filter_specifications.keys():
            all_filter_specifications[key] = desired_filter_specifications[key]
        for key, val in all_filter_specifications.items():
            # click only
            if key == "political_leaning":
                if val == "Democrat":
                    elem = self.get_element_by_x_path(dem_check_box_xpath)
                elif val == "Republican":
                    elem = self.get_element_by_x_path(rep_check_box_xpath)
                elem.click()
            if key == "pop":
                if val[0] != None:
                    x_path = "//a[3]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[3]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[3]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "size":
                if val[0] != None:
                    x_path = "//a[4]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[4]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[4]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "minority_perc":
                if val[0] != None:
                    x_path = "//a[5]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[5]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[6]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "avg_compactness":
                if val[0] != None:
                    x_path = "//a[6]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[6]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[6]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
        # click elsewhere
        xpath_top = "//h2[normalize-space()='States']"
        self.click_element_by_x_path(xpath_top)

    def click_reverse_order_button(self):
        x_path = "//button[@class='btn btn-outline-secondary']//*[name()='svg']"
        self.click_element_by_x_path(x_path)

    def filter_politicians_by(self, desired_filter_specifications):
        all_filter_specifications = {
            "political_leaning": None,
            "age": (None, None),
            "gender": (None, None),
            "years_in_office": (None, None),
            "missed_vote_percentage": (None, None),
            "votes_with_party_percentage": (None, None),
        }
        # click filter_by button
        desired_x_path = "//button[@id='dropdown-filter-button']"
        filter_elem = self.get_element_by_x_path(desired_x_path)
        filter_elem.click()
        dem_check_box_xpath = "//label[normalize-space()='Democrat']"
        rep_check_box_xpath = "//label[normalize-space()='Republican']"
        for key in desired_filter_specifications.keys():
            all_filter_specifications[key] = desired_filter_specifications[key]
        for key, val in all_filter_specifications.items():
            time.sleep(1)
            # click only
            if key == "political_leaning":
                if val == "Democrat":
                    elem = self.get_element_by_x_path(dem_check_box_xpath)
                elif val == "Republican":
                    elem = self.get_element_by_x_path(rep_check_box_xpath)
                elem.click()
            if key == "age":
                time.sleep(1)
                if val[0] != None:
                    x_path = "//a[3]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[3]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[3]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "gender":
                male_box_xpath = "//a[4]//form[1]//div[1]//input[1]"
                female_box_xpath = "//a[5]//form[1]//div[1]//input[1]"
                if val == "Male":
                    elem = self.get_element_by_x_path(male_box_xpath)
                elif val == "Female":
                    elem = self.get_element_by_x_path(female_box_xpath)
                elem.click()
            if key == "years_in_office":
                if val[0] != None:
                    x_path = "//a[6]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[6]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[6]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "missed_vote_percentage":
                if val[0] != None:
                    x_path = "//a[7]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[7]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[7]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "votes_with_party_percentage":
                if val[0] != None:
                    x_path = "//a[8]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[8]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[8]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
        # click elsewhere
        xpath_top = "//h2[normalize-space()='Politicians']"
        self.click_element_by_x_path(xpath_top)

    def filter_districts_by(self, desired_filter_specifications):
        all_filter_specifications = {
            "political_leaning": None,
            "pop": (None, None),
            "size": (None, None),
            "minority_perc": (None, None),
        }
        # click filter_by button
        desired_x_path = "//button[@id='dropdown-filter-button']"
        filter_elem = self.get_element_by_x_path(desired_x_path)
        filter_elem.click()
        dem_check_box_xpath = "//label[normalize-space()='Democrat']"
        rep_check_box_xpath = "//label[normalize-space()='Republican']"
        for key in desired_filter_specifications.keys():
            all_filter_specifications[key] = desired_filter_specifications[key]
        for key, val in all_filter_specifications.items():
            time.sleep(1)
            # click only
            if key == "political_leaning":
                if val == "Democrat":
                    elem = self.get_element_by_x_path(dem_check_box_xpath)
                elif val == "Republican":
                    elem = self.get_element_by_x_path(rep_check_box_xpath)
                elem.click()
            if key == "pop":
                if val == (0, 600000):
                    x_path = "//a[3]//form[1]//div[1]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                elif val == (600001, 900000):
                    x_path = "//a[4]//form[1]//div[1]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                elif val == (900001, 1100000):
                    x_path = "//input[@id='2']"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                else:
                    if val[0] != None:
                        x_path = "//a[6]//form[1]//div[1]//div[2]//input[1]"
                        elem = self.get_element_by_x_path(x_path)
                        elem.click()
                        elem.send_keys(str(val[0]))
                    if val[1] != None:
                        x_path = "//a[6]//form[1]//div[1]//div[3]//input[1]"
                        elem = self.get_element_by_x_path(x_path)
                        elem.click()
                        elem.send_keys(str(val[1]))
                    # click checkbox
                    if val[0] or val[1]:
                        time.sleep(1)
                        x_path_check_box = "//a[6]//form[1]//div[1]//div[3]//input[1]"
                        check_box = self.get_element_by_x_path(x_path_check_box)
                        check_box.click()
            if key == "size":
                if val[0] != None:
                    x_path = "//a[7]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[7]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = "//a[7]//form[1]//div[1]//div[3]//input[1]"
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "minority_perc":
                if val[0] != None:
                    x_path = "//a[8]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[8]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[8]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
            if key == "avg_compactness":
                if val[0] != None:
                    x_path = "//a[9]//form[1]//div[1]//div[2]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[0]))
                if val[1] != None:
                    x_path = "//a[9]//form[1]//div[1]//div[3]//input[1]"
                    elem = self.get_element_by_x_path(x_path)
                    elem.click()
                    elem.send_keys(str(val[1]))
                # click checkbox
                if val[0] or val[1]:
                    time.sleep(1)
                    x_path_check_box = (
                        "//a[9]//form[1]//div[1]//div[1]//div[1]//input[1]"
                    )
                    check_box = self.get_element_by_x_path(x_path_check_box)
                    check_box.click()
        # click elsewhere
        xpath_top = "//h2[normalize-space()='Congressional Districts']"
        self.click_element_by_x_path(xpath_top)

    def click_element_by_x_path(self, x_path):
        elem = self.get_element_by_x_path(x_path)
        elem.click()

    def sort_states_by(self, desired_sort=None):
        if desired_sort:
            x_path_sort_by = "//button[@id='dropdown-sort-button']"
            sortby_elem = self.get_element_by_x_path(x_path_sort_by)
            sortby_elem.click()
            x_path = "//a[normalize-space()='{}']".format(desired_sort)
            self.click_element_by_x_path(x_path)
            xpath_top = "//h2[normalize-space()='States']"
            self.click_element_by_x_path(xpath_top)

    def sort_districts_by(self, desired_sort=None):
        if desired_sort:
            x_path_sort_by = "//button[@id='dropdown-sort-button']"
            sortby_elem = self.get_element_by_x_path(x_path_sort_by)
            sortby_elem.click()
            x_path = "//a[normalize-space()='{}']".format(desired_sort)
            self.click_element_by_x_path(x_path)
            xpath_top = "//h2[normalize-space()='Congressional Districts']"
            self.click_element_by_x_path(xpath_top)

    def sort_politicians_by(self, desired_sort=None):
        if desired_sort:
            x_path_sort_by = "//button[@id='dropdown-sort-button']"
            sortby_elem = self.get_element_by_x_path(x_path_sort_by)
            sortby_elem.click()
            x_path = "//a[normalize-space()='{}']".format(desired_sort)
            self.click_element_by_x_path(x_path)
            xpath_top = "//h2[normalize-space()='Politicians']"
            self.click_element_by_x_path(xpath_top)

    def start_browser(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        # Only used locally
        if RUN_LOCAL:
            self.driver = webdriver.Chrome()
        else:
            self.driver = webdriver.Chrome(
                os.getcwd() + self.chromedriver_path, options=chrome_options
            )
        self.driver.get(self.base_url)

    def check_for_instance(self, name):

        x_path = "//span[normalize-space()='{}']".format(name)
        # x_path_2 = "//*[contains(text(),\'{}\')]".format(name)
        elem = self.driver.find_element_by_xpath(x_path)
        if not elem:
            raise ValueError("Cannot find {}".format(elem))

    def create_xpath_dict(self):
        x_path_dict = {}
        x_path_dict["Home"] = "//a[normalize-space()='Home']"
        x_path_dict["About"] = "//a[normalize-space()='About']"
        x_path_dict["States"] = "//a[normalize-space()='States']"
        x_path_dict["Districts"] = "//a[normalize-space()='Districts']"
        x_path_dict["Politicians"] = "//a[normalize-space()='Politicians']"
        x_path_dict["FAQ"] = "//a[normalize-space()='FAQ']"
        x_path_dict["Click here"] = "//button[normalize-space()='Click here']"
        # Can only be found from about page
        x_path_dict["GerryMap GitLab"] = "//a[normalize-space()='GerryMap GitLab']"
        return x_path_dict

    def get_element_by_name(self, name):
        x_path = self.x_path_dict[name]
        element = self.driver.find_element_by_xpath(x_path)
        return element

    def get_element_by_x_path(self, x_path):
        element = self.driver.find_element_by_xpath(x_path)
        return element

    def quit_browser(self):
        self.driver.quit()

    def go_documentation(self):
        self.verify_url("{}/about".format(self.base_url))
        documentation_url = "https://documenter.getpostman.com/view/19780402/UVkqsvRV"
        elem = self.get_element_by_x_path(
            "//a//*[normalize-space()='See Postman Documentation']/parent::a"
        )
        elem_url = elem.get_attribute("href")
        self.driver.get(elem_url)
        self.verify_url(documentation_url)

    def click_learn_more_page(self):
        x_path = "//a[normalize-space()='Learn More']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        self.verify_url("{}/about".format(self.base_url))

    def go_splash_page(self):
        self.driver.get(self.base_url)

    def click_states_page(self):
        x_path = "//h5[normalize-space()='States']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        desired_url = self.base_url + "/states"
        self.verify_url(desired_url)

    def click_districts_page(self):
        x_path = "//h5[normalize-space()='Districts']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        desired_url = self.base_url + "/districts"
        self.verify_url(desired_url)

    def click_politicians_page(self):
        x_path = "//h5[normalize-space()='Politicians']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        desired_url = self.base_url + "/politicians"
        self.verify_url(desired_url)

    def go_to_gitlab(self):
        self.verify_url("{}/about".format(self.base_url))
        gitlab_url = "https://gitlab.com/alex.chandler/GerryMap"
        elem = self.get_element_by_x_path("//a//*[normalize-space()='See GitLab Page']/parent::a")
        elem_url = elem.get_attribute("href")
        self.driver.get(elem_url)
        self.verify_url(gitlab_url)

    def go_to_url(self, url):
        time.sleep(1)
        self.driver.get(url)

    def click_element(self, element):
        attempt_cnt = 0
        while attempt_cnt < 5:
            attempt_cnt += 1
            try:
                resp = element.click()
                if resp != None:
                    break
            except Exception as e:
                time.sleep(2)

    def verify_existence_devs(self, desired_devs):
        CSS_selector = ".h5"
        found_devs = []
        dev_elem = self.driver.find_elements_by_css_selector(CSS_selector)
        for dev in dev_elem:
            found_devs.append(dev.text)
        for desired_dev in desired_devs:
            if desired_dev not in found_devs:
                raise ValueError("Could Not Locate Developer: %s" % desired_dev)

    def get_filter_names(self):
        buttons_xpath = "//button"
        button_elems = self.driver.find_elements_by_xpath(buttons_xpath)
        filter_names = []
        for button_elem in button_elems:
            filter_names.append(button_elem.text)
        return filter_names

    def go_to_page(self, page_name):
        self.go_to_page_by_clicking_text(page_name)
        time.sleep(3)

    def go_to_page_by_clicking_text(self, visible_text):
        element = self.get_element_by_name(visible_text)
        self.click_element(element)

    def click_state(self, state_name):
        desired_url = self.base_url + "/states"
        self.verify_url(desired_url)
        state_name_upper = state_name.upper()
        x_path_state = "//span[normalize-space()='{}']".format(state_name_upper)
        x_path_state_2 = "//a[@href='/states/{}']//div[@class='card']//div[@class='card-body']//div[@class='card-title h5']".format(
            state_to_abbrev[state_name]
        )
        attempt_cnt = 0
        found_state = False
        while not found_state and attempt_cnt < 10:
            try:
                time.sleep(3)
                state_element = self.get_element_by_x_path(x_path_state)
                self.click_element(state_element)
                found_state = True
                time.sleep(3)
            # If element is not found, go to the next page due to pagination
            except Exception as e1:
                try:
                    time.sleep(3)
                    state_element = self.get_element_by_x_path(x_path_state_2)
                    self.click_element(state_element)
                    found_state = True
                    time.sleep(3)
                except Exception as e2:
                    x_path_next = "//span[contains(text(),'›')]"
                    next_button_element = self.get_element_by_x_path(x_path_next)
                    self.click_element(next_button_element)
                    attempt_cnt += 1
        if not found_state:
            return False
        state_abbrev = state_to_abbrev[state_name]
        predicted_url = self.predict_state_url(state_abbrev)
        self.verify_url(predicted_url)
        return found_state

    # Returns a list of all state district pairsthat are visible on the states page
    def get_visible_districts(self):
        desired_url = self.base_url + "/districts"
        self.verify_url(desired_url)
        resp = self.driver.find_elements_by_class_name("card-title")
        state_district_pairs = []
        for elem in resp:
            state, district, district_num = elem.text.split(" ")
            state_district_pairs.append((state, district_num))
        return state_district_pairs

    def get_visible_politicians(self):
        resp = self.driver.find_elements_by_class_name("card-title")
        politicians = []
        attempt_cnt = 0
        while len(politicians) == 0 and attempt_cnt < 5:
            resp = self.driver.find_elements_by_class_name("card-title")
            for elem in resp:
                politicians.append(elem.text)
            attempt_cnt += 1
            time.sleep(1)
        return politicians

    def click_district(self, state, district_num):
        x_path_district_precise = "//span[normalize-space()='{} District {}']".format(
            state, str(district_num)
        )
        x_path_district = "//div[contains(text(),'{} District {}')]".format(
            state, str(district_num)
        )
        attempt_cnt = 0
        found_district = False
        while not found_district and attempt_cnt < 300:
            time.sleep(3)
            try:
                district_element = self.get_element_by_x_path(x_path_district)
                self.click_element(district_element)
                found_district = True
            # If element is not found, go to the next page due to paginationx
            except Exception as e:
                try:
                    district_element = self.get_element_by_x_path(
                        x_path_district_precise
                    )
                    self.click_element(district_element)
                    found_district = True
                except Exception as e1:
                    if attempt_cnt % 3 == 0:
                        x_path_next = "//span[contains(text(),'›')]"
                        next_button_element = self.get_element_by_x_path(x_path_next)
                        self.click_element(next_button_element)
                        attempt_cnt += 1
        state_abbrev = state_to_abbrev[state]
        predicted_url = self.predict_district_url(state_abbrev, district_num)
        self.verify_url(predicted_url)
        return found_district

    def enter_into_splash_search_bar(self, text):
        x_path = "//input[@aria-label='input-search-by-address']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        elem.send_keys(text)
        search_xpath = "//button[@id='button-addon1']"
        search_elem = self.get_element_by_x_path(search_xpath)
        self.click_element(search_elem)
        time.sleep(2)

    def enter_into_model_search_bar(self, text):
        x_path = "//input[@aria-label='search-bar']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        elem.send_keys(text)
        search_xpath = "//button[normalize-space()='Search']"
        search_elem = self.get_element_by_x_path(search_xpath)
        self.click_element(search_elem)
        time.sleep(2)

    def enter_into_top_right_search_bar(self, text):
        x_path = "//input[@placeholder='Search']"
        elem = self.get_element_by_x_path(x_path)
        self.click_element(elem)
        elem.send_keys(text)
        search_xpath = "//a[@id='button-addon1']"
        search_elem = self.get_element_by_x_path(search_xpath)
        self.click_element(search_elem)
        time.sleep(2)

    def click_politician(self, name):
        # x_path_politician = "//div[contains(text(),'{}')]".format(name)
        x_path_politician = "//span[normalize-space()='{}']".format(name)
        politician_element = self.get_element_by_x_path(x_path_politician)
        self.click_element(politician_element)
        predicted_url = self.predict_politician_url(name)
        self.verify_url(predicted_url)

    def predict_state_url(self, state_abbrev):
        predicted_url = "{}/states/{}".format(self.base_url, state_abbrev)
        return predicted_url

    def predict_district_url(self, state_abbrev, district_num):
        if district_num == "At-large":
            district_num = "0"
        predicted_url = "{}/districts/{}_{}".format(
            self.base_url, state_abbrev, str(district_num)
        )
        return predicted_url

    def predict_politician_url(self, politician_name):
        corresponding_row = self.politician_df[
            self.politician_df["name"] == politician_name
        ]
        corresponding_row_dict = corresponding_row.to_dict()
        state = list(corresponding_row_dict["state"].values())[0]
        district = list(corresponding_row_dict["district"].values())[0]
        if district == "At-large":
            district = "0"
        ending = state + "_" + str(district)
        return "{}/politicians/{}".format(self.base_url, ending)

    """
    Function that confirms that the driver is on the correct URL.
    """

    def verify_url(self, desired_url):
        attempt_cnt = 0
        verified_url = False
        while not verified_url and attempt_cnt < 5:
            actual_url = self.driver.current_url
            if actual_url != desired_url:
                attempt_cnt += 1
                time.sleep(1)
            else:
                verified_url = True
        return verified_url
