# Decorator function to see whether or not a test fails
def test_function(f):
    def test(self, *args):
        self.report["expected_passed"] += 1
        try:
            f(*args)
            print("Test %s Passed" % str(f.__name__))
            self.report["num_passes"] += 1
            return True
        except Exception as e:
            # Verify the error
            try:
                f(*args)
                print("Test %s Passed" % str(f.__name__))
                self.report["num_passes"] += 1
                return True
            except Exception as e:
                print(
                    "Test {} Failed due to exception {}".format(str(f.__name__), str(e))
                )
                return False

    return test


# A test framework that allows all tests to be run and reporting statistics
class alexTestFramework:
    def report_tests(self):
        print("Finished Running Tests\n")
        num_passes = self.report["num_passes"]
        expected_passed = self.report["expected_passed"]
        num_failed = expected_passed - num_passes
        print("TEST REPORT")
        if num_failed == 0:
            print("SUCCESS")
            print("{} of {} tests Passed".format(num_passes, expected_passed))
            return True
        else:
            print("{} of {} tests Passed".format(num_passes, expected_passed))
            raise ValueError(
                "ERROR: {} of {} tests Failed".format(num_failed, expected_passed)
            )

    def run_all_tests(self):
        print("Beginning GUI Tests\n")
        public_method_names = [
            method
            for method in dir(self)
            if callable(getattr(self, method))
            if not method.startswith("_")
        ]
        for method in public_method_names:
            if method.startswith("test") or method.endswith("test"):
                getattr(self, method)(self)
