from GUI_Protocol_Driver import *
from alexTestFramework import *
import random

# from unittest import TestCase
random.seed(30)
# Note to grader: Details of the tests are hidden in the GUI_Protocol_Driver
# Tests in guitests.py were made in an english like fashion to test functionality of website


class GUITests(alexTestFramework):
    def __init__(self):
        super(GUITests, self).__init__()
        self.report = {"expected_passed": 0, "num_passes": 0}
        self.GPD = GUI_Protocol_Driver()
        self.lower_API_count = True

    # Feature Check: I can click on Fields in Navigation Menu
    @test_function
    def test_go_to_all_pages_in_nav_menu(self):
        for nav_page in self.GPD.nav_pages:
            self.GPD.go_to_page(nav_page)
        self.GPD.go_to_page("Home")

    @test_function
    def test_go_to_nav_pages_from_multiple_pages(self):
        list_nav_pages_shuffled = []
        for i in range(10):
            list_nav_pages_shuffled += random.sample(
                self.GPD.nav_pages, len(self.GPD.nav_pages)
            )
        for nav_page in list_nav_pages_shuffled:
            self.GPD.go_to_page(nav_page)
        self.GPD.go_to_page("Home")

    @test_function
    def test_check_for_devs(self):
        desired_devs = [
            "Helen Fang",
            "Alex L Chandler",
            "Joann Feng",
            "Akshay Sharma",
            "Ting-Yin Chang Chien",
        ]
        self.GPD.go_to_page("About")
        self.GPD.verify_existence_devs(desired_devs)

    @test_function
    def test_go_states(self):
        state_list = state_to_abbrev.keys()
        if self.lower_API_count:
            state_list = ["Alaska", "Arkansas", "Alabama"]
        elif self.lower_API_count:
            state_list = random.sample(state_list, 3)
        for state_name in state_list:
            self.GPD.go_to_page("States")
            found_state = self.GPD.click_state(state_name)
            if not found_state:
                raise Exception("Unable to find state: %s", state_name)
            self.GPD.go_to_page("States")

    @test_function
    # User Story: I can click on Districts from the District Page
    def test_go_districts(self):
        self.GPD.go_to_page("Districts")
        state_district_pairs = self.GPD.get_visible_districts()
        if self.lower_API_count:
            state_district_pairs = random.sample(state_district_pairs, 3)
        for state, district_num in state_district_pairs:
            found_district = self.GPD.click_district(state, district_num)
            if not found_district:
                raise Exception(
                    "Unable to find district %s for state %s" % (district_num, state)
                )
            self.GPD.go_to_page("Districts")

    @test_function
    def test_go_politicians(self):
        self.GPD.go_to_page("Politicians")
        politicians = self.GPD.get_visible_politicians()
        if not politicians:
            self.GPD.go_to_page("Politicians")
        if self.lower_API_count:
            politicians = random.sample(politicians, 3)
        for politician_name in politicians:
            self.GPD.click_politician(politician_name)
            self.GPD.go_to_page("Politicians")

    @test_function
    def test_check_hyperlinks_on_about_page(self):
        self.GPD.go_to_page("About")
        self.GPD.go_documentation()
        self.GPD.go_to_url(self.GPD.base_url)
        self.GPD.go_to_page("About")
        self.GPD.go_to_gitlab()
        self.GPD.go_to_url(self.GPD.base_url)

    @test_function
    def test_check_state_tabs(self):
        desired_filters = ["Search", "Filter By", "Sort By"]
        self.GPD.go_to_page("States")
        found_filters = self.GPD.get_filter_names()
        for desired_filter in desired_filters:
            if desired_filter not in found_filters:
                raise ValueError("Missing desired filter: %s" % desired_filter)

    @test_function
    def test_check_district_tabs(self):
        desired_filters = ["Filter By", "Sort By", "Search"]
        self.GPD.go_to_page("Districts")
        found_filters = self.GPD.get_filter_names()
        for desired_filter in desired_filters:
            if desired_filter not in found_filters:
                raise ValueError("Missing desired filter: %s" % desired_filter)

    @test_function
    def test_check_politician_tabs(self):
        desired_filters = ["Filter By", "Sort By", "Search"]
        self.GPD.go_to_page("Politicians")
        found_filters = self.GPD.get_filter_names()
        for desired_filter in desired_filters:
            if desired_filter not in found_filters:
                raise ValueError("Missing desired filter: %s" % desired_filter)

    @test_function
    def test_splash_page(self):
        self.GPD.go_splash_page()
        # verify learn more link goes to about page
        self.GPD.click_learn_more_page()
        # check states, districts, and politician hyperlinks
        self.GPD.go_splash_page()
        self.GPD.click_states_page()
        self.GPD.go_splash_page()
        self.GPD.click_districts_page()
        self.GPD.go_splash_page()
        self.GPD.click_politicians_page()
        self.GPD.go_splash_page()

    @test_function
    def test_filtering_features_1(self):
        self.GPD.go_to_page("States")
        # State filter Test 1: Find New York
        self.GPD.filter_states_by(
            {"political_leaning": "Democrat", "pop": (19000000, 20000000)}
        )
        self.GPD.check_for_instance("New York".upper())
        self.GPD.go_to_page("States")

    @test_function
    def test_filtering_features_2(self):
        # State filter Test 2: Find Texas
        self.GPD.filter_states_by(
            {
                "political_leaning": "Republican",
                "pop": (28000000, 29000000),
                "size": (250000, None),
            }
        )
        self.GPD.check_for_instance("Texas".upper())

    @test_function
    def test_filtering_features_3(self):
        # District filter Test 1: Find Rhode Island District 1
        self.GPD.go_to_page("Districts")
        self.GPD.filter_districts_by(
            {"political_leaning": "Democrat", "pop": (0, 600000)}
        )
        self.GPD.check_for_instance("Rhode Island District 1")

    @test_function
    def test_filtering_features_4(self):
        # District filter Test 2: Find Idaho District 1
        self.GPD.go_to_page("Districts")
        self.GPD.filter_districts_by(
            {
                "political_leaning": "Republican",
                "pop": (900001, 1100000),
                "size": (39000, 40000),
            }
        )
        self.GPD.check_for_instance("Idaho District 1")

    @test_function
    def test_filtering_features_5(self):
        # Politician filter Test 1: Find Don Young
        self.GPD.go_to_page("Politicians")
        self.GPD.filter_politicians_by(
            {"political_leaning": "Republican", "age": (85, 100)}
        )
        self.GPD.check_for_instance("Don Young")

    @test_function
    def test_filtering_features_6(self):
        # Politician filter Test 2: Find Madison Cawthorn
        self.GPD.go_to_page("Politicians")
        self.GPD.filter_politicians_by({ "political_leaning": "Republican", 
                                    "age" : (15, 31),
        })
        self.GPD.check_for_instance("Madison Cawthorn")

    @test_function
    def test_searching_filter_1(self):
        # Search for Address and Verify that Corresponding State, District, and Representative are shown
        self.GPD.go_to_page("Home")
        self.GPD.enter_into_splash_search_bar("600 BARROW ANCHORAGE AK")
        self.GPD.check_for_instance("ALASKA")
        self.GPD.check_for_instance("Alaska District At-large")
        self.GPD.check_for_instance("Don Young")

    @test_function
    def test_searching_filter_2(self):
        # Search for State and Verify that Corresponding State
        self.GPD.go_to_page("States")
        self.GPD.enter_into_model_search_bar("Texas")
        self.GPD.check_for_instance("TEXAS")

    @test_function
    def test_searching_filter_3(self):
        # Search for District and Verify that Corresponding District
        self.GPD.go_to_page("Districts")
        self.GPD.enter_into_model_search_bar("California District 1")
        self.GPD.check_for_instance("California District 1")

    @test_function
    def test_searching_filter_4(self):
        # Search for Politician and Verify that Corresponding Politician
        self.GPD.go_to_page("Politicians")
        self.GPD.enter_into_model_search_bar("Al Green")
        self.GPD.check_for_instance("Al Green")

    # @test_function
    # def test_searching_filter_5(self):
    #     #Search using top right search bar and verify the corresponding State, District, and Representative
    #     self.GPD.go_to_page("Home")
    #     self.GPD.enter_into_top_right_search_bar("Tennessee")
    #     self.GPD.check_for_instance("TENNESSEE")
    #     self.GPD.check_for_instance("Tennessee District 1")
    #     self.GPD.check_for_instance("Diana Harshbarger")

    @test_function
    def test_sorting_features_1(self):
        # Sorting Test 1: Sort by size descending and find Texas
        self.GPD.go_to_page("States")
        self.GPD.click_reverse_order_button()
        self.GPD.sort_states_by(desired_sort="Size (sq. mi)")
        self.GPD.check_for_instance("Texas".upper())

    @test_function
    def test_sorting_features_2(self):
        # Sorting Test 2: Sort by size and find Rhode Island
        self.GPD.go_to_page("States")
        self.GPD.sort_states_by(desired_sort="Size (sq. mi)")
        self.GPD.check_for_instance("Rhode Island".upper())

    @test_function
    def test_sorting_features_3(self):
        self.GPD.go_to_page("States")
        # Sorting Test 3: Sort by compacntess and find Maryland
        self.GPD.sort_states_by(desired_sort="Compactness")
        self.GPD.check_for_instance("Maryland".upper())

    @test_function
    def test_sorting_features_4(self):
        self.GPD.go_to_page("States")
        self.GPD.click_reverse_order_button()
        # Sorting Test 4: Sort by compacntess descending and find Wyoming
        self.GPD.sort_states_by(desired_sort="Compactness")
        self.GPD.check_for_instance("Wyoming".upper())

    @test_function
    def test_sorting_features_5(self):
        # Sorting Test 5: Sort by size descending and find Rhode Island
        self.GPD.go_to_page("Districts")
        self.GPD.click_reverse_order_button()
        self.GPD.sort_districts_by(desired_sort="Size (sq. mi)")
        self.GPD.check_for_instance("Alaska District At-large")

    @test_function
    def test_sorting_features_6(self):
        # Sorting Test 6: Sort by Minority Percentage and find Kentucky District 5
        self.GPD.go_to_page("Districts")
        self.GPD.sort_districts_by(desired_sort="Minority Percentage")
        self.GPD.check_for_instance("Kentucky District 5")

    @test_function
    def test_sorting_features_7(self):
        # Sorting Test 7: Sort by Minority Percentage descending and find North York District 15
        self.GPD.go_to_page("Districts")
        self.GPD.click_reverse_order_button()
        self.GPD.sort_districts_by(desired_sort="Minority Percentage")
        self.GPD.check_for_instance("North York District 15")

    @test_function
    def test_sorting_features_8(self):
        # Sorting Test 8: Sort by Age and find Alexandria Ocasio-Cortez
        self.GPD.go_to_page("Politicians")
        self.GPD.sort_politicians_by(desired_sort="Age")
        self.GPD.check_for_instance("Alexandria Ocasio-Cortez")

    @test_function
    def test_sorting_features_9(self):
        # Sorting Test 9: Sort by votes_with_party_percentage and find Don Beyer
        self.GPD.go_to_page("Politicians")
        self.GPD.click_reverse_order_button()
        self.GPD.sort_politicians_by(desired_sort="Voting With Party Percentage")
        self.GPD.check_for_instance("Don Beyer")

    @test_function
    def test_sorting_features_10(self):
        # Sorting Test 10: Sort by votes_with_party_percentage ascending and find Thomas Massie
        self.GPD.go_to_page("Politicians")
        self.GPD.sort_politicians_by(desired_sort="Voting With Party Percentage")
        self.GPD.check_for_instance("Thomas Massie")

    @test_function
    def test_sorting_features_11(self):
        # Sorting Test 11: Sort by Missed Votes Percentage descending and find Ron Wright
        self.GPD.go_to_page("Politicians")
        self.GPD.click_reverse_order_button()
        self.GPD.sort_politicians_by(desired_sort="Missed Votes Percentage")
        self.GPD.check_for_instance("Ron Wright")


if __name__ == "__main__":
    tests = GUITests()
    tests.run_all_tests()
    tests.report_tests()