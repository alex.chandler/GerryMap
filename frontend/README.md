# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

## Available Scripts

If it is your first time running this project, you should run:

### `yarn`

It will install the package according to package.json.
NOTE: It there's error, try to find if there's another node_modules you installed globally.

Then you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test --coverage`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `yarn prettier --write .`

Before every commit.

## Learn More

-   [React documentation](https://reactjs.org/).

-   For UI we use [react-boostrap](https://react-bootstrap.github.io/getting-started/introduction). Use the tags in the documentation.

-   [react-router-dom](https://reactrouter.com/docs/en/v6/getting-started/overview). Note the version differences.

-   Class and function based [Refer](http://blog.magmalabs.io/2021/02/24/classes-vs-hooks-on-react.html): For using hook easily, we use function based desgin.

-   For api call we use axios instead of fetch. [Refer](https://www.smashingmagazine.com/2020/06/rest-api-react-fetch-axios/)

-   Charts: [Recharts](https://recharts.org/en-US/)

-   Icons: [React-icons](https://react-icons.github.io/react-icons/)
