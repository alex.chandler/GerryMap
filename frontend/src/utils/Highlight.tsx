//const Text = ({ children, style }) => <span style={style}>{children}</span>;

function escapeRegExp(text: string) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

export function Highlight(fullText: string, searchTerm: string) {
    //Highlight function
    if (!searchTerm.trim()) {
        return <span>{fullText}</span>;
    }

    const regexStr =
        "(" + searchTerm.trim().split(/\s+/).map(escapeRegExp).join("|") + ")";
    const regex = new RegExp(regexStr, "gi");
    const parts = fullText.split(regex);
    const final = parts
        .filter((part) => part)
        .map((part, i) =>
            regex.test(part) ? (
                <span style={{ backgroundColor: "yellow" }} key={i}>
                    {part}
                </span>
            ) : (
                <span key={i}>{part}</span>
            )
        );
    return <span>{final}</span>;
}
