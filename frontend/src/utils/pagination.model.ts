export interface PaginationResponseModel {
    total_items: number;
    page_num: number;
    page_size: number;
    first_index: number;
    last_index: number;
}

export interface PaginationParameterModel {
    page: number;
    page_size: number;
}
