export interface chartData {
    name: string;
    value: number;
}

export const populationData: chartData[] = [
    { name: "White", value: 400 },
    { name: "Hispanic", value: 300 },
    { name: "Asian", value: 300 },
    { name: "Black", value: 200 },
];

export const genderData: chartData[] = [
    { name: "Male", value: 400 },
    { name: "Female", value: 300 },
];

export const politicianChartData: chartData[] = [
    { name: "Miss Vote", value: 17 },
    { name: "Vote With Party", value: 30 },
];

export interface chartDataJs {
    labels: string[];
    datasets: {
        label: string;
        data: number[];
        backgroundColor: string[];
        borderColor: string[];
        borderWidth: number;
    }[];
}

export interface DemographicDetailModel {
    white_pop: number;
    hispanic_pop: number;
    asian_pop: number;
    black_pop: number;
    male_pop: number;
    female_pop: number;
}
