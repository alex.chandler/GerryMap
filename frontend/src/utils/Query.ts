export function getQueryString(params: {
    [key: string]: string | number | boolean;
}) {
    const components = [];
    for (const key in params) {
        const value = params[key];
        components.push(key + "=" + String(value));
    }
    return "?" + components.join("&");
}

export interface SearchQueryModel {
    query: string;
}

export interface SearchAddressModel {
    address: string;
}
