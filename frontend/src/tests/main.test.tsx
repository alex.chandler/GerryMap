import { render, screen, fireEvent } from "@testing-library/react";
import { within } from "@testing-library/dom";
import HomePage from "../pages/HomePage";
import About from "../pages/About";
import States from "../pages/States";
import StateInstance from "../pages/StateInstance";
import Districts from "../pages/Districts";
import DistrictInstance from "../pages/DistrictInstance";
import Politicians from "../pages/Politicians";
import PoliticianInstance from "../pages/PoliticianInstance";
import SearchPage from "../pages/SearchPage";
import Header from "../components/Header";
import Faq from "../pages/Faq";
import { BrowserRouter } from "react-router-dom";
import { act } from "react-dom/test-utils";
import axios from "axios";
import { states } from "../models/state.model";
import { districts } from "../models/district.model";
import { politicians } from "../models/politician.model";
import { getDistrictName } from "../utils/Names";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;
// jest.mock('react-router-dom', () => ({
//     ...jest.requireActual('react-router-dom'),
//     useParams: () => ({
//         id: "CA",
//     }),
// }));

// mock useNavigate
const mockedUsedNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: () => mockedUsedNavigate, // Return an empty jest function to test whether it was called or not...I'm not depending on the results so no need to put in a return value
}));

const spiedConsoleError = jest.spyOn(global.console, "error");
afterAll(() => {
    spiedConsoleError.mockRestore();
});

function testTableCardView() {
    //Renders table view
    const toggleTableButton = screen.getByTestId("toggle-table-view");
    fireEvent.click(toggleTableButton);
    const tableElement = screen.getByRole("table");
    expect(tableElement).toBeInTheDocument();

    //click first table element
    const rowElement = screen.getAllByRole("row");
    fireEvent.click(rowElement[1]);
    expect(mockedUsedNavigate).toHaveBeenCalledTimes(1);

    //Renders card view
    const toggleCardButton = screen.getByTestId("toggle-card-view");
    fireEvent.click(toggleCardButton);
    expect(tableElement).not.toBeInTheDocument();
}

async function testPagination() {
    //Renders pagination
    const dropdown = screen.getByTestId("item-dropdown");
    expect(dropdown).toBeInTheDocument();
    const showItem = dropdown.querySelector("#dropdown-basic-button");
    if (showItem) {
        fireEvent.click(showItem);
    }
    const target = screen.getByTestId("select-option-25");
    fireEvent.click(target);
    await expect(target).toHaveTextContent("25");
    await expect(showItem).toHaveTextContent("25");

    const goFirstPageButton = screen.getByTestId("button-go-first-page");
    fireEvent.click(goFirstPageButton);
    const firstPageButton = await screen
        .getByTestId("button-first-page")
        .closest("li");
    expect(firstPageButton).toHaveTextContent("current");
}

async function testSort() {
    const sortDropdownButton = screen.getByText(/Sort By/i);
    fireEvent.click(sortDropdownButton);
    const option = screen.getByTestId("dropdown-sort-1");
    fireEvent.click(option);
    expect(option).toHaveAttribute("aria-selected", "true");
}

async function testSortAscending() {
    const button = screen.getByTestId("ascending-button");
    const ascendingIcon = screen.getByTestId("ascending-icon");
    expect(ascendingIcon).toBeInTheDocument();
    fireEvent.click(button);
    const descendingIcon = screen.getByTestId("descending-icon");
    expect(descendingIcon).toBeInTheDocument();
    expect(ascendingIcon).not.toBeInTheDocument();
}

async function testFilter() {
    const filterDropdownButton = screen.getByText(/Filter By/i);
    fireEvent.click(filterDropdownButton);
    const option = screen.getByTestId("dropdown-filter-1");
    fireEvent.click(option);
    let checkbox = within(option).getByRole("checkbox");
    expect(checkbox).toHaveAttribute("checked");

    fireEvent.click(option);
    checkbox = within(option).getByRole("checkbox");
    expect(checkbox).not.toHaveAttribute("checked");

    const dropdownGroup = screen.getByTestId("group-filter-sort");
    const textInput = within(dropdownGroup).getAllByRole("spinbutton");

    fireEvent.change(textInput[0], { target: { value: "23" } });
    expect((textInput[0] as HTMLInputElement).value).toBe("23");
}

async function testSearch() {
    const searchBar = screen.getByTestId("group-search");
    const textInput = within(searchBar).getByRole("textbox");
    const searchButton = within(searchBar).getByRole("button");

    fireEvent.change(textInput, { target: { value: "test" } });
    expect((textInput as HTMLInputElement).value).toBe("test");
    fireEvent.click(searchButton);
}

describe("renders static pages", () => {
    test("renders faq page", () => {
        render(<Faq />);
        const linkElement = screen.getByText(/Frequently Asked Questions/i);
        expect(linkElement).toBeInTheDocument();
    });
});

describe("renders splash page", () => {
    beforeEach(() => {
        mockedAxios.get.mockImplementation(() => {
            return Promise.resolve({
                data: {
                    state: states.data[0],
                    district: districts.data[0],
                    politician: politicians.data[0],
                },
            });
        });

        act(() => {
            render(<HomePage />);
        });
    });

    test("renders basic elements", () => {
        const linkElement = screen.getAllByText(/gerry/i);
        expect(linkElement[0]).toBeInTheDocument();
    });

    test("render search by address", async () => {
        const addressInputGroup = screen.getByTestId(
            "inputgroup-search-by-address"
        );
        const textInput = within(addressInputGroup).getByRole("textbox");
        const searchButton = within(addressInputGroup).getByRole("button");
        fireEvent.change(textInput, {
            target: { value: "110 Inner Campus Drive, Austin, TX 78705" },
        });
        expect((textInput as HTMLInputElement).value).toBe(
            "110 Inner Campus Drive, Austin, TX 78705"
        );
        fireEvent.click(searchButton);
        fireEvent.keyDown(textInput, {
            key: "Enter",
            code: "Enter",
            charCode: 13,
        });
        // TODO: test api and check cards rendered
        // const stateCard = await screen.findByTestId("state-card");
        // expect(
        //     within(stateCard).getByText(states.data[0].state_name)
        // ).toBeInTheDocument();
    });
});

describe("renders search page", () => {
    beforeEach(() => {
        mockedAxios.get.mockResolvedValueOnce(() => {
            return Promise.resolve({
                data: {
                    states: states,
                    districts: districts,
                    politicians: politicians,
                },
            });
        });

        act(() => {
            render(
                <>
                    <Header />
                    <BrowserRouter>
                        <SearchPage />
                    </BrowserRouter>
                </>
            );
        });
    });

    test("render search all", async () => {
        const searchInputGroup = screen.getByTestId("inputgroup-search-all");
        const textInput = within(searchInputGroup).getByRole("textbox");
        const searchButton = within(searchInputGroup).getByRole("button");
        fireEvent.change(textInput, {
            target: { value: "texas" },
        });
        expect((textInput as HTMLInputElement).value).toBe("texas");
        fireEvent.click(searchButton);
        fireEvent.keyDown(textInput, {
            key: "Enter",
            code: "Enter",
            charCode: 13,
        });
        // TODO: test api and check table rendered
        // Let's also make sure our Axios mock was called the way we expect
        //   expect(axiosMock.get).toHaveBeenCalledTimes(1);
        //   expect(axiosMock.get).toHaveBeenCalledWith(url);
    });
});

describe("renders about page", () => {
    //Test 3
    test("renders about page with mocked data", async () => {
        const contributors = [
            {
                name: "Helen Fang",
                email: "string",
                commits: 20,
                additions: 3,
                deletions: 3,
            },
            {
                name: "Alex L Chandler",
                email: "string",
                commits: 15,
                additions: 3,
                deletions: 3,
            },
            {
                name: "Alex Chandler",
                email: "string",
                commits: 7,
                additions: 3,
                deletions: 3,
            },
        ];

        const statistics = {
            statistics: {
                counts: {
                    all: 2,
                    closed: 1,
                    opened: 1,
                },
            },
        };

        mockedAxios.get.mockImplementation((url) => {
            if (
                url.includes(
                    "https://gitlab.com/api/v4/projects/33862053/repository/contributors"
                )
            ) {
                return Promise.resolve({
                    data: contributors,
                });
            } else {
                return Promise.resolve({
                    data: statistics,
                });
            }
            //return Promise.reject(new Error("not found"))
        });

        act(() => {
            render(<About />);
        });

        //Renders correct description text
        const element = await screen.findAllByText(/about/i);
        expect(element[0]).toBeInTheDocument();

        //Renders correct issue count
        const issueElements = screen.getAllByText(/Issues/i);
        expect(issueElements[0]).toHaveTextContent(/Issues: 2/i);

        //Renders correct commit counts
        const commitElements = screen.getAllByText(/Commits/i);
        expect(commitElements[0]).toHaveTextContent(/Commits: 20/i);
        expect(commitElements[1]).toHaveTextContent(/Commits: 22/i);
    });
});

describe("renders states page with mocked data", () => {
    beforeEach(() => {
        mockedAxios.get
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: states,
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: {
                        ...states,
                        meta: { ...states.meta, page_size: 25, page_num: 4 },
                    },
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: {
                        ...states,
                        meta: { ...states.meta, page_num: 1 },
                    },
                });
            });

        act(() => {
            render(
                <BrowserRouter>
                    <States />
                </BrowserRouter>
            );
        });
    });

    test("renders basic elements", async () => {
        //Renders correct description text
        const linkElement = await screen.findByText(/states/i);
        expect(linkElement).toBeInTheDocument();

        //Renders correct political leaning text
        const stateNameElement = screen.getAllByText(states.data[0].state_name);
        expect(stateNameElement[0]).toBeInTheDocument();
        // expect(stateNameElement[0]).toHaveTextContent(
        //     states.data[0].party_detailed
        // );
        // expect(stateNameElement).toHaveTextContent(
        //     states.data[1].party_detailed
        // );
    });

    test("renders table/card view", async () => {
        testTableCardView();
    });

    test("renders pagination", async () => {
        testPagination();
    });

    test("renders sort dropdown", async () => {
        testSort();
        testSortAscending();
    });

    test("renders filter dropdown", async () => {
        testFilter();
    });

    test("renders search bar", async () => {
        testSearch();
    });
});

describe("renders state instance page", () => {
    //Test 7
    test("renders state instance page with mocked data", async () => {
        mockedAxios.get
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: states.data[0],
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: districts,
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: politicians,
                });
            });
        act(() => {
            render(
                <BrowserRouter>
                    <StateInstance />
                </BrowserRouter>
            );
        });

        //Renders correct description text
        const element = await screen.findByText(states.data[0].state_name);
        expect(element).toBeInTheDocument();

        // May have problems here
        const str =
            states.data[0].state_name +
            " District " +
            districts.data[0].district_num;
        const districtElement = screen.getByText(new RegExp(str, "i"));
        expect(districtElement).toBeInTheDocument();

        const politicianElement = screen.getByText(politicians.data[0].name);
        expect(politicianElement).toBeInTheDocument();

        //TODO: better approch to check url param
    });
});

describe("renders district page with mocked data", () => {
    beforeEach(() => {
        mockedAxios.get
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: districts,
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: {
                        ...districts,
                        meta: { ...districts.meta, page_size: 25, page_num: 4 },
                    },
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: {
                        ...districts,
                        meta: { ...districts.meta, page_num: 1 },
                    },
                });
            });

        act(() => {
            render(
                <BrowserRouter>
                    <Districts />
                </BrowserRouter>
            );
        });
    });

    test("renders basic elements", async () => {
        //Renders correct description text
        const linkElement = await screen.findByText(/districts/i);
        expect(linkElement).toBeInTheDocument();

        //Renders correct political leaning text
        // const districtsElement = screen.getAllByText(/Political Leaning/i);
        const districtsElement = screen.getAllByText(
            districts.data[0].representative
        );
        expect(districtsElement[0]).toBeInTheDocument();
        // expect(districtsElement[0]).toHaveTextContent(
        //     districts.data[0].party_detailed
        // );
        // expect(districtsElement[1]).toHaveTextContent(
        //     districts.data[1].party_detailed
        // );
    });

    test("renders table/card view", async () => {
        testTableCardView();
    });

    test("renders pagination", async () => {
        testPagination();
    });

    test("renders sort dropdown", async () => {
        testSort();
        testSortAscending();
    });

    test("renders filter dropdown", async () => {
        testFilter();
    });

    test("renders search bar", async () => {
        testSearch();
    });
});

describe("renders district instance page", () => {
    test("renders district instance page with mocked data", async () => {
        mockedAxios.get
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: districts.data[0],
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: politicians.data[0],
                });
            });

        act(() => {
            render(
                <BrowserRouter>
                    <DistrictInstance />
                </BrowserRouter>
            );
        });

        //Renders correct description text
        const element = await screen.findByText(
            getDistrictName(
                districts.data[0].state_abbrev,
                districts.data[0].district_num
            )
        );
        expect(element).toBeInTheDocument();

        const politicianElement = screen.getByText(
            new RegExp(politicians.data[0].name, "i")
        );
        expect(politicianElement).toBeInTheDocument();
    });
});

describe("renders politician page with mocked data", () => {
    beforeEach(() => {
        mockedAxios.get
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: politicians,
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: {
                        ...politicians,
                        meta: {
                            ...politicians.meta,
                            page_size: 25,
                            page_num: 4,
                        },
                    },
                });
            })
            .mockImplementationOnce(() => {
                return Promise.resolve({
                    data: {
                        ...politicians,
                        meta: {
                            ...politicians.meta,
                            page_num: 1,
                        },
                    },
                });
            });
        act(() => {
            render(
                <BrowserRouter>
                    <Politicians />
                </BrowserRouter>
            );
        });
    });

    test("renders politician page", async () => {
        //Renders correct description text
        const linkElement = await screen.findByText(/politicians/i);
        expect(linkElement).toBeInTheDocument();

        //Renders correct political leaning text
        // const politiciansElement = screen.getAllByText(/Political Party/i);
        // expect(politiciansElement[0]).toHaveTextContent(
        //     politicians.data[0].party_detailed
        // );
        // expect(politiciansElement[1]).toHaveTextContent(
        //     politicians.data[1].party_detailed
        // );
        const politiciansElement = screen.getAllByText(
            politicians.data[0].state_abbrev
        );
        expect(politiciansElement[0]).toBeInTheDocument();
    });

    test("renders table/card view", async () => {
        testTableCardView();
    });

    test("renders pagination", async () => {
        testPagination();
    });

    test("renders default image when fetching image error", async () => {
        //Test politician image load error
        const imgElement = screen.getAllByRole("img");
        fireEvent(imgElement[0], new Event("error"));
        expect(imgElement[0]).toHaveAttribute(
            "src",
            "placeholder-image-person.jpeg"
        );
    });

    test("renders sort dropdown", async () => {
        testSort();
        testSortAscending();
    });

    test("renders filter dropdown", async () => {
        testFilter();
    });

    test("renders search bar", async () => {
        testSearch();
    });
});

describe("renders politician instance page", () => {
    test("renders politician instance page with mocked data", async () => {
        mockedAxios.get.mockImplementationOnce(() => {
            return Promise.resolve({
                data: politicians.data[0],
            });
        });
        act(() => {
            render(
                <BrowserRouter>
                    <PoliticianInstance />
                </BrowserRouter>
            );
        });

        //Renders correct description text
        const element = await screen.findByText(politicians.data[0].name);
        expect(element).toBeInTheDocument();
    });
});

describe("tests failing to fetch api for all pages", () => {
    beforeEach(() => {
        mockedAxios.get.mockImplementationOnce(() => {
            return Promise.reject(new Error());
        });
    });

    afterEach(async () => {
        const element = await screen.findAllByText("");
        expect(element[0]).toBeInTheDocument();
        expect(console.error).toHaveBeenCalled();
    });

    test("about page", async () => {
        act(() => {
            render(<About />);
        });
    });

    test("states page", async () => {
        act(() => {
            render(
                <BrowserRouter>
                    <States />
                </BrowserRouter>
            );
        });
    });

    test("state instance page", async () => {
        act(() => {
            render(
                <BrowserRouter>
                    <StateInstance />
                </BrowserRouter>
            );
        });
    });

    test("districts page", async () => {
        act(() => {
            render(
                <BrowserRouter>
                    <Districts />
                </BrowserRouter>
            );
        });
    });

    test("district instance page", async () => {
        act(() => {
            render(
                <BrowserRouter>
                    <DistrictInstance />
                </BrowserRouter>
            );
        });
    });

    test("politicians page", async () => {
        act(() => {
            render(
                <BrowserRouter>
                    <Politicians />
                </BrowserRouter>
            );
        });
    });

    test("politicians instance page", async () => {
        act(() => {
            render(
                <BrowserRouter>
                    <PoliticianInstance />
                </BrowserRouter>
            );
        });
    });
});
