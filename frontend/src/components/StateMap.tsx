import React from "react";
import { ComposableMap, Geographies, Geography } from "react-simple-maps";
import { StateInstanceModel } from "../models/state.model";
import { IDistrictTopoObject } from "./DistrictMap";
import { computeColor } from "../styles/Colors";

interface IStateMapProps {
    stateDetails: StateInstanceModel;
}

interface IStateMapState {
    json_url: string;
    sqMiles: number;
    color: string;
    data?: {
        properties: {
            lat: string;
            lng: string;
        };
        objects: { [key: string]: IDistrictTopoObject };
    };
}

class StateMap extends React.Component<IStateMapProps, IStateMapState> {
    constructor(props: IStateMapProps) {
        super(props);
        this.state = {
            sqMiles: props.stateDetails.sq_miles,
            color: computeColor(
                props.stateDetails.party_detailed,
                props.stateDetails.MOV
            ),
            json_url:
                "https://raw.githubusercontent.com/fang-helen/gerrymap_json/master/states/" +
                props.stateDetails.state_abbrev +
                ".json",
            data: undefined,
        };
    }

    componentDidMount() {
        // load the geo data from the json file
        const setState = this.setState.bind(this);
        fetch(this.state.json_url)
            .then((response) => response.json())
            .then((jsonData) => {
                setState({ data: jsonData });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        if (this.state.data == null) {
            // component has not finished loading async data yet
            return <div>Loading Map...</div>;
        }

        const geojson = this.state.data;

        const lat = parseFloat(geojson["properties"].lat);
        const long = parseFloat(geojson["properties"].lng);

        const scaleFactor = 600000 / Math.sqrt(this.state.sqMiles);
        return (
            <div className="map-container">
                <ComposableMap
                    height={550}
                    projection="geoMercator"
                    style={{ maxHeight: "500px", width: "100%" }}
                    projectionConfig={{
                        center: [long, lat],
                        scale: scaleFactor,
                    }}
                >
                    <Geographies geography={geojson}>
                        {({ geographies }) =>
                            geographies.map((geo) => (
                                <Geography
                                    key={geo.rsmKey}
                                    geography={geo}
                                    fill={this.state.color}
                                />
                            ))
                        }
                    </Geographies>
                </ComposableMap>
            </div>
        );
    }
}

export default StateMap;
