import { DistrictInstanceModel } from "../models/district.model";
import { Highlight } from "../utils/Highlight";
import { getDistrictName } from "../utils/Names";
import Table from "react-bootstrap/Table";
import { useNavigate } from "react-router-dom";

export interface IDistrictTableProps {
    districts: DistrictInstanceModel[];
    searchTerm: string;
}

export default function DistrictTable(props: IDistrictTableProps) {
    const navigate = useNavigate();
    function handleRowClick(district: DistrictInstanceModel) {
        navigate(
            "/districts/" +
                (district.state_abbrev + "_" + district.district_num)
        );
    }
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>State Abbr.</th>
                    <th>Representative</th>
                    <th>Political Leaning</th>
                    <th>Population</th>
                    <th>Size (sq. mi)</th>
                    <th>Minority %</th>
                    <th>Compactness</th>
                </tr>
            </thead>
            <tbody>
                <>
                    {props.districts.map((district) => (
                        <tr
                            onClick={() => handleRowClick(district)}
                            key={district.state_abbrev + district.district_num}
                        >
                            <td>
                                {Highlight(
                                    getDistrictName(
                                        district.state_abbrev,
                                        district.district_num
                                    ),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.state_abbrev,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.representative,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.party_detailed,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.total_pop.toString(),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.sq_miles.toString(),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.non_white_percentage + "%",
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    district.district_compactness.toFixed(2),
                                    props.searchTerm
                                )}
                            </td>
                        </tr>
                    ))}
                </>
            </tbody>
        </Table>
    );
}
