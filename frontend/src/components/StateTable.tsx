import { StateInstanceModel } from "../models/state.model";
import { Highlight } from "../utils/Highlight";
import Table from "react-bootstrap/Table";
import { useNavigate } from "react-router-dom";

export interface IStateTableProps {
    states: StateInstanceModel[];
    searchTerm: string;
}

export default function StateTable(props: IStateTableProps) {
    const navigate = useNavigate();
    function handleRowClick(state: StateInstanceModel) {
        navigate("/states/" + state.state_abbrev);
    }

    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Abbreviation</th>
                    <th>Region</th>
                    <th>Size (sq. mi)</th>
                    <th>Political Leaning</th>
                    <th>Minority %</th>
                    <th>Compactness</th>
                </tr>
            </thead>
            <tbody>
                <>
                    {props.states.map((state) => (
                        <tr
                            onClick={() => handleRowClick(state)}
                            key={state.state_name}
                        >
                            <td>
                                {Highlight(state.state_name, props.searchTerm)}
                            </td>
                            <td>
                                {Highlight(
                                    state.state_abbrev,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    state.census_region,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    state.sq_miles.toString(),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    state.party_detailed,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    state.non_white_percentage
                                        ? state.non_white_percentage.toFixed(
                                              0
                                          ) + "%"
                                        : "0",
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    state.state_compactness.toFixed(2),
                                    props.searchTerm
                                )}
                            </td>
                        </tr>
                    ))}
                </>
            </tbody>
        </Table>
    );
}
