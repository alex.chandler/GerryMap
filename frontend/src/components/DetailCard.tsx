import React, { ReactElement } from "react";
import { Link } from "react-router-dom";
import Card from "react-bootstrap/Card";

interface IDetailCardProps {
    title?: string;
    itemKey?: string;
    content: ReactElement;
    to: string;
    details?: ReactElement;
}

export default function DetailCard(props: IDetailCardProps) {
    return (
        <Link className="card-link" to={props.to}>
            <Card>
                {props.content}
                {props.title !== undefined ? (
                    <Card.Body>
                        <Card.Title>{props.title}</Card.Title>
                    </Card.Body>
                ) : null}
                {props.details !== undefined ? props.details : null}
            </Card>
        </Link>
    );
}
