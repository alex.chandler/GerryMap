import { PoliticianInstanceModel } from "../models/politician.model";
import { Highlight } from "../utils/Highlight";
import { abbrevToName } from "../utils/Names";
import Table from "react-bootstrap/Table";
import { useNavigate } from "react-router-dom";

export interface IPoliticianTableProps {
    politicians: PoliticianInstanceModel[];
    searchTerm: string;
}

export default function PoliticianTable(props: IPoliticianTableProps) {
    const navigate = useNavigate();
    function handleRowClick(politician: PoliticianInstanceModel) {
        navigate(
            "/politicians/" +
                politician.state_abbrev +
                "_" +
                politician.district_num
        );
    }
    return (
        <Table striped bordered hover className="row-politician-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>State</th>
                    <th>State Abbr.</th>
                    <th>Congressional District</th>
                    <th>Political Party</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Years in office</th>
                    <th>Missed Votes Percentage</th>
                    <th>Voting with Party Percent</th>
                </tr>
            </thead>
            <tbody>
                <>
                    {props.politicians.map((politician) => (
                        <tr
                            onClick={() => handleRowClick(politician)}
                            key={
                                politician.state_abbrev +
                                "_" +
                                politician.district_num
                            }
                        >
                            <td>
                                {Highlight(politician.name, props.searchTerm)}
                            </td>
                            <td>
                                {Highlight(
                                    abbrevToName[politician.state_abbrev].name,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    politician.state_abbrev,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    politician.district_num.toString(),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    politician.party_detailed,
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    politician.age.toString(),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(politician.gender, props.searchTerm)}
                            </td>
                            <td>
                                {Highlight(
                                    politician.years_in_office.toString(),
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    politician.missed_votes_pct + "%",
                                    props.searchTerm
                                )}
                            </td>
                            <td>
                                {Highlight(
                                    politician.votes_with_party_pct + "%",
                                    props.searchTerm
                                )}
                            </td>
                        </tr>
                    ))}
                </>
            </tbody>
        </Table>
    );
}
