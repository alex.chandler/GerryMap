import Pagination from "react-bootstrap/Pagination";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import Form from "react-bootstrap/Form";
import "../styles/PaginationComponent.scss";

export interface IPaginationComponentProps {
    totalItems: number;
    lastPage: number;
    curPage: number;
    paginate: (n: number) => void;
    curItemPerPage: number;
    changeItemsPerPage: (n: number) => void;
    lastIndex: number;
    firstIndex: number;
}

export function PaginationComponent(props: IPaginationComponentProps) {
    const itemsPerPage = [5, 10, 25, 50];
    const pageNumbers = [];
    const pageRange = 2;
    const curPage = props.curPage;

    const showPreEllipsis = curPage - (pageRange + 1) > 1 ? true : false;
    const showPostEllipsis =
        curPage + (pageRange + 1) < props.lastPage ? true : false;
    let startPage = curPage - pageRange;
    let endPage = curPage + pageRange;

    if (startPage <= 0) {
        endPage += Math.abs(1 - startPage);
        startPage = 2;
    }

    if (endPage > props.lastPage) {
        startPage -= Math.abs(endPage - props.lastPage);
        if (startPage < 0) {
            startPage = 2;
        }
        endPage = props.lastPage - 1;
    }

    for (let i = startPage; i <= endPage; i++) {
        if (i > 1 && i < props.lastPage) {
            pageNumbers.push(i);
        }
    }

    return (
        <>
            <Pagination>
                <Pagination.First
                    data-testid="button-go-first-page"
                    onClick={() => props.paginate(1)}
                />
                <Pagination.Prev
                    onClick={() =>
                        props.paginate(curPage - 1 < 1 ? 1 : curPage - 1)
                    }
                />
                <Pagination.Item
                    onClick={() => props.paginate(1)}
                    data-testid="button-first-page"
                    active={curPage === 1}
                >
                    {1}
                </Pagination.Item>
                {showPreEllipsis && <Pagination.Ellipsis />}
                {pageNumbers.map((number) => (
                    <Pagination.Item
                        key={number}
                        onClick={() => props.paginate(number)}
                        active={number === curPage}
                    >
                        {number}
                    </Pagination.Item>
                ))}
                {showPostEllipsis && <Pagination.Ellipsis />}
                {props.lastPage !== 1 && (
                    <Pagination.Item
                        onClick={() => props.paginate(props.lastPage)}
                        active={curPage === props.lastPage}
                    >
                        {props.lastPage}
                    </Pagination.Item>
                )}
                <Pagination.Next
                    onClick={() =>
                        props.paginate(
                            curPage + 1 > props.lastPage
                                ? props.lastPage
                                : curPage + 1
                        )
                    }
                />
                <Pagination.Last
                    onClick={() => props.paginate(props.lastPage)}
                />
                <Form.Label className="ps-3">
                    {props.firstIndex} - {props.lastIndex} of {props.totalItems}
                </Form.Label>
                <Form.Label className="ps-3" htmlFor="basic-url">
                    Items Per Page:
                </Form.Label>
                <DropdownButton
                    data-testid="item-dropdown"
                    className="ps-1"
                    id="dropdown-basic-button"
                    variant="outline-primary"
                    title={props.curItemPerPage}
                >
                    {itemsPerPage.map((number) => (
                        <Dropdown.Item
                            data-testid={"select-option-" + number}
                            key={number}
                            onClick={() => {
                                props.changeItemsPerPage(number);
                            }}
                            active={number === props.curItemPerPage}
                        >
                            {number}
                        </Dropdown.Item>
                    ))}
                </DropdownButton>
            </Pagination>
        </>
    );
}
