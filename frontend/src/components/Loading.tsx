export function Loading() {
    return (
        <div
            style={{
                width: "100%",
                padding: "5em",
                textAlign: "center",
            }}
        >
            Loading...
        </div>
    );
}
