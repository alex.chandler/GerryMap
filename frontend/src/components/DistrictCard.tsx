import { DistrictInstanceModel } from "../models/district.model";
import { Highlight } from "../utils/Highlight";
import { getDistrictName } from "../utils/Names";

export interface IDistrictCardProps {
    district: DistrictInstanceModel;
    searchParam: string;
}

export function DistrictCard(props: IDistrictCardProps) {
    return (
        <>
            <div className="card-body">
                <div className="card-title h5">
                    {Highlight(
                        getDistrictName(
                            props.district.state_abbrev,
                            props.district.district_num
                        ),
                        props.searchParam
                    )}
                </div>
                <div className="list-info-group-wrap ">
                    <div className="list-info-group border-bottom">
                        <div className="info-header">State Abbreviation</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.state_abbrev,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Representative</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.representative,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Size</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.sq_miles.toLocaleString(
                                    "en-US"
                                ) + " sq mi",
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Population</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.total_pop.toLocaleString(
                                    "en-US"
                                ),
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Political Leaning</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.party_detailed,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Minority Percentage</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.non_white_percentage + "%",
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group ">
                        <div className="info-header">Compactness</div>
                        <div className="info-body">
                            {Highlight(
                                props.district.district_compactness.toFixed(2),
                                props.searchParam
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
