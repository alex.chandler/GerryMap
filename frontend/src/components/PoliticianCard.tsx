import { PoliticianInstanceModel } from "../models/politician.model";
import { Highlight } from "../utils/Highlight";
import { abbrevToName } from "../utils/Names";

export interface IPoliticianCardProps {
    politician: PoliticianInstanceModel;
    searchParam: string;
}

export function PoliticianCard(props: IPoliticianCardProps) {
    return (
        <>
            <div className="card-body">
                <div className="card-title h5">
                    {Highlight(props.politician.name, props.searchParam)}
                </div>
                <div className="list-info-group-wrap ">
                    <div className="list-info-group border-bottom">
                        <div className="info-header">State</div>
                        <div className="info-body">
                            {Highlight(
                                abbrevToName[props.politician.state_abbrev]
                                    .name,
                                props.searchParam
                            )}
                        </div>
                    </div>

                    <div className="list-info-group border-bottom">
                        <div className="info-header">State Abbreviation</div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.state_abbrev,
                                props.searchParam
                            )}
                        </div>
                    </div>

                    <div className="list-info-group border-bottom">
                        <div className="info-header">
                            Congressional District
                        </div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.district_num.toString(),
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Political Party</div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.party_detailed,
                                props.searchParam
                            )}
                        </div>
                    </div>

                    <div className="list-info-group border-bottom">
                        <div className="info-header">Age</div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.age.toString(),
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Gender</div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.gender,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Years in office</div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.years_in_office.toString(),
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">
                            {" "}
                            Missed Votes Percentage
                        </div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.missed_votes_pct + "%",
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group">
                        <div className="info-header">
                            {" "}
                            Voting with Party Percent
                        </div>
                        <div className="info-body">
                            {Highlight(
                                props.politician.votes_with_party_pct + "%",
                                props.searchParam
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
