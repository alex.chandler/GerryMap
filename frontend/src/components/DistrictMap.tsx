import React from "react";
import { ComposableMap, Geographies, Geography } from "react-simple-maps";
import { DistrictInstanceModel } from "../models/district.model";
import { LIGHTGRAY, rgbToHex, computeColor } from "../styles/Colors";

interface IDistrictMapProps {
    districtDetails: DistrictInstanceModel;
}

interface IDistrictMapState {
    state_abbrev: string;
    district_num: number;
    MOV: number;
    sq_miles: number;
    json_url: string;
    state_json_url: string;
    color: string;
    data?: {
        objects: { [key: string]: IDistrictTopoObject };
    };
    state_data?: {
        objects: { [key: string]: IDistrictTopoObject };
    };
}

export interface IDistrictTopoObject {
    geometries: {
        properties: {
            INTPTLAT: string; // lat
            INTPTLON: string; // lng
            CD116FP: string; // district number
        };
    }[];
}

class DistrictMap extends React.Component<
    IDistrictMapProps,
    IDistrictMapState
> {
    constructor(props: IDistrictMapProps) {
        super(props);
        this.state = {
            state_abbrev: props.districtDetails.state_abbrev,
            district_num: props.districtDetails.district_num,
            MOV: props.districtDetails.MOV,
            sq_miles: props.districtDetails.sq_miles,
            color: computeColor(
                props.districtDetails.party_detailed,
                props.districtDetails.MOV
            ),
            json_url:
                "https://raw.githubusercontent.com/fang-helen/gerrymap_json/master/districts/" +
                props.districtDetails.state_abbrev +
                "_" +
                props.districtDetails.district_num +
                ".json",
            state_json_url:
                "https://raw.githubusercontent.com/fang-helen/gerrymap_json/master/states/" +
                props.districtDetails.state_abbrev +
                ".json",
            data: undefined,
            state_data: undefined,
        };
    }

    componentDidMount() {
        // load the geo data from the json file
        const setState = this.setState.bind(this);
        fetch(this.state.json_url)
            .then((response) => response.json())
            .then((jsonData) => {
                setState({ data: jsonData });
            })
            .catch((error) => {
                console.error(error);
            });

        const cacheMapJson = localStorage.getItem(this.state.state_json_url);
        if (cacheMapJson !== undefined && cacheMapJson !== null) {
            setState({ state_data: JSON.parse(cacheMapJson) });
        } else {
            fetch(this.state.state_json_url)
                .then((response) => {
                    return response.json();
                })
                .then((jsonData) => {
                    setState({ state_data: jsonData });
                    if (localStorage.length > 5) {
                        localStorage.clear();
                    }
                    localStorage.setItem(
                        this.state.state_json_url,
                        JSON.stringify(jsonData)
                    );
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    render() {
        if (!this.state.data || !this.state.state_data) {
            // component has not finished loading async data yet
            return <div>Loading Map...</div>;
        }

        const district_json = this.state.data;
        const properties =
            district_json["objects"][
                this.state.state_abbrev + "_" + this.state.district_num
            ]["geometries"][0]["properties"];
        const lat = parseFloat(properties.INTPTLAT);
        const long = parseFloat(properties.INTPTLON);
        const tgt = properties.CD116FP;

        const scaleFactor = 500000 / Math.sqrt(this.state.sq_miles);
        const state_json = this.state.state_data;

        return (
            <div className="map-container">
                <ComposableMap
                    height={400}
                    projection="geoMercator"
                    style={{ maxHeight: "400px", width: "100%" }}
                    projectionConfig={{
                        center: [long, lat],
                        scale: scaleFactor,
                    }}
                >
                    <Geographies geography={state_json}>
                        {({ geographies }) =>
                            geographies.map((geo) => (
                                <Geography
                                    key={geo.rsmKey}
                                    geography={geo}
                                    fill={
                                        geo.properties.CD116FP === tgt
                                            ? this.state.color
                                            : rgbToHex(LIGHTGRAY)
                                    }
                                    stroke="#EEEEEE"
                                    strokeWidth={0.5}
                                />
                            ))
                        }
                    </Geographies>
                </ComposableMap>
            </div>
        );
    }
}

export default DistrictMap;
