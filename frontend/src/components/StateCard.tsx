import { StateInstanceModel } from "../models/state.model";
import { Highlight } from "../utils/Highlight";

export interface IStateCardProps {
    state: StateInstanceModel;
    searchParam: string;
}

export function StateCard(props: IStateCardProps) {
    return (
        <>
            <div className="card-body">
                <div className="card-title h5">
                    {Highlight(props.state.state_name, props.searchParam)}
                </div>
                <div className="list-info-group-wrap ">
                    <div className="list-info-group border-bottom">
                        <div className="info-header">State Abbreviation</div>
                        <div className="info-body">
                            {Highlight(
                                props.state.state_abbrev,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Region</div>
                        <div className="info-body">
                            {Highlight(
                                props.state.census_region,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Size</div>
                        <div className="info-body">
                            {Highlight(
                                props.state.sq_miles.toLocaleString("en-US") +
                                    " sq mi",
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Political Leaning</div>
                        <div className="info-body">
                            {Highlight(
                                props.state.party_detailed,
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group border-bottom">
                        <div className="info-header">Minority Percentage</div>
                        <div className="info-body">
                            {Highlight(
                                props.state.non_white_percentage
                                    ? props.state.non_white_percentage.toFixed(
                                          0
                                      ) + "%"
                                    : "0",
                                props.searchParam
                            )}
                        </div>
                    </div>
                    <div className="list-info-group ">
                        <div className="info-header">Compactness</div>
                        <div className="info-body">
                            {Highlight(
                                props.state.state_compactness.toFixed(2),
                                props.searchParam
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
