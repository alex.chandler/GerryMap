import { useState, useRef } from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import { FaSearch } from "react-icons/fa";

export default function Header() {
    const cacheSearchTerm = localStorage.getItem("searchTerm")
        ? localStorage.getItem("searchTerm")
        : "";
    const [searchText, setSearchText] = useState<string>(
        cacheSearchTerm ? cacheSearchTerm : ""
    );
    const refSearchButton = useRef<HTMLButtonElement>(null);

    return (
        <Navbar fixed="top" bg="light" expand="md">
            <Container>
                <Navbar.Brand href="/">
                    <img
                        src="/logo.png"
                        width="150"
                        height="50"
                        className="d-inline-block align-top"
                        alt="GerryMap logo"
                    />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/about">About</Nav.Link>
                        <Nav.Link href="/states">States</Nav.Link>
                        <Nav.Link href="/districts">Districts</Nav.Link>
                        <Nav.Link href="/politicians">Politicians</Nav.Link>
                        <Nav.Link href="/faq">FAQ</Nav.Link>
                        <Nav.Link href="/visualizations">
                            Visualizations
                        </Nav.Link>
                        <Nav.Link
                            href="/provider-vis"
                            style={{
                                whiteSpace: "nowrap",
                                paddingRight: "1em",
                            }}
                        >
                            Provider Visualizations
                        </Nav.Link>
                        <Nav className="input-group">
                            <Nav className="form-outline">
                                <InputGroup data-testid="inputgroup-search-all">
                                    <FormControl
                                        aria-describedby="basic-addon1"
                                        placeholder="Search"
                                        className="form-50"
                                        value={searchText}
                                        onChange={(e) => {
                                            setSearchText(e.target.value);
                                            localStorage.setItem(
                                                "searchTerm",
                                                e.target.value
                                            );
                                        }}
                                        onKeyDown={(e) => {
                                            if (e.key === "Enter") {
                                                if (
                                                    refSearchButton &&
                                                    refSearchButton.current
                                                )
                                                    refSearchButton.current.click();
                                            }
                                        }}
                                    />
                                    <Button
                                        variant="outline-dark"
                                        id="button-search"
                                        data-testid="button-search"
                                        type="submit"
                                        href={"/search/" + searchText}
                                        ref={refSearchButton}
                                    >
                                        <FaSearch />
                                    </Button>
                                </InputGroup>
                            </Nav>
                        </Nav>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
