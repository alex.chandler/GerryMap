import React, { useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import InputGroup from "react-bootstrap/InputGroup";
import Form from "react-bootstrap/Form";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
// import FloatingLabel from "react-bootstrap/FloatingLabel";
// import Collapse from "react-bootstrap/Collapse";
import "../styles/TableNavbar.scss";
import { HiSortAscending, HiSortDescending } from "react-icons/hi";

interface ITableNavbarProps {
    dropdowns: ITableDropdownItem[];
    sorts: string[];
    searchPlaceholder: string;
    changeSortParam: (s: string) => void;
    changeSortAscending: (b: boolean) => void;
    changeFilterParams: (title: string, range: string) => void;
    removeFilterParams: (title: string) => void;
    handleSearch: (query: string) => void;
}

interface ITableDropdownItem {
    title: string;
    items: string[];
}

interface filterItem {
    title: string;
    items: string[];
    rangeMin: number;
    rangeMax: number;
}

export default function TableNavbar(props: ITableNavbarProps) {
    const [dropdowns] = useState<ITableDropdownItem[]>(props.dropdowns);
    const [sorts] = useState<string[]>(props.sorts);
    const [searchValue, setSearchValue] = useState<string>("");
    const [filterClose, setFilterClose] = useState<boolean>(true);
    const [sortAscending, setSortAscending] = useState<boolean>(true);
    const [sortAttribute, setSortAttribute] = useState("Name");
    const tmpDropdowns = [];
    for (let i = 0; i < props.dropdowns.length; i++) {
        tmpDropdowns.push({
            title: props.dropdowns[i].title,
            items: [],
            rangeMin: -1,
            rangeMax: -1,
        });
    }
    const [filterAttributes, setFilterAttributes] =
        useState<filterItem[]>(tmpDropdowns);

    function triggerFilterChange(
        filterIndex: number,
        itemIndex: number,
        itemArray: string[],
        filterArray: filterItem
    ) {
        if (itemArray.length == 0) {
            props.removeFilterParams(dropdowns[filterIndex].title);
        } else {
            let res_min = -1;
            let res_max = -1;
            let returnRange = true;
            // let returnString = "";
            for (let i = 0; i < itemArray.length; i++) {
                if (itemArray[i].includes("-")) {
                    const string = itemArray[i].replaceAll(",", "");
                    const arr = string.split("-");
                    const newMin = Number(arr[0]);
                    const newMax = Number(arr[1]);
                    if (res_min === -1 || newMin < res_min) res_min = newMin;
                    if (res_max === -1 || newMax > res_max) res_max = newMax;
                } else if (itemArray[i] === "range") {
                    if (res_min === -1 || filterArray.rangeMin < res_min)
                        res_min = filterArray.rangeMin;
                    if (res_max === -1 || filterArray.rangeMax > res_max)
                        res_max = filterArray.rangeMax;
                } else {
                    //"democratic"
                    returnRange = false;
                    if (
                        itemArray.length === dropdowns[filterIndex].items.length
                    ) {
                        props.removeFilterParams(dropdowns[filterIndex].title);
                    } else {
                        props.changeFilterParams(
                            filterArray.title,
                            itemArray[i]
                        );
                    }
                    break;
                }
            }
            if (returnRange && res_min !== -1 && res_max !== -1) {
                props.changeFilterParams(
                    filterArray.title,
                    res_min + "-" + res_max
                );
            }
        }
    }

    function toggleFilterCheck(filterIndex: number, itemIndex: number) {
        setFilterAttributes((prevState) => {
            const itemArray = [...prevState[filterIndex].items];
            const filterArray = { ...prevState[filterIndex] };
            const index = itemArray.indexOf(
                dropdowns[filterIndex].items[itemIndex]
            );
            if (index !== -1) {
                itemArray.splice(index, 1);
            } else {
                itemArray.push(dropdowns[filterIndex].items[itemIndex]);
            }
            triggerFilterChange(filterIndex, itemIndex, itemArray, filterArray);

            return prevState.map((filterItem, idx) => {
                if (filterIndex === idx) {
                    return {
                        ...prevState[filterIndex],
                        items: [...itemArray],
                    };
                } else {
                    return filterItem;
                }
            });
        });
    }

    function changeRange(
        filterIndex: number,
        itemIndex: number,
        min: string | undefined,
        max: string | undefined
    ) {
        //const re = /^\d*$/;
        const re = /^.*/;
        const input = min !== undefined ? min : max;
        if (input === "" || (input !== undefined && re.test(input))) {
            const newNumber = input === "" ? -1 : Number(input);
            setFilterAttributes((prevState) => {
                const itemArray = [...prevState[filterIndex].items];
                let filterArray = { ...prevState[filterIndex] };
                if (min !== undefined) {
                    filterArray = {
                        ...prevState[filterIndex],
                        rangeMin: newNumber,
                    };
                } else {
                    filterArray = {
                        ...prevState[filterIndex],
                        rangeMax: newNumber,
                    };
                }
                triggerFilterChange(
                    filterIndex,
                    itemIndex,
                    itemArray,
                    filterArray
                );

                return prevState.map((filterItem, idx) => {
                    if (filterIndex === idx) {
                        return filterArray;
                    } else {
                        return filterItem;
                    }
                });
            });
        }
    }

    return (
        <>
            <Col>
                <InputGroup data-testid="group-search">
                    <Form.Control
                        placeholder={props.searchPlaceholder}
                        aria-label="search-bar"
                        type="text"
                        value={searchValue}
                        onChange={(e) => {
                            setSearchValue(e.target.value);
                        }}
                        onKeyDown={(e) => {
                            if (e.key === "Enter")
                                props.handleSearch(searchValue);
                        }}
                    />
                    <Button
                        variant="primary"
                        onClick={() => props.handleSearch(searchValue)}
                    >
                        Search
                    </Button>
                </InputGroup>
            </Col>
            <Col md="auto">
                <InputGroup
                    data-testid="group-filter-sort"
                    className="flex-nowrap"
                >
                    <Dropdown
                        id="dropdown-filter"
                        autoClose="outside"
                        onClick={() => setFilterClose(!filterClose)}
                    >
                        <Dropdown.Toggle
                            variant="outline-secondary"
                            id="dropdown-filter-button"
                        >
                            Filter By
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            {dropdowns.map((dropdown, index) => (
                                <>
                                    <Dropdown.Header key={dropdown.title}>
                                        {dropdown.title}
                                    </Dropdown.Header>
                                    {dropdown.items.map((item, itemIndex) => (
                                        <Dropdown.Item
                                            data-testid={
                                                "dropdown-filter-" +
                                                (10 * index + itemIndex)
                                            }
                                            href="#"
                                            onClick={() => {
                                                toggleFilterCheck(
                                                    index,
                                                    itemIndex
                                                );
                                            }}
                                            key={
                                                dropdown.title + "_" + itemIndex
                                            }
                                        >
                                            {item === "range" ? (
                                                <Form>
                                                    <Row className="row-range">
                                                        <Col md="auto">
                                                            <Form.Check
                                                                key={Math.random()}
                                                                type="checkbox"
                                                                id="default-checkbox"
                                                                readOnly
                                                                checked={filterAttributes[
                                                                    index
                                                                ].items.includes(
                                                                    item
                                                                )}
                                                            />
                                                        </Col>
                                                        <Col className="px-0">
                                                            <Form.Control
                                                                size="sm"
                                                                type="number"
                                                                placeholder="Min."
                                                                value={
                                                                    filterAttributes[
                                                                        index
                                                                    ]
                                                                        .rangeMin ===
                                                                    -1
                                                                        ? undefined
                                                                        : filterAttributes[
                                                                              index
                                                                          ]
                                                                              .rangeMin
                                                                }
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    changeRange(
                                                                        index,
                                                                        itemIndex,
                                                                        e
                                                                            .currentTarget
                                                                            .value,
                                                                        undefined
                                                                    );
                                                                }}
                                                                onClick={(
                                                                    e
                                                                ) => {
                                                                    e.stopPropagation();
                                                                }}
                                                            />
                                                        </Col>
                                                        <Col className="px-1">
                                                            <Form.Control
                                                                size="sm"
                                                                type="number"
                                                                placeholder="Max."
                                                                value={
                                                                    filterAttributes[
                                                                        index
                                                                    ]
                                                                        .rangeMax ===
                                                                    -1
                                                                        ? undefined
                                                                        : filterAttributes[
                                                                              index
                                                                          ]
                                                                              .rangeMax
                                                                }
                                                                onChange={(
                                                                    e
                                                                ) => {
                                                                    changeRange(
                                                                        index,
                                                                        itemIndex,
                                                                        undefined,
                                                                        e
                                                                            .currentTarget
                                                                            .value
                                                                    );
                                                                }}
                                                                onClick={(
                                                                    e
                                                                ) => {
                                                                    e.stopPropagation();
                                                                }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                </Form>
                                            ) : (
                                                <Form>
                                                    <Form.Check
                                                        key={Math.random()}
                                                        type="checkbox"
                                                        id={itemIndex.toString()}
                                                        label={item}
                                                        readOnly
                                                        checked={filterAttributes[
                                                            index
                                                        ].items.includes(item)}
                                                    />
                                                </Form>
                                            )}
                                        </Dropdown.Item>
                                    ))}
                                    {index !== dropdowns.length - 1 ? (
                                        <Dropdown.Divider />
                                    ) : (
                                        <></>
                                    )}
                                </>
                            ))}
                        </Dropdown.Menu>
                    </Dropdown>

                    <DropdownButton
                        variant="outline-secondary"
                        title="Sort By"
                        id="dropdown-sort-button"
                        data-testid="dropdown-sort-button"
                        align="end"
                    >
                        {sorts.map((item, idx) => (
                            <Dropdown.Item
                                data-testid={"dropdown-sort-" + idx}
                                href="#"
                                key={item + idx}
                                active={item === sortAttribute}
                                onClick={() => {
                                    setSortAttribute(item);
                                    props.changeSortParam(item);
                                }}
                            >
                                {item}
                            </Dropdown.Item>
                        ))}
                    </DropdownButton>
                    <Button
                        data-testid="ascending-button"
                        variant="outline-secondary"
                        onClick={() => {
                            const cur_ascending = sortAscending;
                            setSortAscending(!cur_ascending);
                            props.changeSortAscending(!cur_ascending);
                        }}
                    >
                        {sortAscending && (
                            <HiSortAscending data-testid="ascending-icon" />
                        )}
                        {!sortAscending && (
                            <HiSortDescending data-testid="descending-icon" />
                        )}
                    </Button>
                </InputGroup>
            </Col>
        </>
    );
}
