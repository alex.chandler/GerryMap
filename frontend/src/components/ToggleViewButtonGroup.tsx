import * as React from "react";
import Col from "react-bootstrap/Col";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import ToggleButton from "react-bootstrap/ToggleButton";
import { HiViewList, HiViewGrid } from "react-icons/hi";

export interface IToggleViewButtonGroupProps {
    changeState: (check: boolean) => void;
    tableView: boolean;
}

export default function ToggleViewButtonGroup(
    props: IToggleViewButtonGroupProps
) {
    return (
        <Col md="auto">
            <ButtonGroup className="mb-2">
                <ToggleButton
                    id="toggle-card-view"
                    data-testid="toggle-card-view"
                    type="radio"
                    variant="outline-primary"
                    value={props.tableView ? "0" : "1"}
                    checked={!props.tableView}
                    onChange={() => props.changeState(false)}
                >
                    <HiViewGrid />
                </ToggleButton>
                <ToggleButton
                    id="toggle-table-view"
                    data-testid="toggle-table-view"
                    type="radio"
                    variant="outline-primary"
                    value={props.tableView ? "1" : "0"}
                    checked={props.tableView}
                    onChange={() => props.changeState(true)}
                >
                    <HiViewList />
                </ToggleButton>
            </ButtonGroup>
        </Col>
    );
}
