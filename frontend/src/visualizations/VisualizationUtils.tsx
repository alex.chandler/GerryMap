import * as d3 from "d3";
import { getQueryString } from "../utils/Query";
import axios from "axios";

export function LoadingVisualization() {
    return <div>Loading visualization, please be patient...</div>;
}

export const height = 500;
export const width = 975;

export const getPagesToFetch = async (endpoint: string, pageSize = 10000) => {
    const { data: checkMeta } = await axios.get(
        endpoint + getQueryString({ limit: 1, page: 1 })
    );
    const total = checkMeta.meta_total;
    return Array.from(
        {
            length:
                Math.floor(total / pageSize) + (total % pageSize == 0 ? 0 : 1),
        },
        (x, i) => i + 1
    ); // generate array (1, 2, 3... pagesToFetch)
};

export interface AuthorModel {
    author_work_count: number;
    author_name: string;
    author_country_id: number;
}

export interface CountryModel {
    country_id: number;
    country_name: string;
    country_region: string;
}

// legends based on https://observablehq.com/@d3/color-legend

// creates a legend for a scaleQuantize color range
export function ColorScaleLegend(
    color: d3.ScaleQuantize<string, never>,
    svg: d3.Selection<null, undefined, null, undefined>,
    title: string
) {
    const tickSize = 6;
    const width = 320;
    const height = 44 + tickSize;
    const marginTop = 18;
    const marginRight = 0;
    const marginBottom = 16 + tickSize;
    const marginLeft = 0;
    const ticks = width / 64;

    // clear the svg
    svg.selectAll("*").remove();

    svg.attr("width", width)
        .attr("height", height)
        .attr("viewBox", [0, 0, width, height])
        .attr(
            "style",
            "width: 450px; height: 100%; display: block; overflow: visible"
        );

    const thresholds = color.thresholds();
    const thresholdFormat = (d: d3.NumberValue, i: number) => {
        return thresholds[i].toFixed(2);
    };

    const x = d3
        .scaleLinear()
        .domain([-1, color.range().length - 1])
        .rangeRound([marginLeft, width - marginRight]);

    svg.append("g")
        .selectAll("rect")
        .data(color.range())
        .join("rect")
        .attr("x", (d, i) => x(i - 1))
        .attr("y", marginTop)
        .attr("width", (d, i) => x(i) - x(i - 1))
        .attr("height", height - marginTop - marginBottom)
        .attr("fill", (d) => d);

    const tickValues = d3.range(thresholds.length);
    const tickAdjust = (
        g: d3.Selection<SVGGElement, undefined, null, undefined>
    ) =>
        g.selectAll(".tick line").attr("y1", marginTop + marginBottom - height);

    svg.append("g")
        .attr("transform", `translate(0,${height - marginBottom})`)
        .call(
            d3
                .axisBottom(x as unknown as d3.AxisScale<d3.NumberValue>)
                .ticks(ticks)
                .tickFormat(thresholdFormat)
                .tickSize(tickSize)
                .tickValues(tickValues)
        )
        .call(tickAdjust)
        .call((g) => g.select(".domain").remove())
        .call((g) =>
            g
                .append("text")
                .attr("x", marginLeft)
                .attr("y", marginTop + marginBottom - height - 6)
                .attr("fill", "currentColor")
                .attr("text-anchor", "start")
                .attr("font-weight", "bold")
                .attr("class", "title")
                .text(title)
        );
}

// creates a legend for a discrete value => color mapping
export function SwatchLegend(legendItems: Map<string, string>) {
    const swatchSize = 15;
    const swatchWidth = swatchSize * 2;
    const swatchHeight = swatchSize;
    const marginLeft = 0;

    const getStyle = (color: string) => {
        return {
            display: "inline-flex",
            alignItems: "center",
            marginRight: "1em",
            width: swatchWidth + "px",
            height: swatchHeight + "px",
            background: color,
        };
    };

    return (
        <div
            style={{
                display: "flex",
                alignItems: "center",
                marginLeft: marginLeft + "px",
                minHeight: "33px",
                font: "12px sans-serif",
            }}
        >
            {Array.from(legendItems).map(([key, value]) => (
                <div
                    key={key}
                    style={{
                        display: "flex",
                        flexFlow: "row nowrap",
                        alignItems: "center",
                        margin: "0 10px",
                        minHeight: "33px",
                        font: "12px sans-serif",
                        fontWeight: "bold",
                    }}
                >
                    <div style={getStyle(value)} />
                    <div>{key}</div>
                </div>
            ))}
        </div>
    );
}
