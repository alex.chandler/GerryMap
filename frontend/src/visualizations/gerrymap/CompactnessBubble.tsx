import React, { useEffect, useState, useRef } from "react";
import * as d3 from "d3";
import axios from "axios";
import {
    height,
    width,
    LoadingVisualization,
    SwatchLegend,
} from "../VisualizationUtils";
import { getQueryString } from "../../utils/Query";
import { StateInstanceModel } from "../../models/state.model";
import "../../styles/Visualizations.scss";
import { Row } from "react-bootstrap";
import { RED, BLUE, rgbToHex } from "../../styles/Colors";

const CompactnessBubble: React.FunctionComponent = () => {
    const [stateData, setStateData] = useState<StateInstanceModel[]>();
    const [loading, setLoading] = useState(true);

    // fetch data from backend api
    useEffect(() => {
        axios
            .get(
                process.env.REACT_APP_API_ENDPOINT +
                    "/states" +
                    getQueryString({ page_size: 50 })
            )
            .then((resp) => resp.data)
            .then((states) => {
                setStateData(states.data);
            });
    }, []);

    const chart = useRef(null);
    const repColor = rgbToHex(RED, 0.5);
    const demColor = rgbToHex(BLUE, 0.5);

    const legendItems = new Map([
        ["REPUBLICAN", repColor],
        ["DEMOCRAT", demColor],
    ]);

    useEffect(() => {
        if (!stateData) return;

        // bubble chart d3 setup referenced from https://observablehq.com/@d3/bubble-chart
        // with some configs referenced from https://github.com/weknowinc/react-bubble-chart-d3

        // I know there's a react-bubble-chart-d3 package (GH link above), but
        // I wanted to make sizes inversely proportional to value and this
        // level of customizability wasn't supported

        // for sizing svg and circles
        const localWidth = width * 3;
        const localHeight = height * 3;

        const padding = 3; // padding between circles
        const margin = 1; // default margins
        const marginTop = margin; // top margin, in pixels
        const marginRight = margin; // right margin, in pixels
        const marginBottom = margin; // bottom margin, in pixels
        const marginLeft = margin; // left margin, in pixels
        const fill = "#999"; // a static fill color, if no group channel is specified
        const stroke = "white"; // a static stroke around the bubbles
        const strokeWidth = 1; // the stroke width around the bubbles, if any
        const strokeOpacity = 0.4; // the stroke opacity around the bubbles, if any

        // font sizes
        const valueSize = 25;
        const labelSize = 15;

        const V = d3.map(stateData, (d) => 1 - d.state_compactness); // values
        const G = d3.map(stateData, (d) => d.party_detailed); // groups (for coloring)
        const I = d3.range(V.length).filter((i) => V[i] > 0); // indices of bubbles
        const color = (group: string) => {
            const c = legendItems.get(group);
            if (c === undefined) {
                return fill;
            }
            return c;
        };

        // Compute labels and titles.
        const L = d3.map(stateData, (d) => d.state_name);
        const T = d3.map(stateData, (d) => d.state_name);

        // Compute layout: create a 1-deep hierarchy, and pack it.
        const root = d3
            .pack()
            .size([
                localWidth - marginLeft - marginRight,
                localHeight - marginTop - marginBottom,
            ])
            .padding(padding)(
            d3
                .hierarchy({ children: I })
                .sum((i) => Math.pow(V[i as unknown as number], 1.5))
        );

        const svg = d3.select(chart.current);
        svg.selectAll("*").remove();
        svg.attr("width", localWidth * 0.75)
            .attr("height", localHeight * 0.75)
            .attr("viewBox", [
                localWidth * 0.1,
                -marginTop,
                localWidth,
                localHeight,
            ])
            .attr("style", "max-width: 100%; height: auto; height: intrinsic;")
            .attr("fill", "currentColor")
            .attr("font-size", 12)
            .attr("color", "black")
            .attr("font-family", "sans-serif")
            .attr("text-anchor", "middle");

        const bubbleChart = svg
            .append("g")
            .attr("class", "bubble-chart")
            .attr(
                "transform",
                () => "translate(" + width * -0.05 + "," + width * -0.01 + ")"
            );

        const leaf = bubbleChart
            .selectAll("a")
            .data(root.leaves())
            .join("a")
            .attr("transform", (d) => `translate(${d.x},${d.y})`);

        leaf.append("circle")
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .attr("stroke-opacity", strokeOpacity)
            .attr(
                "fill",
                G
                    ? (d) => color(G[d.data as number])
                    : fill == null
                    ? "none"
                    : fill
            )
            .attr("r", (d) => d.r);

        if (T) leaf.append("title").text((d) => T[d.data as number]);

        if (L) {
            // A unique identifier for clip paths (to avoid conflicts).
            const uid = `O-${Math.random().toString(16).slice(2)}`;

            leaf.append("clipPath")
                .attr("id", (d) => `${uid}-clip-${d.data}`)
                .append("circle")
                .attr("r", (d) => d.r);

            // put label text on bubbles
            leaf.append("text")
                .style("font-size", `${labelSize}px`)
                .style("font-weight", "bold")
                .attr("clip-path", (d) => "url(#clip-" + d.data + ")")
                .selectAll("tspan")
                .data((d) => `${L[d.data as number]}`.split(/\n/g))
                .join("tspan")
                .attr("x", 0)
                .attr("y", () => labelSize / 2)
                .attr("fill-opacity", (d, i, D) =>
                    i === D.length - 1 ? 0.7 : null
                )
                .text((d) => d);

            // put values on bubbles
            leaf.append("text")
                .style("font-size", `${valueSize}px`)
                .style("font-weight", "bold")
                .attr("clip-path", (d) => "url(#clip-" + d.data + ")")
                .selectAll("tspan")
                .data((d) =>
                    `${(1 - V[d.data as number]).toFixed(2)}`.split(/\n/g)
                )
                .join("tspan")
                .attr("x", 0)
                .attr("y", () => -valueSize * 0.5)
                .attr("fill-opacity", (d, i, D) =>
                    i === D.length - 1 ? 0.7 : null
                )
                .text((d) => d);
        }

        setLoading(false);
    }, [stateData]);

    return (
        <>
            <Row>
                <h5>States by average compactness</h5>
            </Row>
            <Row className="chart-container">
                <div>
                    {loading && <LoadingVisualization />}
                    <svg ref={chart}></svg>
                    {!loading && SwatchLegend(legendItems)}
                </div>
            </Row>
        </>
    );
};

export default CompactnessBubble;
