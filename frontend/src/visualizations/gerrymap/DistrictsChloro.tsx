import { useEffect, useState, useRef } from "react";
import * as d3 from "d3";
import { feature, mesh } from "topojson-client";
import { Point } from "topojson-specification";
import { Feature, MultiLineString, GeoJsonProperties } from "geojson";
import axios from "axios";
import {
    height,
    width,
    LoadingVisualization,
    ColorScaleLegend,
} from "../VisualizationUtils";
import { getQueryString } from "../../utils/Query";
import { DistrictInstanceModel } from "../../models/district.model";
import "../../styles/Visualizations.scss";
import { Row } from "react-bootstrap";

interface FeatureCollection {
    type: string;
    features: Feature<Point, GeoJsonProperties>[];
}

export default function DistrictsChloro() {
    const [districtMap, setDistrictMap] = useState<FeatureCollection>();
    const [stateMap, setStateMap] = useState<MultiLineString>();
    const [loading, setLoading] = useState(true);
    const [districtData, setDistrictData] = useState<DistrictInstanceModel[]>();

    const d3Map = useRef(null);
    const d3Legend = useRef(null);

    // fetch US map of districts
    useEffect(() => {
        fetch(
            "https://raw.githubusercontent.com/fang-helen/gerrymap_json/master/usa_sm.json"
        )
            .then((resp) => resp.json())
            .then((districtsmap) => {
                const f = feature(
                    districtsmap,
                    districtsmap.objects.districts
                ) as unknown as FeatureCollection;
                setDistrictMap(f);
            });
    }, []);

    // fetch US map of states, to use as borders
    useEffect(() => {
        fetch("https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json")
            .then((resp) => resp.json())
            .then((map) => {
                setStateMap(mesh(map, map.objects.states, (a, b) => a !== b));
            });
    }, []);

    // fetch data from our backend API
    useEffect(() => {
        axios
            .get(
                process.env.REACT_APP_API_ENDPOINT +
                    "/districts" +
                    getQueryString({ page_size: 435 })
            )
            .then((resp) => resp.data)
            .then((districts) => {
                setDistrictData(districts.data);
            });
    }, []);

    // hook for rendering the d3 map
    useEffect(() => {
        // chloropleth based on https://observablehq.com/@d3/choropleth and
        // d3 setup referenced from https://gitlab.com/RG8452/campus-catalog/-/tree/main

        if (!districtMap || !districtData || !stateMap) return;

        const unknown = "#ccc"; // fill color for missing data
        const stroke = "white"; // stroke color for borders
        const strokeLinecap = "round"; // stroke line cap for borders
        const strokeLinejoin = "round"; // stroke line join for borders
        const strokeWidth = 1; // stroke width for borders
        const strokeOpacity = null; // stroke opacity for borders
        const projection = d3.geoAlbersUsa();

        // ids from data array
        const N = d3.map(
            districtData,
            (d) => d.state_abbrev + "_" + d.district_num
        );
        // values from data array
        const V = d3
            .map(districtData, (d) => d.district_compactness)
            .map((d) => (d == null ? NaN : +d));
        // map of id -> index of N from data array
        const indexes = new d3.InternMap(N.map((id, i) => [id, i]));
        // map of ids from feature array
        const If = d3.map(districtMap.features, (d) => d.id) as string[];

        // Construct color scales.
        const range = d3.schemeBlues[9].slice(0).reverse();
        const domain = d3.extent(V) as [number, number];
        const colorScale = d3.scaleQuantize(domain, range);
        colorScale.unknown(unknown);

        // Compute titles.
        const format = colorScale.tickFormat(100, undefined);
        const title = (f: Feature<Point, GeoJsonProperties>, i: number) =>
            `${f.properties ? f.properties.name : "??"}\n${format(V[i])}`;

        // Retrieve our map via the ref and clear the svg,
        // else it will add on top of it
        const svg = d3.select(d3Map.current);
        svg.selectAll("*").remove();

        // for projecting the data
        const projectionpath = d3.geoPath(projection);

        svg.attr("width", width)
            .attr("height", height)
            .attr("viewBox", [0, 0, width, height])
            .attr("style", "width: 100%; height: 100%;");

        svg.append("g")
            .selectAll("path")
            .data(districtMap.features)
            .join("path")
            .attr("fill", (d, i) => {
                const key = indexes.get(If[i]);
                return key !== undefined ? colorScale(V[key]) : unknown;
            })
            .attr("d", projectionpath)
            .append("title")
            .text((d, i) => {
                const index = indexes.get(If[i]);
                return index ? title(d, index) : "??";
            });

        svg.append("path")
            .attr("pointer-events", "none")
            .attr("fill", "none")
            .attr("stroke", stroke)
            .attr("stroke-linecap", strokeLinecap)
            .attr("stroke-linejoin", strokeLinejoin)
            .attr("stroke-width", strokeWidth)
            .attr("stroke-opacity", strokeOpacity)
            .attr("d", projectionpath(stateMap));

        // construct the legend
        ColorScaleLegend(
            colorScale,
            d3.select(d3Legend.current) as d3.Selection<
                null,
                undefined,
                null,
                undefined
            >,
            "Compactness"
        );

        // At this point, we've retrieved our data and drawn
        // our SVG. We have no need for a loading string any more.
        setLoading(false);
    }, [districtData, districtMap, stateMap]);

    return (
        <>
            <Row>
                <h5>Compactness of districts</h5>
            </Row>
            <Row className="chart-container">
                <div className="chart">
                    {loading && <LoadingVisualization />}
                    <svg ref={d3Map}></svg>
                </div>
                <div
                    style={{
                        margin: "10px",
                        padding: "10px",
                    }}
                >
                    <svg ref={d3Legend}></svg>
                </div>
            </Row>
        </>
    );
}
