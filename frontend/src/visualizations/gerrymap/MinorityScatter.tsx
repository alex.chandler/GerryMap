import { useEffect, useState, useRef } from "react";
import * as d3 from "d3";
import axios from "axios";
import {
    height,
    width,
    LoadingVisualization,
    SwatchLegend,
} from "../VisualizationUtils";
import { getQueryString } from "../../utils/Query";
import { DistrictInstanceModel } from "../../models/district.model";
import "../../styles/Visualizations.scss";
import { Row } from "react-bootstrap";
import { RED, BLUE, rgbToHex } from "../../styles/Colors";

export default function MinorityScatter() {
    const [loading, setLoading] = useState(true);
    const [districtData, setDistrictData] = useState<DistrictInstanceModel[]>();

    // fetch data from our backend API
    useEffect(() => {
        axios
            .get(
                process.env.REACT_APP_API_ENDPOINT +
                    "/districts" +
                    getQueryString({ page_size: 435 })
            )
            .then((resp) => resp.data)
            .then((districts) => {
                setDistrictData(districts.data);
            });
    }, []);

    const d3Chart = useRef(null);
    const repColor = rgbToHex(RED, 0.6);
    const demColor = rgbToHex(BLUE, 0.6);

    // hook for rendering the d3 chart
    useEffect(() => {
        // scatterplot based on https://observablehq.com/@d3/scatterplot

        if (!districtData) return;

        const stroke = "white";
        const strokeWidth = 1;

        // split data by party so we can add series in separate colors
        const repSeries = d3.filter(
            districtData,
            (d) => d.party_detailed == "REPUBLICAN"
        );
        const demSeries = d3.filter(
            districtData,
            (d) => d.party_detailed == "DEMOCRAT"
        );

        let X = d3.map(districtData, (d) => d.non_white_percentage);
        let Y = d3.map(districtData, (d) => d.district_compactness);

        // Compute default domains.
        const xDomain = d3.extent(X) as [number, number];
        const yDomain = d3.extent(Y) as [number, number];

        // all values in pixels
        const r = 6; // dot radius
        const marginTop = 50;
        const marginRight = 50;
        const marginBottom = 50;
        const marginLeft = 50;
        const inset = r * 2;
        const xRange = [marginLeft + inset, width - marginRight - inset];
        const yRange = [height - marginBottom - inset, marginTop + inset];

        // Construct scales and axes.
        const xScale = d3.scaleLinear(xDomain, xRange);
        const yScale = d3.scaleLinear(yDomain, yRange);
        const xAxis = d3.axisBottom(xScale).ticks(width / 80, null);
        const yAxis = d3.axisLeft(yScale).ticks(height / 50, null);
        const xLabel = "Minority percentage";
        const yLabel = "District compactness";

        const svg = d3.select(d3Chart.current);
        svg.selectAll("*").remove();

        svg.attr("width", width)
            .attr("height", height)
            .attr("viewBox", [0, 0, width, height])
            .attr("style", "max-width: 100%; height: auto; height: intrinsic;");

        // construct x axis
        svg.append("g")
            .attr("transform", `translate(0,${height - marginBottom})`)
            .call(xAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick line")
                    .clone()
                    .attr("y2", marginTop + marginBottom - height)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", width)
                    .attr("y", marginBottom - 4)
                    .attr("font-size", "2em")
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "end")
                    .text(xLabel)
            );

        // construct y axis
        svg.append("g")
            .attr("transform", `translate(${marginLeft},0)`)
            .call(yAxis)
            .call((g) => g.select(".domain").remove())
            .call((g) =>
                g
                    .selectAll(".tick line")
                    .clone()
                    .attr("x2", width - marginLeft - marginRight)
                    .attr("stroke-opacity", 0.1)
            )
            .call((g) =>
                g
                    .append("text")
                    .attr("x", -marginLeft)
                    .attr("y", 30)
                    .attr("font-size", "2em")
                    .attr("fill", "currentColor")
                    .attr("text-anchor", "start")
                    .text(yLabel)
            );

        // construct republicans series
        X = d3.map(repSeries, (d) => d.non_white_percentage);
        Y = d3.map(repSeries, (d) => d.district_compactness);
        let indexes = d3
            .range(X.length)
            .filter((i) => !isNaN(X[i]) && !isNaN(Y[i]));
        svg.append("g")
            .attr("fill", repColor)
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .selectAll("circle")
            .data(indexes)
            .join("circle")
            .attr("cx", (i) => xScale(X[i]))
            .attr("cy", (i) => yScale(Y[i]))
            .attr("r", r);

        // construct democrat series
        X = d3.map(demSeries, (d) => d.non_white_percentage);
        Y = d3.map(demSeries, (d) => d.district_compactness);
        indexes = d3
            .range(X.length)
            .filter((i) => !isNaN(X[i]) && !isNaN(Y[i]));
        svg.append("g")
            .attr("fill", demColor)
            .attr("stroke", stroke)
            .attr("stroke-width", strokeWidth)
            .selectAll("circle")
            .data(indexes)
            .join("circle")
            .attr("cx", (i) => xScale(X[i]))
            .attr("cy", (i) => yScale(Y[i]))
            .attr("r", r);

        // At this point, we've retrieved our data and drawn
        // our SVG. We have no need for a loading string any more.
        setLoading(false);
    }, [districtData]);

    const legendItems = new Map([
        ["REPUBLICAN", repColor],
        ["DEMOCRAT", demColor],
    ]);

    return (
        <>
            <Row>
                <h5>Compactness and minority percentage</h5>
            </Row>
            <Row className="chart-container">
                <div className="chart">
                    {loading && <LoadingVisualization />}
                    <svg ref={d3Chart}></svg>
                    {!loading && SwatchLegend(legendItems)}
                </div>
            </Row>
        </>
    );
}
