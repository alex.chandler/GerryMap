import axios from "axios";
import { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import {
    LineChart,
    Line,
    CartesianGrid,
    XAxis,
    YAxis,
    Legend,
    Tooltip,
    ResponsiveContainer,
} from "recharts";
import { interpolateRainbow } from "d3";

import {
    getPagesToFetch,
    LoadingVisualization,
    CountryModel,
} from "../VisualizationUtils";
import { getQueryString } from "../../utils/Query";
import { darkenColorStr } from "../../styles/Colors";

interface chartSeries {
    decade: number;
    [key: string]: number;
}

export default function BookPublishedDecades({
    countries = undefined,
}: {
    countries: Map<number, CountryModel> | undefined;
}) {
    const [data, setData] = useState<chartSeries[]>();
    const [regions, setRegions] = useState<string[]>();
    const endpoint = "https://api.bookrus.me/books";

    useEffect(() => {
        if (!countries) {
            return;
        }

        const fetchData = async () => {
            const pageSize = 10000;
            const pagesToFetch = await getPagesToFetch(endpoint, pageSize);

            // there are way too many books, split up the api calls
            // construct map of decade => (region => count) from each call
            const decadeCounts: Map<number, Map<string, number>>[] = [];
            await Promise.all(
                // fetch each page of books
                pagesToFetch.map(async (pageNum: number) => {
                    try {
                        const { data: books } = await axios.get(
                            endpoint +
                                getQueryString({
                                    limit: pageSize,
                                    sort: "book_id",
                                    page: pageNum,
                                })
                        );

                        // get each books' published decade and region
                        const decadeCountLocal = new Map<
                            number,
                            Map<string, number>
                        >();
                        books.data.forEach(
                            (book: {
                                book_published: string | null;
                                book_country_id: number;
                            }) => {
                                const published = book.book_published;
                                // ignore the books that give errors
                                if (published != null) {
                                    const publishedYear = parseInt(
                                        published
                                            .replaceAll("?", "0")
                                            .split("-")[0]
                                    );
                                    const publishedCountry = countries.get(
                                        book.book_country_id
                                    );

                                    // year must be parseable and have 4 digits,
                                    // and country must be valid
                                    if (
                                        !isNaN(publishedYear) &&
                                        publishedYear.toString().length == 4 &&
                                        publishedCountry
                                    ) {
                                        const region =
                                            publishedCountry.country_region;
                                        const publishedDecade =
                                            publishedYear -
                                            (publishedYear % 10);

                                        // count the book in the correct location
                                        // based on decade...
                                        let regionMap =
                                            decadeCountLocal.get(
                                                publishedDecade
                                            );
                                        if (regionMap === undefined) {
                                            regionMap = new Map<
                                                string,
                                                number
                                            >();
                                            decadeCountLocal.set(
                                                publishedDecade,
                                                regionMap
                                            );
                                        }
                                        // ...and on region
                                        let prevCount = regionMap.get(region);
                                        if (prevCount === undefined) {
                                            prevCount = 1;
                                        } else {
                                            prevCount++;
                                        }
                                        regionMap.set(region, prevCount);
                                    }
                                }
                            }
                        );
                        decadeCounts.push(decadeCountLocal);
                    } catch (exception) {
                        console.error(exception);
                    }
                })
            );

            // now merge all separate decades maps into a single map
            const decadeCountsMerged = new Map<number, Map<string, number>>();
            // also count the regions we have represented across this data
            const chartRegions = new Set<string>();

            decadeCounts.forEach((decadesCountLocal) => {
                decadesCountLocal.forEach((regionMap, decade) => {
                    let mergedRegionMap = decadeCountsMerged.get(decade);
                    if (mergedRegionMap === undefined) {
                        mergedRegionMap = new Map<string, number>();
                        decadeCountsMerged.set(decade, mergedRegionMap);
                    }
                    regionMap.forEach((count, region) => {
                        chartRegions.add(region);
                        let prevCount = mergedRegionMap?.get(region);
                        if (prevCount === undefined) {
                            prevCount = count;
                        } else {
                            prevCount += count;
                        }
                        mergedRegionMap?.set(region, prevCount);
                    });
                });
            });

            // convert to json objects to be represented in the LineChart
            const chartObjects: chartSeries[] = [];
            decadeCountsMerged.forEach((regionMap, decade) => {
                // 1 json object for each decade
                const chartData: chartSeries = {
                    decade: decade,
                };
                // populate the json object with regions
                regionMap.forEach((count, region) => {
                    chartData[region] = count;
                });
                // ensure we have a value for all regions in each json object
                chartRegions.forEach((region) => {
                    if (chartData[region] === undefined) {
                        chartData[region] = 0;
                    }
                });
                chartObjects.push(chartData);
            });
            chartObjects.sort(
                (a: chartSeries, b: chartSeries) => a.decade - b.decade
            );

            setData(chartObjects);
            setRegions(Array.from(chartRegions));
        };

        fetchData();
    }, [countries]);

    return (
        <>
            <Row>
                <h5>Number of books published over time by region</h5>
            </Row>
            <Row className="chart-container">
                {(!data || !regions) && <LoadingVisualization />}
                {data && regions && (
                    <div className="chart-bounded">
                        <ResponsiveContainer width="100%" height="100%">
                            <LineChart
                                width={500}
                                height={300}
                                data={data}
                                margin={{
                                    top: 5,
                                    right: 30,
                                    left: 20,
                                    bottom: 5,
                                }}
                            >
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis dataKey="decade" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                {regions.map((region, index) => (
                                    <Line
                                        type="monotone"
                                        dataKey={region}
                                        key={"line" + region}
                                        stroke={darkenColorStr(
                                            interpolateRainbow(
                                                (index + 1) / regions.length
                                            )
                                        )}
                                    />
                                ))}
                            </LineChart>
                        </ResponsiveContainer>
                    </div>
                )}
            </Row>
        </>
    );
}
