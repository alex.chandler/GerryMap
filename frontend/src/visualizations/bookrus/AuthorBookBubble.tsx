import { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import { LoadingVisualization, AuthorModel } from "../VisualizationUtils";
import { chartData } from "../../utils/Charts";
import BubbleChart from "@weknow/react-bubble-chart-d3";

export default function AuthorBookBubble({
    authors = undefined,
}: {
    authors: AuthorModel[] | undefined;
}) {
    const [data, setData] = useState<chartData[]>();

    useEffect(() => {
        if (!authors) return;

        const limit = 30;
        const allBookCounts = authors.map((author: AuthorModel) => ({
            value: author.author_work_count,
            name: author.author_name,
            label: author.author_name,
        }));
        allBookCounts.sort((a: chartData, b: chartData) => b.value - a.value);

        setData(allBookCounts.slice(0, limit));
    }, [authors]);

    return (
        <>
            <Row>
                <h5>Top-30 authors with most book publications</h5>
            </Row>
            <Row className="chart-container">
                <div>
                    {!data && <LoadingVisualization />}
                    {data && (
                        <BubbleChart
                            graph={{
                                zoom: 0.9,
                                offsetX: 0.0,
                                offsetY: 0.0,
                            }}
                            showLegend={false}
                            width={1000}
                            height={1000}
                            valueFont={{
                                family: "Arial",
                                size: 20,
                                color: "black",
                                weight: "bold",
                            }}
                            labelFont={{
                                family: "Arial",
                                size: 14,
                                color: "black",
                                weight: "bold",
                            }}
                            data={data}
                        />
                    )}
                </div>
            </Row>
        </>
    );
}
