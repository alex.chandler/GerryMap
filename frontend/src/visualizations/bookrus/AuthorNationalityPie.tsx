import { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import {
    LoadingVisualization,
    AuthorModel,
    CountryModel,
} from "../VisualizationUtils";
import { chartData } from "../../utils/Charts";
import {
    PieChart,
    Pie,
    Cell,
    Legend,
    Tooltip,
    ResponsiveContainer,
} from "recharts";
import { interpolateViridis } from "d3";
import { darkenColorStr } from "../../styles/Colors";

export default function AuthorNationalityPie({
    authors = undefined,
    countries = undefined,
}: {
    authors: AuthorModel[] | undefined;
    countries: Map<number, CountryModel> | undefined;
}) {
    const [data, setData] = useState<chartData[]>();

    useEffect(() => {
        if (!authors || !countries) {
            return;
        }

        const countryCounts = new Map<string, number>();
        authors.map((author: AuthorModel) => {
            const country = countries.get(author.author_country_id);
            if (country != undefined) {
                const prevCount = countryCounts.get(country.country_name);
                if (prevCount === undefined) {
                    countryCounts.set(country.country_name, 1);
                } else {
                    countryCounts.set(country.country_name, prevCount + 1);
                }
            }
        });

        const countryChartData: chartData[] = [];
        countryCounts.forEach((count, country) => {
            countryChartData.push({ value: count, name: country });
        });

        // show only top 15, then aggregate others into "other"
        countryChartData.sort(
            (a: chartData, b: chartData) => b.value - a.value
        );
        const limit = 15;
        const topCountries = countryChartData.slice(0, limit);
        let countOthers = 0;
        for (let i = limit; i < countryChartData.length; i++) {
            countOthers += countryChartData[i].value;
        }
        topCountries.push({ value: countOthers, name: "Other" });
        setData(topCountries);
    }, [authors, countries]);

    const CustomTooltip = ({
        active = false,
        payload = [],
    }: {
        active?: boolean;
        payload?: chartData[];
        label?: string;
    }) => {
        if (active && payload && payload.length) {
            return (
                <div className="custom-tooltip">
                    <p className="label">
                        {payload[0].name + ": " + payload[0].value}
                    </p>
                </div>
            );
        }
        return null;
    };

    return (
        <>
            <Row>
                <h5>Top-15 nationalities of book authors</h5>
            </Row>
            <Row className="chart-container">
                {!data && <LoadingVisualization />}
                {data && (
                    <div className="chart-bounded">
                        <ResponsiveContainer width="100%" height="100%">
                            <PieChart width={800} height={800}>
                                <Pie
                                    data={data}
                                    dataKey="value"
                                    nameKey="name"
                                    cx="50%"
                                    cy="50%"
                                    outerRadius="80%"
                                    labelLine={false}
                                    fill="#8884d8"
                                    paddingAngle={1}
                                >
                                    {data.map(
                                        (entry: chartData, index: number) => (
                                            <Cell
                                                fill={darkenColorStr(
                                                    interpolateViridis(
                                                        (index + 1) /
                                                            data.length
                                                    )
                                                )}
                                                key={
                                                    "pie" +
                                                    entry.name +
                                                    ":" +
                                                    entry.value
                                                }
                                            />
                                        )
                                    )}
                                </Pie>
                                <Legend />
                                <Tooltip content={<CustomTooltip />} />
                            </PieChart>
                        </ResponsiveContainer>
                    </div>
                )}
            </Row>
        </>
    );
}
