export const BLUE = [0, 0, 255];
export const RED = [255, 0, 0];
export const GRAY = [102, 102, 102];
export const LIGHTGRAY = [180, 180, 180];

function componentToHex(c: number): string {
    const hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
}

export function rgbToHex(color: number[], opacity = 1.0): string {
    return (
        "#" +
        componentToHex(color[0]) +
        componentToHex(color[1]) +
        componentToHex(color[2]) +
        componentToHex(Math.ceil(opacity * 255))
    );
}

// take a string representing a color and return each component scaled by 0.8
export function darkenColorStr(rgbStr: string) {
    let rgb;
    if (rgbStr.indexOf("#") >= 0) {
        // hex
        const regex = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
        const result = regex.exec(rgbStr);
        if (!result) {
            return rgbStr;
        }
        rgb = [
            parseInt(result[1], 16),
            parseInt(result[2], 16),
            parseInt(result[3], 16),
        ];
    } else if (rgbStr.indexOf("rgb") >= 0) {
        // rgb(#, #, #)
        const parsedRgb = rgbStr.split("(")[1].split(")")[0].split(",");
        rgb = parsedRgb.map((component) => parseInt(component));
    } else {
        return rgbStr;
    }
    return rgbToHex(rgb.map((color) => Math.ceil(color * 0.8)));
}

function movThreshold(MOV: number): number {
    if (MOV > 80) {
        return 1;
    }
    if (MOV > 30) {
        return 0.5;
    }
    return MOV / 100;
}

// return color based on party, with intensity based on MOV(margin of victory)
export function computeColor(party: string, MOV: number): string {
    let color =
        party === "REPUBLICAN" ? RED : party === "DEMOCRAT" ? BLUE : GRAY;

    if (color != GRAY) {
        color = color.map((c) =>
            Math.min(Math.ceil(c + 127 * (1 - movThreshold(MOV))), 255)
        );
    }
    return rgbToHex(color);
}
