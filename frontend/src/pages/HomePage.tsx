import { useState, useRef } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import FormControl from "react-bootstrap/FormControl";
import axios from "axios";
import "../styles/HomePage.scss";
import { RiRoadMapFill } from "react-icons/ri";
import { RiMapPin4Fill } from "react-icons/ri";
import { MdPersonSearch } from "react-icons/md";
import { getQueryString, SearchAddressModel } from "../utils/Query";
import { StateInstanceModel } from "../models/state.model";
import { DistrictInstanceModel } from "../models/district.model";
import { PoliticianInstanceModel } from "../models/politician.model";
import { StateCard } from "../components/StateCard";
import { DistrictCard } from "../components/DistrictCard";
import { PoliticianCard } from "../components/PoliticianCard";
import DetailCard from "../components/DetailCard";
import StateMap from "../components/StateMap";
import DistrictMap from "../components/DistrictMap";
import Card from "react-bootstrap/Card";
import { LogoWithText } from "../images/Logo";

export default function HomePage() {
    const [searchParam, setSearchPram] = useState<SearchAddressModel>({
        address: "",
    });
    const [state, setStateData] = useState<StateInstanceModel>();
    const [district, setDistrictData] = useState<DistrictInstanceModel>();
    const [politician, setPoliticianData] = useState<PoliticianInstanceModel>();
    const refSearchByAddressSection = useRef<HTMLDivElement>(null);
    const fetchByAddress = async () => {
        try {
            const { data: response } = await axios.get(
                process.env.REACT_APP_API_ENDPOINT +
                    "/address" +
                    getQueryString({
                        ...searchParam,
                    })
            );

            setStateData(response.state);
            setDistrictData(response.district);
            setPoliticianData(response.politician);

            if (refSearchByAddressSection && refSearchByAddressSection.current)
                refSearchByAddressSection.current.scrollIntoView();
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <Container fluid className="masthead homepage-container">
            <Row className="text-center align-items-center p-5 banner">
                <Col className="mask text-center p-5">
                    {/* <h1>GerryMap</h1> */}
                    <div className="logo-splash">
                        <LogoWithText />
                    </div>
                    <p>
                        Dedicated to identifying, quantifying, and visualizing
                        gerrymandering
                    </p>
                    <ButtonGroup>
                        <Button variant="outline-light" href="/faq">
                            Learn More
                        </Button>
                        <Button variant="outline-light" href="/about">
                            Meet the Team
                        </Button>
                    </ButtonGroup>
                </Col>
            </Row>
            <Row className="text-center align-items-start m-5 pt-5">
                <Col sm>
                    <Button
                        variant="outline-dark m-3 p-5 border rounded"
                        href="/states"
                    >
                        <h5>States</h5>
                        <RiRoadMapFill className="m-2" size={50} />
                        <p>
                            View information, data, and statistics for each U.S.
                            state
                        </p>
                    </Button>
                </Col>
                <Col sm>
                    <Button
                        variant="outline-dark m-3 p-5 border rounded"
                        href="/districts"
                    >
                        <h5>Districts</h5>
                        <RiMapPin4Fill className="m-2" size={50} />
                        <p>
                            Learn which districts are most affected by
                            gerrymandering
                        </p>
                    </Button>
                </Col>
                <Col sm>
                    <Button
                        variant="outline-dark m-3 p-5 border rounded"
                        href="/politicians"
                    >
                        <h5>Politicians</h5>
                        <MdPersonSearch className="m-2" size={50} />
                        <p>Find out more about district representatives</p>
                    </Button>
                </Col>
            </Row>
            <Row className="text-center justify-content-center pt-5">
                <Col md={7}>
                    <h4>Introduction</h4>
                    <iframe
                        src="https://www.youtube.com/embed/gvnVAzqODg0"
                        title="YouTube video player"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowFullScreen
                        style={{
                            overflow: "hidden",
                            aspectRatio: "16 / 9",
                            width: "100%",
                        }}
                    ></iframe>
                </Col>
            </Row>
            <Row className="text-center pt-5 mt-5">
                <Col>
                    <div className="search-address-wrapper pb-5">
                        <h4>Learn more about your district</h4>
                        <InputGroup
                            className="justify-content-center"
                            data-testid="inputgroup-search-by-address"
                        >
                            <FormControl
                                aria-label="input-search-by-address"
                                aria-describedby="basic-addon1"
                                placeholder="Search by your address for your Representative, District, and State"
                                className="form-50"
                                value={searchParam.address}
                                onChange={(e) =>
                                    setSearchPram({ address: e.target.value })
                                }
                                onKeyDown={(e) => {
                                    if (e.key === "Enter") {
                                        fetchByAddress();
                                    }
                                }}
                            />
                            <Button
                                variant="outline-dark"
                                id="button-addon1"
                                onClick={() => {
                                    fetchByAddress();
                                }}
                            >
                                Search
                            </Button>
                        </InputGroup>
                    </div>
                </Col>
            </Row>
            {state && district && politician && (
                <Row
                    className="align-items-start address-result-wrapper justify-content-center"
                    ref={refSearchByAddressSection}
                >
                    <Col lg={4} xl={3} data-testid="state-card">
                        <h3>State</h3>
                        <DetailCard
                            key={state.state_name}
                            to={"/states/" + state.state_abbrev}
                            itemKey={state.state_name}
                            content={<StateMap stateDetails={state} />}
                            details={<StateCard state={state} searchParam="" />}
                        />
                    </Col>
                    <Col lg={4} xl={3}>
                        <h3>District</h3>
                        <DetailCard
                            key={district.state_abbrev + district.district_num}
                            to={
                                "/districts/" +
                                (district.state_abbrev +
                                    "_" +
                                    district.district_num)
                            }
                            itemKey={
                                district.state_abbrev + district.district_num
                            }
                            content={<DistrictMap districtDetails={district} />}
                            details={
                                <DistrictCard
                                    district={district}
                                    searchParam=""
                                />
                            }
                        />
                    </Col>
                    <Col lg={4} xl={3}>
                        <h3>Representative</h3>
                        <DetailCard
                            key={
                                politician.state_abbrev +
                                "_" +
                                politician.district_num
                            }
                            to={
                                "/politicians/" +
                                politician.state_abbrev +
                                "_" +
                                politician.district_num
                            }
                            itemKey={
                                politician.state_abbrev +
                                "_" +
                                politician.district_num
                            }
                            content={
                                <Card.Img
                                    variant="top"
                                    src={politician.photo_url}
                                    className="card-img-crop card-img-politician"
                                />
                            }
                            details={
                                <PoliticianCard
                                    politician={politician}
                                    searchParam=""
                                />
                            }
                        />
                    </Col>
                </Row>
            )}
        </Container>
    );
}
