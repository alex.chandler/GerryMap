import { useEffect, useState } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import DetailCard from "../components/DetailCard";
import StateMap from "../components/StateMap";
import TableNavbar from "../components/TableNavbar";
import { StateInstanceModel } from "../models/state.model";
import axios from "axios";
import ToggleViewButtonGroup from "../components/ToggleViewButtonGroup";
import { getQueryString, SearchQueryModel } from "../utils/Query";
import { PaginationComponent } from "../components/PaginationComponent";
import { Loading } from "../components/Loading";
import {
    PaginationResponseModel,
    PaginationParameterModel,
} from "../utils/pagination.model";
import { StateCard } from "../components/StateCard";
import StateTable from "../components/StateTable";

export default function States() {
    const searchPlaceholder =
        "Search by state name, state abbr., political leaning, or region";
    const [tableView, setTableView] = useState<boolean>(false);
    const [states, setStates] = useState<StateInstanceModel[]>();

    const navbarDropdowns = [
        {
            title: "Political Leaning",
            items: ["Democrat", "Republican"],
        },
        {
            title: "Population",
            items: ["range"],
        },
        {
            title: "Size (sq. mi)",
            items: ["range"],
        },
        {
            title: "Minority Percentage",
            items: ["range"],
        },
        {
            title: "Compactness",
            items: ["range"],
        },
    ];

    const sortFields = [
        "Name",
        "Political Leaning",
        "Population",
        "Size (sq. mi)",
        "Minority Percentage",
        "Compactness",
    ];

    const paramToKey: { [name: string]: { key: string } } = {
        Name: { key: "state_name" },
        "Political Leaning": { key: "party_detailed" },
        Population: { key: "total_pop" },
        "Size (sq. mi)": { key: "sq_miles" },
        "Minority Percentage": { key: "non_white_percentage" },
        Compactness: { key: "state_compactness" },
    };

    // Pagination
    const [paginationRes, setPaginationRes] =
        useState<PaginationResponseModel>();
    const [paginationParams, setPaginationPrams] =
        useState<PaginationParameterModel>({ page: 1, page_size: 10 });

    function paginate(newCurPage: number): void {
        setPaginationPrams((prevState) => {
            return { ...prevState, page: newCurPage };
        });
    }

    function changeItemsPerPage(newCurItemsPerPage: number): void {
        let newPageNum = 1;
        if (paginationRes) {
            const numberOfPages =
                paginationRes.total_items / newCurItemsPerPage;
            newPageNum =
                numberOfPages < paginationRes.page_num
                    ? numberOfPages
                    : paginationRes.page_num;
        }
        setPaginationPrams({ page: newPageNum, page_size: newCurItemsPerPage });
    }

    function changeState(check: boolean) {
        setTableView(check);
    }

    //sort
    const [sortAscending, setSortAscending] = useState({ ascending: true });
    const [sortParam, setSortPram] = useState({ sort: "state_name" });
    function changeSortParam(param: string) {
        const key = paramToKey[param].key;
        setSortPram({ sort: key });
    }

    //filter
    const [filterParams, setFilterParams] = useState<{
        [key: string]: string;
    }>({});
    function changeFilterParams(title: string, range: string) {
        const key = paramToKey[title].key;
        setFilterParams((prevState) => {
            return { ...prevState, [key]: range };
        });
    }

    function removeFilterParams(title: string) {
        const key = paramToKey[title].key;
        setFilterParams((prevState) => {
            const newItem = { ...prevState };
            if (key in newItem) {
                delete newItem[key];
                return newItem;
            } else {
                return prevState;
            }
        });
    }

    //Search
    const [searchParam, setSearchPram] = useState<SearchQueryModel>({
        query: "",
    });
    function handleSearch(newQuery: string) {
        setSearchPram({ query: newQuery });
    }

    useEffect(() => {
        const fetchStates = async () => {
            try {
                const { data: statesResponse } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/states" +
                        getQueryString({
                            ...searchParam,
                            ...paginationParams,
                            ...sortParam,
                            ...sortAscending,
                            ...filterParams,
                        })
                );
                const stateData = statesResponse.data;
                setStates(stateData);
                setPaginationRes(statesResponse.meta);
            } catch (error) {
                console.error(error);
            }
        };
        fetchStates();
    }, [paginationParams, sortParam, sortAscending, filterParams, searchParam]);

    return (
        <Container className="mt-5 p-5" fluid>
            <h2>States</h2>
            <Row>
                <TableNavbar
                    dropdowns={navbarDropdowns}
                    sorts={sortFields}
                    searchPlaceholder={searchPlaceholder}
                    changeSortParam={changeSortParam}
                    changeSortAscending={(param) =>
                        setSortAscending({ ascending: param })
                    }
                    changeFilterParams={changeFilterParams}
                    removeFilterParams={removeFilterParams}
                    handleSearch={handleSearch}
                />
                <ToggleViewButtonGroup
                    changeState={changeState}
                    tableView={tableView}
                />
            </Row>

            {states && tableView && (
                <Row className="table-container">
                    <StateTable
                        states={states}
                        searchTerm={searchParam.query}
                    />
                </Row>
            )}
            {states && !tableView && (
                <Row
                    xs={1}
                    sm={2}
                    md={3}
                    lg={4}
                    xl={5}
                    className="g-3 card-map-group"
                >
                    {states.map((state) => (
                        <Col key={state.state_name}>
                            <DetailCard
                                to={"/states/" + state.state_abbrev}
                                content={<StateMap stateDetails={state} />}
                                details={
                                    <StateCard
                                        state={state}
                                        searchParam={searchParam.query}
                                    />
                                }
                            />
                        </Col>
                    ))}
                </Row>
            )}
            {states && paginationRes && (
                <PaginationComponent
                    totalItems={paginationRes.total_items}
                    lastPage={Math.ceil(
                        paginationRes.total_items / paginationRes.page_size
                    )}
                    curPage={paginationRes.page_num}
                    paginate={paginate}
                    curItemPerPage={paginationRes.page_size}
                    changeItemsPerPage={changeItemsPerPage}
                    lastIndex={paginationRes.last_index}
                    firstIndex={paginationRes.first_index}
                />
            )}
            {!states && <Loading />}
        </Container>
    );
}
