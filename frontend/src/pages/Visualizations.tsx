import { Container, Row } from "react-bootstrap";
import { useEffect, useState } from "react";
import axios from "axios";

import DistrictsChloro from "../visualizations/gerrymap/DistrictsChloro";
import MinorityScatter from "../visualizations/gerrymap/MinorityScatter";
import CompactnessBubble from "../visualizations/gerrymap/CompactnessBubble";

import BookPublishedDecades from "../visualizations/bookrus/BookPublishedDecades";
import AuthorBookBubble from "../visualizations/bookrus/AuthorBookBubble";
import AuthorNationalityPie from "../visualizations/bookrus/AuthorNationalityPie";

import {
    AuthorModel,
    CountryModel,
} from "../visualizations/VisualizationUtils";
import { getQueryString } from "../utils/Query";

export function Visualizations() {
    return (
        <Container className="mt-5 p-5">
            <Row>
                <h2>GerryMap Visualizations</h2>
            </Row>
            <DistrictsChloro />
            <MinorityScatter />
            <CompactnessBubble />
        </Container>
    );
}

export function ProviderVisualizations() {
    const [authorData, setAuthorData] = useState<AuthorModel[]>();
    const [countryData, setCountryData] = useState<Map<number, CountryModel>>();

    const authorsEndpoint = "https://api.bookrus.me/authors";
    const countriesEndpoint = "https://api.bookrus.me/countries";

    useEffect(() => {
        const fetchAuthors = async () => {
            const pageSize = 10000;
            const { data: authors } = await axios.get(
                authorsEndpoint +
                    getQueryString({
                        limit: pageSize,
                        page: 1,
                    })
            );
            setAuthorData(authors.data);
        };
        fetchAuthors();
    }, []);

    useEffect(() => {
        const fetchCountries = async () => {
            const { data: countries } = await axios.get(
                countriesEndpoint +
                    getQueryString({
                        limit: 250,
                        page: 1,
                    })
            );

            const countryIds = new Map<number, CountryModel>();
            countries.data.map((country: CountryModel) => {
                countryIds.set(country.country_id, {
                    country_id: country.country_id,
                    country_name: country.country_name,
                    country_region: country.country_region,
                });
            });

            setCountryData(countryIds);
        };
        fetchCountries();
    }, []);

    return (
        <Container className="mt-5 p-5">
            <Row>
                <h2>BookRUs Visualizations</h2>
            </Row>
            <BookPublishedDecades countries={countryData} />
            <AuthorBookBubble authors={authorData} />
            <AuthorNationalityPie
                authors={authorData}
                countries={countryData}
            />
        </Container>
    );
}
