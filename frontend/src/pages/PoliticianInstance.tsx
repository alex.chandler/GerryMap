// import * as React from "react";
import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { PoliticianInstanceModel } from "../models/politician.model";
// import useScript from "../hooks/useScript";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip } from "recharts";
import { AiFillHome, AiFillMail, AiFillPhone } from "react-icons/ai";
import { FaMapMarkedAlt, FaDemocrat, FaRepublican } from "react-icons/fa";
import { abbrevToName, getDistrictName } from "../utils/Names";
import axios from "axios";
import { chartData, politicianChartData } from "../utils/Charts";
// import DetailCard from "../components/DetailCard";
// import DistrictMap from "../components/DistrictMap";
import "../styles/PoliticianInstance.scss";
import { StateInstanceModel } from "../models/state.model";
import { DistrictInstanceModel } from "../models/district.model";
import DistrictMap from "../components/DistrictMap";
import StateMap from "../components/StateMap";

// export interface IPoliticianInstanceProps {}

export default function PoliticianInstance() {
    const { id } = useParams();
    const [displayData, setDisplayData] = useState<PoliticianInstanceModel>();
    const [districtData, setDistrictData] = useState<DistrictInstanceModel>();
    const [stateData, setStateData] = useState<StateInstanceModel>();
    const [statisticData, setStatisticData] =
        useState<chartData[]>(politicianChartData);

    //TODO: bug sometimes failed loading twitter and facebook
    // useScript("https://platform.twitter.com/widgets.js");
    // useScript(
    //     "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v13.0"
    // );

    useEffect(() => {
        const fetchPolitician = async () => {
            try {
                const { data: politicianData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT + "/politician/" + id
                );
                setDisplayData(politicianData);
                setStatisticData((prevState) => {
                    const items = [...prevState];
                    items[0].value = politicianData.missed_votes_pct;
                    items[1].value = politicianData.votes_with_party_pct;
                    return items;
                });
            } catch (error) {
                console.error(error);
            }
        };

        const fetchDistrict = async () => {
            try {
                const { data: politicianData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT + "/district/" + id
                );
                setDistrictData(politicianData);
            } catch (error) {
                console.error(error);
            }
        };

        const fetchState = async (state_abbrev: string | undefined) => {
            try {
                const { data: stateData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/state/" +
                        state_abbrev
                );
                setStateData(stateData);
            } catch (error) {
                console.error(error);
            }
        };

        fetchPolitician().then(() => {
            fetchState(id?.split("_")[0]).then(() => {
                fetchDistrict();
                const script = document.createElement("script");
                script.src = "https://platform.twitter.com/widgets.js";
                document.body.appendChild(script);
            });
        });
    }, []);

    return (
        <>
            {displayData && (
                <Container className="mt-5 p-5 politician-instance-container">
                    <Row>
                        <Col md={3} className="politician-card-wrap">
                            <Card className="text-center shadow politician-card">
                                <Card.Img
                                    src={displayData.photo_url}
                                ></Card.Img>
                                <Card.Body>
                                    <Card.Title>{displayData.name}</Card.Title>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col md={2}>
                            {stateData && (
                                <Link
                                    className="card-link"
                                    to={"/states/" + displayData.state_abbrev}
                                >
                                    <Card className="text-center shadow politician-card">
                                        <Card.Header>State</Card.Header>
                                        <Card.Body>
                                            <StateMap
                                                stateDetails={stateData}
                                            />
                                            {
                                                abbrevToName[
                                                    displayData.state_abbrev
                                                ].name
                                            }
                                        </Card.Body>
                                    </Card>
                                </Link>
                            )}
                            {districtData && (
                                <Link
                                    className="card-link"
                                    to={
                                        "/districts/" +
                                        displayData.state_abbrev +
                                        "_" +
                                        displayData.district_num
                                    }
                                >
                                    <Card className="text-center shadow politician-card">
                                        <Card.Header>District</Card.Header>
                                        <Card.Body>
                                            <DistrictMap
                                                districtDetails={districtData}
                                            />
                                            {getDistrictName(
                                                displayData.state_abbrev,
                                                displayData.district_num
                                            )}
                                        </Card.Body>
                                    </Card>
                                </Link>
                            )}
                        </Col>
                        <Col>
                            <Card className="shadow">
                                <Card.Header>Summary</Card.Header>
                                <ListGroup variant="flush">
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            Party
                                        </div>
                                        <div className="ms-2 me-auto summary-content">
                                            {displayData.party_detailed}
                                            {displayData.party_detailed.toLowerCase() ===
                                            "democrat" ? (
                                                <FaDemocrat color="blue" />
                                            ) : (
                                                <FaRepublican color="red" />
                                            )}
                                        </div>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            Age
                                        </div>
                                        <div className="ms-2 me-auto">
                                            {displayData.age}
                                        </div>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            Years In Office
                                        </div>
                                        <div className="ms-2 me-auto">
                                            {displayData.years_in_office}
                                        </div>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            Gender
                                        </div>
                                        <div className="ms-2 me-auto">
                                            {displayData.gender}
                                        </div>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            Birthdate
                                        </div>
                                        <div className="ms-2 me-auto">
                                            {displayData.birthdate}
                                        </div>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            First Elected
                                        </div>
                                        <div className="ms-2 me-auto">
                                            {displayData.first_elected}
                                        </div>
                                    </ListGroup.Item>
                                    <ListGroup.Item className="d-flex">
                                        <div className="summary-header">
                                            Committees
                                        </div>
                                        <div className="ms-2 me-auto">
                                            {displayData.committees
                                                .replaceAll("[", "")
                                                .replaceAll("]", "")
                                                .replaceAll("'", "")
                                                .split(",")
                                                .map((commitee, i, arr) => (
                                                    <div
                                                        key={i}
                                                        className={
                                                            arr.length - 1 === i
                                                                ? "mb-1"
                                                                : "mb-1 border-bottom"
                                                        }
                                                    >
                                                        {commitee}
                                                    </div>
                                                ))}
                                        </div>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card>
                            <Card className="shadow">
                                <Card.Header>Statistics</Card.Header>
                                <Card.Body>
                                    <BarChart
                                        width={300}
                                        height={200}
                                        data={statisticData}
                                        barSize={30}
                                    >
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis dataKey="name" />
                                        <YAxis domain={[0, 100]} />
                                        <Tooltip />
                                        <Bar dataKey="value" fill="#82ca9d" />
                                    </BarChart>
                                </Card.Body>
                            </Card>
                            <Card className="shadow">
                                <Card.Header>Contact</Card.Header>
                                <ListGroup variant="flush">
                                    <ListGroup.Item>
                                        <AiFillHome />{" "}
                                        <a
                                            href={"" + displayData.website}
                                            target="_blank"
                                        >
                                            {displayData.website}
                                        </a>
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        <AiFillMail />{" "}
                                        <a
                                            href={"" + displayData.contact_site}
                                            target="_blank"
                                        >
                                            {displayData.contact_site}
                                        </a>
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        <AiFillPhone />{" "}
                                        {displayData.phone_number}
                                    </ListGroup.Item>
                                    <ListGroup.Item>
                                        <FaMapMarkedAlt />{" "}
                                        {displayData.office_addr}
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card>
                        </Col>
                        <Col className="twitter-embed">
                            {displayData.twitter_id !== "" && (
                                <Card className="shadow">
                                    <Card.Header>Twitter</Card.Header>
                                    <a
                                        className="twitter-timeline"
                                        data-height="400"
                                        data-width="500"
                                        href={
                                            "https://twitter.com/" +
                                            displayData.twitter_id
                                        }
                                    >
                                        Tweets
                                    </a>
                                </Card>
                            )}
                            {displayData.facebook_id !== "" && (
                                <Card className="shadow">
                                    <Card.Header>Facebook</Card.Header>
                                    <iframe
                                        src={
                                            "https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F" +
                                            displayData.facebook_id +
                                            "&tabs=timeline&width=400&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
                                        }
                                        width="400"
                                        height="500"
                                        style={{
                                            border: "none",
                                            overflow: "hidden",
                                        }}
                                        scrolling="no"
                                        frameBorder="0"
                                    ></iframe>
                                </Card>
                            )}
                        </Col>
                    </Row>
                </Container>
            )}
        </>
    );
}
