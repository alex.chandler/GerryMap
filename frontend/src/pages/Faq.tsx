import * as React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import { GoLinkExternal } from "react-icons/go";
import { LogoOnly } from "../images/Logo";

// export interface IFaqProps {}

export default function Faq() {
    return (
        <Container className="p-5">
            <div className="logo-background">
                <LogoOnly />
            </div>
            {/* <Logo /> */}
            <Row className="row mt-5 p-5 text-center align-items-center">
                <h1>Frequently Asked Questions</h1>
            </Row>
            <Row className="justify-content-center">
                <Col md={8}>
                    <div className="text-justify">
                        <h4 className="text-blue">What is gerrymandering?</h4>
                        <p>
                            Gerrymandering is the act of drawing electoral
                            districts to favor one party or group of people. The
                            result of such efforts is the election of
                            representatives that do not truly represent their
                            populace.
                        </p>
                        <br></br>
                        <h4 className="text-blue">What does GerryMap do?</h4>
                        <p>
                            GerryMap is a website that contains information
                            about congressional districts, as well as
                            identifies, quantifies, and visualizes
                            gerrymandering. This project aims to consolidate
                            demographic and political data for each district, as
                            well as quantify gerrymandering based on the shape
                            of each congressional district.
                        </p>
                        <br></br>
                        <h4 className="text-blue">
                            Why is gerrymandering important, and how does it
                            affect us?
                        </h4>
                        <p>
                            Gerrymandering distorts democracy. It means that
                            your vote means less or more than it would in a fair
                            system. Recent research suggests that gerrymandering
                            even helps inflate partisanship. At worst, it allows
                            for unelected people that you have never heard of to
                            heavily influence the outcome of an election.
                        </p>
                        <br></br>
                        <h4 className="text-blue">Is gerrymandering legal?</h4>
                        <p>
                            Well, the answer is complicated. First, it it is a
                            question of constitutionality. In 2019, Chief
                            Justice Roberts as part of a 5-4 conservative
                            majority decision, wrote “We conclude that partisan
                            gerrymandering claims present political questions
                            beyond the reach of the federal courts.” Racial
                            gerrymandering, on the other hand, is still
                            unconstitutional, with a history court cases having
                            prevented racial gerrymandering from redistricting.
                        </p>
                        <br></br>
                        <h4 className="text-blue">
                            What are metrics to identify gerrymandering?
                        </h4>
                        <p>
                            One common method is to measure the Reock score for
                            each district, and to find the states with the
                            lowest average Reock score for all districts in
                            their state. The Reock score is the ratio of area
                            inside the district to the entire area of the
                            smallest encapsulating circle. In fact, this is how
                            the GerryMap team calculates compactness scores.
                            Other metrics were created using partisan data. One
                            data point in this category is the mean-median
                            difference, which calculates the difference between
                            the median vote share for all districts and the
                            average vote share across the state. The average
                            vote share is a closer representation of an expected
                            partisan distribution without gerrymandering.
                        </p>
                        <br></br>
                        <h4 className="text-blue">
                            Where can I learn more about gerrymandering?
                        </h4>
                        <p>Here are some resources to get you started.</p>
                        <ButtonGroup
                            vertical
                            className="mb-3 mx-3"
                            style={{ display: "flex" }}
                        >
                            <Button
                                className="btn-block button"
                                variant="outline-dark"
                                href="https://projects.fivethirtyeight.com/partisan-gerrymandering-north-carolina/"
                                target="_blank"
                            >
                                FiveThirtyEight: One Way To Spot A Partisan
                                Gerrymander
                                <GoLinkExternal className="mb-2" size={15} />
                            </Button>
                            <Button
                                className="btn-block button"
                                variant="outline-dark"
                                href="https://projects.fivethirtyeight.com/redistricting-2022-maps/"
                                target="_blank"
                            >
                                FiveThirtyEight: What Redistricting Looks Like
                                In Every State
                                <GoLinkExternal className="mb-2" size={15} />
                            </Button>
                            <Button
                                className="btn-block button"
                                variant="outline-dark"
                                href="https://www.nytimes.com/interactive/2021/11/07/us/politics/redistricting-maps-explained.html"
                                target="_blank"
                            >
                                New York Times: Redistricting Maps Explained
                                <GoLinkExternal className="mb-2" size={15} />
                            </Button>
                            <Button
                                className="btn-block button"
                                variant="outline-dark"
                                href="https://www.brennancenter.org/our-work/analysis-opinion/what-extreme-gerrymandering"
                                target="_blank"
                            >
                                Brennan Center: What is Extreme Gerrymandering?
                                <GoLinkExternal className="mb-2" size={15} />
                            </Button>
                        </ButtonGroup>
                        <h4 className="text-blue">Sources</h4>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem>
                                <b className="text-red">FAQ page sources</b>
                                <br></br>
                                <ButtonGroup
                                    vertical
                                    className="my-2"
                                    style={{ display: "flex" }}
                                >
                                    <Button
                                        className="btn-block button"
                                        variant="outline-dark"
                                        href="https://indivisible.org/resource/fighting-gerrymandering-states"
                                        target="_blank"
                                    >
                                        indivisible.org{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                    <Button
                                        className="btn-block button"
                                        variant="outline-dark"
                                        href="https://redistricting.lls.edu/redistricting-101/why-should-we-care/"
                                        target="_blank"
                                    >
                                        redistricting.lls.edu{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                    <Button
                                        className="btn-block button"
                                        variant="outline-dark"
                                        href="https://www.fairvote.org/gerrymandering#gerrymandering_key_facts"
                                        target="_blank"
                                    >
                                        fairvote.org{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                    <Button
                                        className="btn-block button"
                                        variant="outline-dark"
                                        href="https://www.aclu.org/news/voting-rights/what-is-redistricting-and-why-should-we-care/"
                                        target="_blank"
                                    >
                                        aclu.org{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                </ButtonGroup>
                            </ListGroupItem>
                            <ListGroupItem>
                                <b className="text-red">Splash page image</b>
                                <br></br>
                                <ButtonGroup style={{ display: "flex" }}>
                                    <Button
                                        className="btn-block button my-2"
                                        variant="outline-dark"
                                        href="https://documenter.getpostman.com/view/19780402/UVkqsvRV"
                                        target="_blank"
                                    >
                                        unsplash.com{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                </ButtonGroup>
                            </ListGroupItem>
                        </ListGroup>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}
