import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";
import axios from "axios";
import "../styles/About.scss";
import {
    Contributors,
    IssuesStatistics,
    GitlabStat,
} from "../models/gitlab.model";
import { ListGroup, ListGroupItem } from "react-bootstrap";
import Accordion from "react-bootstrap/Accordion";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import { GoLinkExternal } from "react-icons/go";
// import { getConfigFileParsingDiagnostics } from "typescript";

const aboutDesc =
    "Gerrymandering is the act of drawing electoral districts to favor one " +
    "party or group of people. The result of such efforts is the election of " +
    "representatives that do not truly represent their populace. GerryMap is " +
    "a website dedicated to documenting gerrymandering across the United States " +
    "and informing those who may be affected by it or want to learn more.";

const apiDesc =
    "Our website provides a RESTful API for public use. Using the API, it's " +
    "possible to retrieve resources such as state, district, and politician " +
    "information that is displayed on our site in a JSON format. ";

const initialProjectStat: GitlabStat = {
    userId: "project",
    name: ["GerryMap"],
    commits: 0,
    issues: 0,
    openIssue: 0,
    closeIssue: 0,
    unitTests: 0,
    imagePath: "",
    desc: "",
    type: "",
};

const initialGitlabStat: GitlabStat[] = [
    {
        userId: "silvercat5922",
        name: ["Helen Fang"],
        commits: 0,
        issues: 0,
        openIssue: 0,
        closeIssue: 0,
        unitTests: 29,
        imagePath:
            "https://helen568328643.files.wordpress.com/2022/02/headshot2022-1.jpg?w=400",
        desc:
            "I'm a 3rd-year computer science major from Austin, TX. " +
            "In my free time, I like to cook and play video games with my friends.",
        type: "Backend",
    },
    {
        userId: "alex.chandler",
        name: ["Alex L Chandler", "Alex Chandler"],
        commits: 0,
        issues: 0,
        openIssue: 0,
        closeIssue: 0,
        unitTests: 32,
        imagePath:
            "https://miro.medium.com/max/1286/1*7zKMM1OyVBi08R23ZjFG_A.jpeg",
        desc:
            "I'm a Junior computer science student at the University of Texas passionate in the " +
            "intersection between politics and computational geometry. In my spare time I love to play cello and basketball.",
        type: "Backend",
    },
    {
        userId: "joannfeng",
        name: ["Joann Feng", "joannfeng"],
        commits: 0,
        issues: 0,
        openIssue: 0,
        closeIssue: 0,
        unitTests: 2,
        imagePath:
            "https://cdn.discordapp.com/attachments/948048391257063495/948048421607059506/joann-profile.jpg",
        desc:
            "I'm a fourth-year student majoring in Computer Science at the University of Texas at Austin. " +
            "I am interested in lapidary and mobile games.",
        type: "Frontend",
    },
    {
        userId: "akshay-a-sharma",
        name: ["Akshay Sharma"],
        commits: 0,
        issues: 0,
        openIssue: 0,
        closeIssue: 0,
        unitTests: 25,
        imagePath:
            "https://miro.medium.com/max/700/1*hIJOy27C6bPRFdpLUhEt3A.jpeg",
        desc:
            "I'm a sophomore computer science major at UT Austin with an interest in software engineering. " +
            "I like to play video games in my free time.",
        type: "Backend",
    },
    {
        userId: "tingyincc",
        name: ["Ting-Yin Chang Chien", "Ting-Yin Changchien", "tingyincc"],
        commits: 0,
        issues: 0,
        openIssue: 0,
        closeIssue: 0,
        unitTests: 35,
        imagePath:
            "https://miro.medium.com/max/1400/1*pho_-zgxfH4cP28YX2c_qg.jpeg",
        desc:
            "I'm a first-year master's student from Taiwan, majoring in Computer Science at the University of Texas at Austin. " +
            "My hobbies are traveling, cooking, and drawing.",
        type: "Frontend",
    },
];

export default function About() {
    const [dataGitlabStat, setDataGitlabStat] =
        useState<GitlabStat[]>(initialGitlabStat);
    const [dataProjectStat, setDataProjectStat] =
        useState<GitlabStat>(initialProjectStat);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        const fetchDataContributor = async () => {
            try {
                const { data: response } = await axios.get(
                    "https://gitlab.com/api/v4/projects/33862053/repository/contributors"
                );
                const contributors = response as Contributors[];
                const items = [...initialGitlabStat];
                const project_items = { ...initialProjectStat };
                for (let i = 0; i < contributors.length; i++) {
                    // Matching contributors name
                    const index = initialGitlabStat.findIndex((e) =>
                        e.name.find((ele) => ele === contributors[i].name)
                    );
                    if (index !== undefined && index !== -1) {
                        items[index].commits += contributors[i].commits;
                        project_items.commits += contributors[i].commits;
                    }
                }
                setDataGitlabStat((prevState) => {
                    const gitlab_item = [...prevState];
                    for (let i = 0; i < initialGitlabStat.length; i++) {
                        gitlab_item[i].commits = items[i].commits;
                    }
                    return gitlab_item;
                });
                setDataProjectStat((prevState) => {
                    const proj_item = { ...prevState };
                    proj_item.commits = project_items.commits;
                    return proj_item;
                });
            } catch (error) {
                console.error(error);
            }
        };

        const fetchDataIssuesStatistics = async (
            index: number,
            userId: string
        ) => {
            try {
                const { data: response } = await axios.get(
                    "https://gitlab.com/api/v4/projects/33862053/issues_statistics?assignee_username=" +
                        userId
                );
                const issueStat = response as IssuesStatistics;
                setDataGitlabStat((prevState) => {
                    const items = [...prevState];
                    items[index].issues = issueStat.statistics.counts.all;
                    items[index].closeIssue =
                        issueStat.statistics.counts.closed;
                    items[index].openIssue = issueStat.statistics.counts.opened;
                    return items;
                });
            } catch (error) {
                console.error(error);
            }
        };

        const fetchDataProjectStatistics = async () => {
            try {
                const { data: response } = await axios.get(
                    "https://gitlab.com/api/v4/projects/33862053/issues_statistics"
                );
                const issueStat = response as IssuesStatistics;
                setDataProjectStat((prevState) => {
                    const item = { ...prevState };
                    item.issues = issueStat.statistics.counts.all;
                    item.closeIssue = issueStat.statistics.counts.closed;
                    item.openIssue = issueStat.statistics.counts.opened;
                    return item;
                });
            } catch (error) {
                console.error(error);
            }
        };

        fetchDataContributor();
        for (let i = 0; i < initialGitlabStat.length; i++) {
            fetchDataIssuesStatistics(i, initialGitlabStat[i].userId);
        }
        fetchDataProjectStatistics();

        setLoading(false);
    }, []);

    return (
        <Container className="p-5">
            <Row className="row pt-5 text-center align-items-center">
                <h1>About</h1>
            </Row>
            <Row className="justify-content-center">
                <Col md={8}>
                    <h4 className="text-red">Purpose</h4>
                    <div className="text-justify">
                        <p>{aboutDesc}</p>
                    </div>

                    <br></br>
                    <h4 className="text-red">GerryMap API</h4>
                    <div className="text-justify">
                        <p className="text-justify">{apiDesc}</p>
                    </div>

                    <br></br>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>
                                <h4 className="text-blue">APIs</h4>
                            </Accordion.Header>
                            <Accordion.Body>
                                <ListGroup className="list-group-flush">
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button mb-2"
                                            variant="outline-dark" 
                                            href="https://www2.census.gov/geo/tiger/TIGERrd13/CD113/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Census Bureau</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        Though not an API, we downloaded
                                        shapefiles of all the districts from
                                        Census Bureau so that we could display
                                        district maps on the website. We also
                                        wrote python scripts to convert the .shp
                                        files into topojson and to calculate
                                        compactness scores for each district.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://www.census.gov/data/developers.html"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Census Gov API</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>A collection of APIs used to
                                        get demographic and election data from
                                        each state and congressional district.
                                        The Census Gov API Information was
                                        scraped with the CensusAPIs wrapper
                                        class, using data mostly from the
                                        American Community Survey.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://developers.google.com/civic-information"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Google Civic Info API</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        An API designed to provide political
                                        information about House representatives
                                        and senators for each district and
                                        state, respectively. We built a wrapper
                                        class called GoogleCivicAPI to handle
                                        getting information for each address.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://developers.google.com/maps/documentation/geocoding/overview"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Google Geocode API</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        The Geocoding API provides geocoding and
                                        reverse geocoding for addresses. Since
                                        the Google Civic Info API only allows us
                                        to query by address, we used this API to
                                        convert latitude-longitude pairs into
                                        addresses. Our GoogleGeocodeAPI class
                                        has one main function that gets
                                        addresses for geological coordinates.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://www.opensecrets.org/open-data/api"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Open Secrets API</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        An API designed to provide information
                                        about politicians. We used the API to
                                        retrieve key details such as political
                                        information, financial information, and
                                        personal media for each politician. Our
                                        OpenSecretsAPI class calls on two main
                                        API requests for each candidate.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://projects.propublica.org/api-docs/congress-api/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Propublica API</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        We used this API to get information
                                        similar to Open Secrets API, but
                                        combining the two APIs helped reduce the
                                        number of missing fields in the data. We
                                        used a python script to query the API
                                        for each district representative and
                                        parse the relevant data.
                                    </ListGroupItem>
                                </ListGroup>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                    <br></br>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>
                                <h4 className="text-blue">Tools Used</h4>
                            </Accordion.Header>
                            <Accordion.Body>
                                <ListGroup className="list-group-flush">
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://reactjs.org/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">React</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        Frontend framework for building user
                                        interface.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://about.gitlab.com/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">GitLab</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        For version control and team
                                        coordination.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://www.postman.com/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Postman</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        To provide API documentation.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://www.namecheap.com/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">NameCheap</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        For domain management.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://aws.amazon.com/amplify/hosting/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Amplify</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        For hosting the frontend of our website.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://aws.amazon.com/elasticbeanstalk/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Elastic Beanstalk</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        For hosting the backend of our website.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href=""
                                            target="_blank"
                                        >
                                            <b className="text-blue">RDS</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        For hosting our website database.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://www.w3schools.com/css/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">CSS</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        For theming and improving visuals on the
                                        frontend.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://getbootstrap.com/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Bootstrap</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        We used a variety of react-bootstrap
                                        libraries to assist us with frontend
                                        organization.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <Button 
                                            className="btn-block button my-2"
                                            variant="outline-dark" 
                                            href="https://www.selenium.dev/"
                                            target="_blank"
                                        >
                                            <b className="text-blue">Selenium</b> <GoLinkExternal className="mb-2" size={15}/>
                                        </Button>
                                        <br></br>
                                        Used for both unit testing and
                                        collecting politician photos.
                                    </ListGroupItem>
                                </ListGroup>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                    <br></br>
                    <Accordion>
                        <Accordion.Item eventKey="0">
                            <Accordion.Header>
                                <h4 className="text-blue">
                                    Project Phase Leaders
                                </h4>
                            </Accordion.Header>
                            <Accordion.Body>
                                <ListGroup className="list-group-flush">
                                    <ListGroupItem>
                                        <b className="text-blue">
                                            Phase 1 Leader
                                        </b>
                                        <br></br>
                                        Alex Chandler, who provided inspiration
                                        for the direction of the project.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <b className="text-blue">
                                            Phase 2 Leader
                                        </b>
                                        <br></br>
                                        Helen Fang, who worked on getting a lot
                                        of the moving parts deployed.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <b className="text-blue">
                                            Phase 3 Leader
                                        </b>
                                        <br></br>
                                        Ting-Yin Changchien, who worked on
                                        various use cases in the frontend for
                                        searching, filtering, and sorting.
                                    </ListGroupItem>
                                    <ListGroupItem>
                                        <b className="text-blue">
                                            Phase 4 Leader
                                        </b>
                                        <br></br>
                                        Akshay Sharma, who worked on refactoring
                                        the backend to make it much cleaner.
                                    </ListGroupItem>
                                </ListGroup>
                            </Accordion.Body>
                        </Accordion.Item>
                    </Accordion>
                    <br></br>
                </Col>
            </Row>
            <Row>
                <Col>
                    {loading && <div>Loading</div>}
                    {!loading && (
                        <CardGroup>
                            {dataGitlabStat?.map((stat) => (
                                <Card className="mb-2" key={stat.userId}>
                                    <Card.Img
                                        variant="top"
                                        src={stat.imagePath}
                                        className="card-img-crop card-img-about"
                                    />
                                    <Card.Body>
                                        <Card.Title>{stat.name[0]}</Card.Title>
                                        <Card.Text>{stat.desc}</Card.Text>
                                    </Card.Body>
                                    <ListGroup className="list-group-flush">
                                        <ListGroupItem>
                                            {stat.type} Developer
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Commits: {stat.commits}
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Issues: {stat.issues}
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Open: {stat.openIssue}
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Closed: {stat.closeIssue}
                                        </ListGroupItem>
                                        <ListGroupItem>
                                            Unit Tests: {stat.unitTests}
                                        </ListGroupItem>
                                    </ListGroup>
                                </Card>
                            ))}
                        </CardGroup>
                    )}
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={5}>
                    <Card className="mt-5 mb-2" style={{ textAlign: "center" }}>
                        <Card.Body>
                            <Card.Title>
                                <ButtonGroup vertical className="d-flex">
                                    <Button
                                        className="btn-block button"
                                        variant="outline-dark"
                                        href="https://gitlab.com/alex.chandler/GerryMap"
                                        target="_blank"
                                    >
                                        <b className="text-blue">
                                            See GitLab Page
                                        </b>{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                    <Button
                                        className="btn-block button"
                                        variant="outline-dark"
                                        href="https://documenter.getpostman.com/view/19780402/UVkqsvRV"
                                        target="_blank"
                                    >
                                        <b className="text-blue">
                                            See Postman Documentation
                                        </b>{" "}
                                        <GoLinkExternal
                                            className="mb-2"
                                            size={15}
                                        />
                                    </Button>
                                </ButtonGroup>
                            </Card.Title>
                            <ListGroup className="list-group">
                                <ListGroupItem>
                                    Total Commits: {dataProjectStat.commits}
                                </ListGroupItem>
                                <ListGroupItem>
                                    Total Issues: {dataProjectStat.issues}
                                </ListGroupItem>
                                <ListGroupItem>
                                    Open Issues: {dataProjectStat.openIssue}
                                </ListGroupItem>
                                <ListGroupItem>
                                    Closed Issues: {dataProjectStat.closeIssue}
                                </ListGroupItem>
                                <ListGroupItem>Unit Tests: 56</ListGroupItem>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
                <br></br>
            </Row>
        </Container>
    );
}
