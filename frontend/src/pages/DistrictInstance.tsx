import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import DistrictMap from "../components/DistrictMap";
import StateMap from "../components/StateMap";
import { PoliticianInstanceModel } from "../models/politician.model";
import { DistrictInstanceModel } from "../models/district.model";
import { getDistrictName, abbrevToName } from "../utils/Names";
import { chartDataJs, populationData, genderData } from "../utils/Charts";
import axios from "axios";
import { StateInstanceModel } from "../models/state.model";
import { Pie } from "react-chartjs-2";

export default function DistrictInstance() {
    const { id } = useParams();
    const [displayData, setDisplayData] = useState<DistrictInstanceModel>();
    const [politicianData, setPoliticianData] =
        useState<PoliticianInstanceModel>();
    const [stateData, setStateData] = useState<StateInstanceModel>();

    const [popData, setPopData] = useState(populationData);
    const [genData, setGenData] = useState(genderData);

    const [popChartjs, setPopChartjs] = useState<chartDataJs>();
    const [genChartjs, setGenChartjs] = useState<chartDataJs>();

    useEffect(() => {
        const fetchDistrict = async () => {
            try {
                const { data: districtData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT + "/district/" + id
                );
                setDisplayData(districtData);
                setPopData((prevState) => {
                    const items = [...prevState];
                    items[0].value = districtData.white_pop;
                    items[1].value = districtData.hispanic_pop;
                    items[2].value = districtData.asian_pop;
                    items[3].value = districtData.black_pop;
                    return items;
                });
                setGenData((prevState) => {
                    const items = [...prevState];
                    items[0].value = districtData.male_pop;
                    items[1].value = districtData.female_pop;
                    return items;
                });
            } catch (error) {
                console.error(error);
            }
        };

        const fetchPoliticiansByDistrict = async () => {
            try {
                const { data: politicianData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT + "/politician/" + id
                );
                setPoliticianData(politicianData);
            } catch (error) {
                console.error(error);
            }
        };

        const fetchStateDetails = async (state_abbrev: string | undefined) => {
            try {
                const { data: stateData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/state/" +
                        state_abbrev
                );
                setStateData(stateData);
            } catch (error) {
                console.error(error);
            }
        };

        fetchDistrict().then(() => {
            fetchPoliticiansByDistrict().then(() => {
                fetchStateDetails(id?.split("_")[0]);
                setPopChartjs({
                    labels: ["White", "Hispanic", "Asian", "Black"],
                    datasets: [
                        {
                            label: "# of Votes",
                            data: popData.map((d) => d.value),
                            backgroundColor: [
                                "#B5E19F",
                                "#F4A8D2",
                                "#C5AFF3",
                                "#FFE181",
                            ],
                            borderColor: [
                                "#85CE61",
                                "#EF7CBB",
                                "#A482ED",
                                "#FFC200",
                            ],
                            borderWidth: 1,
                        },
                    ],
                });
                setGenChartjs({
                    labels: ["Male", "Female"],
                    datasets: [
                        {
                            label: "# of Votes",
                            data: genData.map((d) => d.value),
                            backgroundColor: ["#A2D8F3", "#F3A2CE"],
                            borderColor: ["#70C4ED", "#ED70B4"],
                            borderWidth: 1,
                        },
                    ],
                });
            });
        });
    }, []);

    return (
        <Container className="mt-5 p-5">
            <Row>
                <Col></Col>
                <Col md="auto">
                    {displayData && (
                        <>
                            <h1>
                                {getDistrictName(
                                    displayData.state_abbrev,
                                    displayData.district_num
                                )}
                            </h1>
                            <Row className="state-instance-container">
                                <Col>
                                    <DistrictMap
                                        districtDetails={displayData}
                                    />
                                </Col>

                                <Col md={2}>
                                    {politicianData && (
                                        <Link
                                            className="card-link"
                                            to={"/politicians/" + id}
                                        >
                                            <Card className="text-center shadow politician-card">
                                                <Card.Header>
                                                    Representative
                                                </Card.Header>
                                                <Card.Img
                                                    src={
                                                        politicianData.photo_url
                                                    }
                                                ></Card.Img>
                                                <Card.Body>
                                                    {politicianData.name}
                                                </Card.Body>
                                            </Card>
                                        </Link>
                                    )}
                                    {stateData && (
                                        <Link
                                            className="card-link"
                                            to={
                                                "/states/" +
                                                displayData.state_abbrev
                                            }
                                        >
                                            <Card className="text-center shadow politician-card">
                                                <Card.Header>State</Card.Header>
                                                <Card.Body>
                                                    <StateMap
                                                        stateDetails={stateData}
                                                    />
                                                    {
                                                        abbrevToName[
                                                            displayData
                                                                .state_abbrev
                                                        ].name
                                                    }
                                                </Card.Body>
                                            </Card>
                                        </Link>
                                    )}
                                </Col>
                                <Col md={3}>
                                    <Card className="chart-card">
                                        <Card.Body>
                                            <Card.Title>
                                                Demographic Composition
                                            </Card.Title>
                                            <Col className="chart-pop">
                                                {popChartjs ? (
                                                    <Pie data={popChartjs} />
                                                ) : null}
                                            </Col>
                                            <Col className="chart-gender">
                                                {genChartjs ? (
                                                    <Pie data={genChartjs} />
                                                ) : null}
                                            </Col>
                                        </Card.Body>
                                    </Card>
                                </Col>
                                <Col>
                                    <Row className="info-card-group">
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Political Leaning
                                                </Card.Title>
                                                {displayData.party_detailed}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Total Population
                                                </Card.Title>
                                                {displayData.total_pop.toLocaleString(
                                                    "en-US"
                                                )}{" "}
                                                people (approx.)
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>Size</Card.Title>
                                                {displayData.sq_miles.toLocaleString(
                                                    "en-US"
                                                )}{" "}
                                                sq mi
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Compactness
                                                </Card.Title>
                                                {displayData.district_compactness.toFixed(
                                                    2
                                                )}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Candidate Votes
                                                </Card.Title>
                                                {displayData.candidatevotes.toLocaleString(
                                                    "en-US"
                                                )}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Total Votes
                                                </Card.Title>
                                                {displayData.totalvotes.toLocaleString(
                                                    "en-US"
                                                )}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    MOV (margin-of-victory)
                                                </Card.Title>
                                                {displayData.MOV.toFixed(2)}%
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Percent Minority
                                                </Card.Title>
                                                {displayData.non_white_percentage.toFixed(
                                                    2
                                                )}
                                                %
                                            </Card.Body>
                                        </Card>
                                    </Row>
                                </Col>
                            </Row>

                            {/* <Row className="state-instance-container instance-second-row">
                                <Col md={4}>
                                    {politicianData && (
                                        <Link
                                            className="card-link"
                                            to={"/politicians/" + id}
                                        >
                                            <Card className="text-center shadow politician-card">
                                                <Card.Header>
                                                    Representative
                                                </Card.Header>
                                                <Card.Img
                                                    src={
                                                        politicianData.photo_url
                                                    }
                                                ></Card.Img>
                                                <Card.Body>
                                                    {politicianData.name}
                                                </Card.Body>
                                            </Card>
                                        </Link>
                                    )}
                                    {stateData && (
                                        <Link
                                            className="card-link"
                                            to={
                                                "/states/" +
                                                displayData.state_abbrev
                                            }
                                        >
                                            <Card className="text-center shadow politician-card">
                                                <Card.Header>State</Card.Header>
                                                <Card.Body>
                                                    <StateMap
                                                        stateDetails={stateData}
                                                    />
                                                    {
                                                        abbrevToName[
                                                            displayData
                                                                .state_abbrev
                                                        ].name
                                                    }
                                                </Card.Body>
                                            </Card>
                                        </Link>
                                    )}
                                </Col>
                                <Col>
                                    <Row>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Political Leaning
                                                </Card.Title>
                                                {displayData.party_detailed}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Total Population
                                                </Card.Title>
                                                {displayData.total_pop.toLocaleString(
                                                    "en-US"
                                                )}{" "}
                                                people (approx.)
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>Size</Card.Title>
                                                {displayData.sq_miles.toLocaleString(
                                                    "en-US"
                                                )}{" "}
                                                sq mi
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Compactness
                                                </Card.Title>
                                                {displayData.district_compactness.toFixed(
                                                    2
                                                )}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Candidate Votes
                                                </Card.Title>
                                                {displayData.candidatevotes.toLocaleString(
                                                    "en-US"
                                                )}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Total Votes
                                                </Card.Title>
                                                {displayData.totalvotes.toLocaleString(
                                                    "en-US"
                                                )}
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-blue"
                                                    : "card-red"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    MOV (margin-of-victory)
                                                </Card.Title>
                                                {displayData.MOV.toFixed(2)}%
                                            </Card.Body>
                                        </Card>
                                        <Card
                                            className={
                                                displayData.party_detailed.toLowerCase() ===
                                                "democrat"
                                                    ? "card-red"
                                                    : "card-blue"
                                            }
                                        >
                                            <Card.Body>
                                                <Card.Title>
                                                    Percent Minority
                                                </Card.Title>
                                                {displayData.non_white_percentage.toFixed(
                                                    2
                                                )}
                                                %
                                            </Card.Body>
                                        </Card>
                                    </Row>
                                </Col>
                            </Row> */}
                        </>
                    )}
                </Col>
                <Col></Col>
            </Row>
        </Container>
    );
}
