import { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import { PoliticianInstanceModel } from "../models/politician.model";
import DetailCard from "../components/DetailCard";
import TableNavbar from "../components/TableNavbar";
import "../styles/Politicians.scss";
import { PaginationComponent } from "../components/PaginationComponent";
import ToggleViewButtonGroup from "../components/ToggleViewButtonGroup";
import { getQueryString } from "../utils/Query";
import axios from "axios";
import { Loading } from "../components/Loading";
import {
    PaginationResponseModel,
    PaginationParameterModel,
} from "../utils/pagination.model";
import { PoliticianCard } from "../components/PoliticianCard";
import PoliticianTable from "../components/PoliticianTable";

export default function Politicians() {
    const searchPlaceholder =
        "Search by name, state, state abbr., district number, or political party";
    const [tableView, setTableView] = useState<boolean>(false);
    const [politicians, setPoliticianData] =
        useState<PoliticianInstanceModel[]>();

    const navbarDropdowns = [
        {
            title: "Political Party",
            items: ["Democrat", "Republican"],
        },
        {
            title: "Age",
            items: ["range"],
        },
        {
            title: "Gender",
            items: ["Male", "Female"],
        },
        {
            title: "Years In Office",
            items: ["range"],
        },
        {
            title: "Missed Votes Percentage",
            items: ["range"],
        },
        {
            title: "Voting With Party Percentage",
            items: ["range"],
        },
    ];
    const sortFields = [
        "Name",
        "Age",
        "Gender",
        "Years In Office",
        "Missed Votes Percentage",
        "Voting With Party Percentage",
    ];
    const paramToKey: { [name: string]: { key: string } } = {
        Name: { key: "name" },
        Age: { key: "age" },
        Gender: { key: "gender" },
        "Years In Office": { key: "years_in_office" },
        "Missed Votes Percentage": { key: "missed_votes_pct" },
        "Voting With Party Percentage": { key: "votes_with_party_pct" },
        "Political Party": { key: "party_detailed" },
    };

    // Pagination
    const [paginationRes, setPaginationRes] =
        useState<PaginationResponseModel>();
    const [paginationParams, setPaginationPrams] =
        useState<PaginationParameterModel>({ page: 1, page_size: 10 });

    function paginate(newCurPage: number): void {
        setPaginationPrams((prevState) => {
            return { ...prevState, page: newCurPage };
        });
    }

    function changeItemsPerPage(newCurItemsPerPage: number): void {
        let newPageNum = 1;
        if (paginationRes) {
            const numberOfPages =
                paginationRes.total_items / newCurItemsPerPage;
            newPageNum =
                numberOfPages < paginationRes.page_num
                    ? numberOfPages
                    : paginationRes.page_num;
        }
        setPaginationPrams({ page: newPageNum, page_size: newCurItemsPerPage });
    }

    // Toggle View
    function changeState(check: boolean) {
        setTableView(check);
    }

    //sort
    const [sortAscending, setSortAscending] = useState({ ascending: true });
    const [sortParam, setSortPram] = useState({ sort: "name" });
    function changeSortParam(param: string) {
        const key = paramToKey[param].key;
        setSortPram({ sort: key });
    }

    //filter
    const [filterParams, setFilterParams] = useState<{
        [key: string]: string;
    }>({});
    function changeFilterParams(title: string, range: string) {
        const key = paramToKey[title].key;
        if (key == "gender") {
            if (range.toLowerCase() === "male") {
                range = "M";
            } else if (range.toLowerCase() === "female") {
                range = "F";
            }
        }
        setFilterParams((prevState) => {
            return { ...prevState, [key]: range };
        });
    }
    function removeFilterParams(title: string) {
        const key = paramToKey[title].key;
        setFilterParams((prevState) => {
            const newItem = { ...prevState };
            if (key in newItem) {
                delete newItem[key];
                return newItem;
            } else {
                return prevState;
            }
        });
    }

    //Search
    const [searchParam, setSearchPram] = useState<{ query: string }>({
        query: "",
    });
    function handleSearch(newQuery: string) {
        setSearchPram({ query: newQuery });
    }

    useEffect(() => {
        const fetchPoliticians = async () => {
            try {
                const { data: politicianResponse } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/politicians" +
                        getQueryString({
                            ...searchParam,
                            ...paginationParams,
                            ...sortParam,
                            ...sortAscending,
                            ...filterParams,
                        })
                );
                const politicianData = politicianResponse.data;
                setPoliticianData(politicianData);
                setPaginationRes(politicianResponse.meta);
            } catch (error) {
                console.error(error);
            }
        };
        fetchPoliticians();
    }, [paginationParams, sortParam, sortAscending, filterParams, searchParam]);

    return (
        <Container className="mt-5 p-5">
            <h2>Politicians</h2>
            <Row>
                <TableNavbar
                    dropdowns={navbarDropdowns}
                    sorts={sortFields}
                    searchPlaceholder={searchPlaceholder}
                    changeSortParam={changeSortParam}
                    changeSortAscending={(param) =>
                        setSortAscending({ ascending: param })
                    }
                    changeFilterParams={changeFilterParams}
                    removeFilterParams={removeFilterParams}
                    handleSearch={handleSearch}
                />
                <ToggleViewButtonGroup
                    changeState={changeState}
                    tableView={tableView}
                />
            </Row>

            {politicians && tableView && (
                <Row className="table-container">
                    <PoliticianTable
                        politicians={politicians}
                        searchTerm={searchParam.query}
                    />
                </Row>
            )}
            {politicians && !tableView && (
                <Row
                    xs={1}
                    sm={2}
                    md={3}
                    lg={4}
                    xl={5}
                    className="g-3 row-politician-list"
                >
                    {politicians.map((politician) => (
                        <Col
                            key={
                                politician.state_abbrev +
                                politician.district_num
                            }
                        >
                            <DetailCard
                                to={
                                    "/politicians/" +
                                    politician.state_abbrev +
                                    "_" +
                                    politician.district_num
                                }
                                content={
                                    <Card.Img
                                        variant="top"
                                        src={politician.photo_url}
                                        className="card-img-crop card-img-politician"
                                        onError={(e) => {
                                            (
                                                e.target as HTMLImageElement
                                            ).src = require("../images/placeholder-image-person.jpeg");
                                        }}
                                    />
                                }
                                details={
                                    <PoliticianCard
                                        politician={politician}
                                        searchParam={searchParam.query}
                                    />
                                }
                            />
                        </Col>
                    ))}
                </Row>
            )}
            {politicians && paginationRes && (
                <PaginationComponent
                    totalItems={paginationRes.total_items}
                    lastPage={Math.ceil(
                        paginationRes.total_items / paginationRes.page_size
                    )}
                    curPage={paginationRes.page_num}
                    paginate={paginate}
                    curItemPerPage={paginationRes.page_size}
                    changeItemsPerPage={changeItemsPerPage}
                    lastIndex={paginationRes.last_index}
                    firstIndex={paginationRes.first_index}
                />
            )}

            {!politicians && <Loading />}
        </Container>
    );
}
