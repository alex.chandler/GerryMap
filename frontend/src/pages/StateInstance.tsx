// import * as React from 'react';
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import StateMap from "../components/StateMap";
import DistrictMap from "../components/DistrictMap";
import { StateInstanceModel } from "../models/state.model";
import { DistrictInstanceModel } from "../models/district.model";
import { PoliticianInstanceModel } from "../models/politician.model";
import Card from "react-bootstrap/Card";
import { chartDataJs, populationData, genderData } from "../utils/Charts";
import { getDistrictName } from "../utils/Names";
import DetailCard from "../components/DetailCard";
import axios from "axios";
import { getQueryString } from "../utils/Query";
import { Pie } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import "../styles/StateInstance.scss";

ChartJS.register(ArcElement, Tooltip, Legend);

export default function StateInstance() {
    const { id } = useParams();
    const [displayData, setDisplayData] = useState<StateInstanceModel>();
    const [districts, setDistricts] = useState<DistrictInstanceModel[]>();
    const [politicians, setPoliticians] = useState<PoliticianInstanceModel[]>();

    const [popData, setPopData] = useState(populationData);
    const [genData, setGenData] = useState(genderData);

    const [popChartjs, setPopChartjs] = useState<chartDataJs>();
    const [genChartjs, setGenChartjs] = useState<chartDataJs>();

    useEffect(() => {
        const fetchState = async () => {
            try {
                const { data: stateData } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT + "/state/" + id
                );
                setDisplayData(stateData);

                setPopData((prevState) => {
                    const items = [...prevState];
                    items[0].value = stateData.white_pop;
                    items[1].value = stateData.hispanic_pop;
                    items[2].value = stateData.asian_pop;
                    items[3].value = stateData.black_pop;
                    return items;
                });

                setGenData((prevState) => {
                    const items = [...prevState];
                    items[0].value = stateData.male_pop;
                    items[1].value = stateData.female_pop;
                    return items;
                });
            } catch (error) {
                console.error(error);
            }
        };

        const fetchDistrictsByState = async () => {
            try {
                const { data: districtsResponse } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/districts" +
                        getQueryString({ page: 1, state: id ?? "0" })
                );
                const districtsData = districtsResponse.data;
                setDistricts(districtsData);
            } catch (error) {
                console.error(error);
            }
        };

        const fetchPoliticiansByState = async () => {
            try {
                const { data: politiciansResponse } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/politicians" +
                        getQueryString({ page: 1, state: id ?? "0" })
                );
                const politiciansData = politiciansResponse.data;
                setPoliticians(politiciansData);
            } catch (error) {
                console.error(error);
            }
        };

        fetchState().then(() => {
            fetchDistrictsByState().then(() => {
                fetchPoliticiansByState();
                setPopChartjs({
                    labels: ["White", "Hispanic", "Asian", "Black"],
                    datasets: [
                        {
                            label: "# of Votes",
                            data: popData.map((d) => d.value),
                            backgroundColor: [
                                "#B5E19F",
                                "#F4A8D2",
                                "#C5AFF3",
                                "#FFE181",
                            ],
                            borderColor: [
                                "#85CE61",
                                "#EF7CBB",
                                "#A482ED",
                                "#FFC200",
                            ],
                            borderWidth: 1,
                        },
                    ],
                });
                setGenChartjs({
                    labels: ["Male", "Female"],
                    datasets: [
                        {
                            label: "# of Votes",
                            data: genData.map((d) => d.value),
                            backgroundColor: ["#A2D8F3", "#F3A2CE"],
                            borderColor: ["#70C4ED", "#ED70B4"],
                            borderWidth: 1,
                        },
                    ],
                });
            });
        });
    }, []);

    return (
        <Container className="mt-5 p-5 ">
            <Row>
                {displayData && (
                    <>
                        <h1>{displayData.state_name}</h1>
                        <Row className="state-instance-container">
                            <Col>
                                <StateMap stateDetails={displayData} />
                            </Col>
                            <Col md={3}>
                                <Card className="chart-card">
                                    <Card.Body>
                                        <Card.Title>
                                            Demographic Composition
                                        </Card.Title>
                                        <Col className="chart-pop">
                                            {popChartjs ? (
                                                <Pie data={popChartjs} />
                                            ) : null}
                                        </Col>
                                        <Col className="chart-gender">
                                            {genChartjs ? (
                                                <Pie data={genChartjs} />
                                            ) : null}
                                        </Col>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col>
                                <Row className="info-card-group">
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-blue"
                                                : "card-red"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>
                                                Political Leaning
                                            </Card.Title>
                                            {displayData.party_detailed}
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-red"
                                                : "card-blue"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>
                                                Total Population
                                            </Card.Title>
                                            {displayData.total_pop.toLocaleString(
                                                "en-US"
                                            )}{" "}
                                            people (approx.)
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-blue"
                                                : "card-red"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>Size</Card.Title>
                                            {displayData.sq_miles.toLocaleString(
                                                "en-US"
                                            )}{" "}
                                            sq mi
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-red"
                                                : "card-blue"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>Compactness</Card.Title>
                                            {displayData.state_compactness.toFixed(
                                                2
                                            )}
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-blue"
                                                : "card-red"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>
                                                Candidate Votes
                                            </Card.Title>
                                            {displayData.candidatevotes.toLocaleString(
                                                "en-US"
                                            )}
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-red"
                                                : "card-blue"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>Total Votes</Card.Title>
                                            {displayData.totalvotes.toLocaleString(
                                                "en-US"
                                            )}
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-blue"
                                                : "card-red"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>
                                                MOV (margin-of-victory)
                                            </Card.Title>
                                            {displayData.MOV.toFixed(2)}%
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-red"
                                                : "card-blue"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>
                                                Census Region
                                            </Card.Title>
                                            {displayData.census_region}
                                        </Card.Body>
                                    </Card>
                                    <Card
                                        className={
                                            displayData.party_detailed.toLowerCase() ===
                                            "democrat"
                                                ? "card-blue"
                                                : "card-red"
                                        }
                                    >
                                        <Card.Body>
                                            <Card.Title>
                                                Percent Minority
                                            </Card.Title>
                                            {displayData.non_white_percentage.toFixed(
                                                2
                                            )}
                                            %
                                        </Card.Body>
                                    </Card>
                                </Row>
                            </Col>
                        </Row>

                        <Row>
                            <h3>Districts in this state</h3>
                        </Row>

                        <Row xs={1} sm={2} md={3} lg={4} xl={5}>
                            {districts &&
                                districts.map((district) => (
                                    <Col
                                        key={
                                            district.state_abbrev +
                                            district.district_num
                                        }
                                    >
                                        <DetailCard
                                            to={
                                                "/districts/" +
                                                (district.state_abbrev +
                                                    "_" +
                                                    district.district_num)
                                            }
                                            itemKey={
                                                district.state_abbrev +
                                                district.district_num
                                            }
                                            content={
                                                <DistrictMap
                                                    districtDetails={district}
                                                />
                                            }
                                            title={getDistrictName(
                                                district.state_abbrev,
                                                district.district_num
                                            )}
                                        />
                                    </Col>
                                ))}
                        </Row>
                        <Row>
                            <h3>Politicians from this state</h3>
                        </Row>

                        <Row xs={1} sm={2} md={3} lg={4} xl={5}>
                            {politicians &&
                                politicians.map((politician) => (
                                    <Col
                                        key={
                                            politician.state_abbrev +
                                            "_" +
                                            politician.district_num
                                        }
                                    >
                                        <DetailCard
                                            to={
                                                "/politicians/" +
                                                politician.state_abbrev +
                                                "_" +
                                                politician.district_num
                                            }
                                            itemKey={
                                                politician.state_abbrev +
                                                "_" +
                                                politician.district_num
                                            }
                                            content={
                                                <Card.Img
                                                    variant="top"
                                                    src={politician.photo_url}
                                                    style={{
                                                        height: "20rem",
                                                        objectFit: "cover",
                                                        objectPosition:
                                                            "left top",
                                                    }}
                                                />
                                            }
                                            title={politician.name}
                                        />
                                    </Col>
                                ))}
                        </Row>
                    </>
                )}
            </Row>
        </Container>
    );
}
