import { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { DistrictInstanceModel } from "../models/district.model";
import DistrictMap from "../components/DistrictMap";
import DetailCard from "../components/DetailCard";
import TableNavbar from "../components/TableNavbar";
import ToggleViewButtonGroup from "../components/ToggleViewButtonGroup";
import axios from "axios";
import { PaginationComponent } from "../components/PaginationComponent";
import { Loading } from "../components/Loading";
import {
    PaginationResponseModel,
    PaginationParameterModel,
} from "../utils/pagination.model";
import { getQueryString } from "../utils/Query";
import { DistrictCard } from "../components/DistrictCard";
import DistrictTable from "../components/DistrictTable";

export default function Districts() {
    const searchPlaceholder =
        "Search by district number, state name, state abbr., representative, or political leaning";
    const [tableView, setTableView] = useState<boolean>(false);
    const [districts, setDistricts] = useState<DistrictInstanceModel[]>();

    const navbarDropdowns = [
        {
            title: "Political Leaning",
            items: ["Democrat", "Republican"],
        },
        {
            title: "Population",
            items: [
                "0-600,000",
                "600,001-900,000",
                "900,001-1,100,000",
                "range",
            ],
        },
        {
            title: "Size (sq. mi)",
            items: ["range"],
        },
        {
            title: "Minority Percentage",
            items: ["range"],
        },
        {
            title: "Compactness",
            items: ["range"],
        },
    ];

    const sortFields = [
        "Name",
        "Political Leaning",
        "Population",
        "Size (sq. mi)",
        "Minority Percentage",
        "Compactness",
    ];

    const paramToKey: { [name: string]: { key: string } } = {
        Name: { key: "state_abbrev" },
        "Political Leaning": { key: "party_detailed" },
        Population: { key: "total_pop" },
        "Size (sq. mi)": { key: "sq_miles" },
        "Minority Percentage": { key: "non_white_percentage" },
        Compactness: { key: "district_compactness" },
    };

    // Pagination
    const [paginationRes, setPaginationRes] =
        useState<PaginationResponseModel>();
    const [paginationPrams, setPaginationPrams] =
        useState<PaginationParameterModel>({ page: 1, page_size: 10 });

    function paginate(newCurPage: number): void {
        setPaginationPrams((prevState) => {
            return { ...prevState, page: newCurPage };
        });
    }

    function changeItemsPerPage(newCurItemsPerPage: number): void {
        let newPageNum = 1;
        if (paginationRes) {
            const numberOfPages =
                paginationRes.total_items / newCurItemsPerPage;
            newPageNum =
                numberOfPages < paginationRes.page_num
                    ? numberOfPages
                    : paginationRes.page_num;
        }
        setPaginationPrams({ page: newPageNum, page_size: newCurItemsPerPage });
    }

    function changeState(check: boolean) {
        setTableView(check);
    }

    //sort
    const [sortAscending, setSortAscending] = useState({ ascending: true });
    const [sortParam, setSortPram] = useState({ sort: "state_abbrev" });
    function changeSortParam(param: string) {
        const key = paramToKey[param].key;
        setSortPram({ sort: key });
    }

    //filter
    const [filterParams, setFilterParams] = useState<{
        [key: string]: string;
    }>({});
    function changeFilterParams(title: string, range: string) {
        const key = paramToKey[title].key;
        setFilterParams((prevState) => {
            return { ...prevState, [key]: range };
        });
    }

    function removeFilterParams(title: string) {
        const key = paramToKey[title].key;
        setFilterParams((prevState) => {
            const newItem = { ...prevState };
            if (key in newItem) {
                delete newItem[key];
                return newItem;
            } else {
                return prevState;
            }
        });
    }

    //Search
    const [searchParam, setSearchPram] = useState<{ query: string }>({
        query: "",
    });
    function handleSearch(newQuery: string) {
        setSearchPram({ query: newQuery });
    }

    useEffect(() => {
        const fetchDistricts = async () => {
            try {
                const { data: districtResponse } = await axios.get(
                    process.env.REACT_APP_API_ENDPOINT +
                        "/districts" +
                        getQueryString({
                            ...searchParam,
                            ...paginationPrams,
                            ...sortParam,
                            ...sortAscending,
                            ...filterParams,
                        })
                );
                const districtData = districtResponse.data;
                setDistricts(districtData);
                setPaginationRes(districtResponse.meta);
            } catch (error) {
                console.error(error);
            }
        };
        fetchDistricts();
    }, [paginationPrams, sortParam, sortAscending, filterParams, searchParam]);

    return (
        <Container className="mt-5 p-5">
            <h2>Congressional Districts</h2>
            <Row>
                <TableNavbar
                    dropdowns={navbarDropdowns}
                    sorts={sortFields}
                    searchPlaceholder={searchPlaceholder}
                    changeSortParam={changeSortParam}
                    changeSortAscending={(param) =>
                        setSortAscending({ ascending: param })
                    }
                    changeFilterParams={changeFilterParams}
                    removeFilterParams={removeFilterParams}
                    handleSearch={handleSearch}
                />
                <ToggleViewButtonGroup
                    changeState={changeState}
                    tableView={tableView}
                />
            </Row>

            {districts && tableView && (
                <Row className="table-container">
                    <DistrictTable
                        districts={districts}
                        searchTerm={searchParam.query}
                    />
                </Row>
            )}
            {districts && !tableView && (
                <Row xs={1} sm={2} md={3} lg={4} xl={5} className="g-3">
                    {districts.map((district) => (
                        <Col
                            key={district.state_abbrev + district.district_num}
                        >
                            <DetailCard
                                to={
                                    "/districts/" +
                                    (district.state_abbrev +
                                        "_" +
                                        district.district_num)
                                }
                                content={
                                    <DistrictMap districtDetails={district} />
                                }
                                details={
                                    <DistrictCard
                                        district={district}
                                        searchParam={searchParam.query}
                                    />
                                }
                            />
                        </Col>
                    ))}
                </Row>
            )}
            {districts && paginationRes && (
                <PaginationComponent
                    totalItems={paginationRes.total_items}
                    lastPage={Math.ceil(
                        paginationRes.total_items / paginationRes.page_size
                    )}
                    curPage={paginationRes.page_num}
                    paginate={paginate}
                    curItemPerPage={paginationRes.page_size}
                    changeItemsPerPage={changeItemsPerPage}
                    lastIndex={paginationRes.last_index}
                    firstIndex={paginationRes.first_index}
                />
            )}
            {!districts && <Loading />}
        </Container>
    );
}
