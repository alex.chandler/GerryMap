import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { getQueryString } from "../utils/Query";
import axios from "axios";
import StateTable from "../components/StateTable";
import DistrictTable from "../components/DistrictTable";
import PoliticianTable from "../components/PoliticianTable";
import { StateInstanceModel } from "../models/state.model";
import { DistrictInstanceModel } from "../models/district.model";
import { PoliticianInstanceModel } from "../models/politician.model";

interface ISearchPageProps {
    queryString?: string;
}

export default function SearchPage(props: ISearchPageProps) {
    const { query } = useParams();
    const [searchTerm] = useState<string>(
        props.queryString ? props.queryString : ""
    );
    const [states, setStates] = useState<StateInstanceModel[]>();
    const [districts, setDistricts] = useState<DistrictInstanceModel[]>();
    const [politicians, setPoliticians] = useState<PoliticianInstanceModel[]>();
    const performSearch = async () => {
        try {
            const { data: response } = await axios.get(
                process.env.REACT_APP_API_ENDPOINT +
                    "/search" +
                    getQueryString({ query: query === undefined ? "" : query })
            );
            setStates(response.states.data);
            setDistricts(response.districts.data);
            setPoliticians(response.politicians.data);
        } catch (error) {
            console.error(error);
        }
    };
    useEffect(() => {
        performSearch();
    }, [searchTerm]);
    return (
        <Container className="mt-5 p-5">
            {(query === undefined || query === "") && (
                <Row>Type something to search!</Row>
            )}
            {query && (
                <>
                    <Row>
                        <h2>
                            Search Results of: <b>{query}</b>
                        </h2>
                    </Row>
                    <Row className="mt-3">
                        <h3>States</h3>
                        <p>Results:{states ? states.length : "0"}</p>
                        {states && (
                            <StateTable states={states} searchTerm={query} />
                        )}
                    </Row>

                    <Row className="mt-3">
                        <h3>Districts</h3>
                        <p>Results:{districts ? districts.length : "0"}</p>
                        {districts && (
                            <DistrictTable
                                districts={districts}
                                searchTerm={query}
                            />
                        )}
                    </Row>
                    <Row className="mt-3">
                        <h3>Politicians</h3>
                        <p>Results:{politicians ? politicians.length : "0"}</p>
                        {politicians && (
                            <PoliticianTable
                                politicians={politicians}
                                searchTerm={query}
                            />
                        )}
                    </Row>
                </>
            )}
        </Container>
    );
}
