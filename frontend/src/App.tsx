// import React, { useState } from 'react';
import Header from "./components/Header";
import "./App.css";
// import Col from 'react-bootstrap/Col'
// import { render } from "react-dom";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage";
import About from "./pages/About";
import States from "./pages/States";
import StateInstance from "./pages/StateInstance";
import Districts from "./pages/Districts";
import DistrictInstance from "./pages/DistrictInstance";
import Politicians from "./pages/Politicians";
import PoliticianInstance from "./pages/PoliticianInstance";
import Faq from "./pages/Faq";
import SearchPage from "./pages/SearchPage";
import { ProviderVisualizations, Visualizations } from "./pages/Visualizations";

function App() {
    return (
        <>
            <Header />
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/about" element={<About />} />
                <Route path="/states" element={<States />} />
                {/* TODO: How to pass props to instance? */}
                <Route path="/states/:id" element={<StateInstance />} />
                <Route path="/districts" element={<Districts />} />
                <Route path="/districts/:id" element={<DistrictInstance />} />
                <Route path="/politicians" element={<Politicians />} />
                <Route
                    path="/politicians/:id"
                    element={<PoliticianInstance />}
                />
                <Route path="/faq" element={<Faq />} />
                <Route path="/search/:query" element={<SearchPage />} />
                <Route path="/search/" element={<SearchPage />} />
                <Route path="/visualizations/" element={<Visualizations />} />
                <Route
                    path="/provider-vis/"
                    element={<ProviderVisualizations />}
                />
            </Routes>
        </>
    );
}

export default App;
