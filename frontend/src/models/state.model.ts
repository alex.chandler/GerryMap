import { DemographicDetailModel } from "../utils/Charts";
import { PaginationResponseModel } from "../utils/pagination.model";

export interface StateModel {
    state_name: string;
    state_abbrev: string;
    census_region: string;
    sq_miles: number;
    party_detailed: string;
    non_white_percentage: number;
    state_compactness: number;
    total_pop: number;
    MOV: number;
    candidatevotes: number;
    totalvotes: number;
}

export interface StateInstanceModel
    extends StateModel,
        DemographicDetailModel {}

export interface StateResponseModel {
    data: StateInstanceModel[];
    meta: PaginationResponseModel;
}

export const states: StateResponseModel = {
    data: [
        {
            state_name: "California",
            state_abbrev: "CA",
            census_region: "West",
            party_detailed: "Democratic",
            state_compactness: 0.47596276374395163,
            sq_miles: 155831,
            total_pop: 39512223,
            white_pop: 23484958,
            hispanic_pop: 15574882,
            asian_pop: 5865435,
            black_pop: 2282144,
            male_pop: 19640794,
            female_pop: 19871429,
            non_white_percentage: 40.56,
            MOV: 16.6553,
            candidatevotes: 123,
            totalvotes: 12341,
        },
        {
            state_name: "Mississippi",
            state_abbrev: "MS",
            total_pop: 2976149,
            sq_miles: 46923,
            census_region: "South",
            party_detailed: "Republican",
            non_white_percentage: 41.97,
            state_compactness: 0.4998672088039461,
            white_pop: 1726970,
            hispanic_pop: 90143,
            asian_pop: 31154,
            black_pop: 1130608,
            male_pop: 1434957,
            female_pop: 1541192,
            MOV: 16.0379,
            candidatevotes: 123,
            totalvotes: 12341,
        },
        {
            state_name: "Missouri",
            state_abbrev: "MO",
            total_pop: 6137428,
            sq_miles: 68742,
            census_region: "South",
            party_detailed: "Republican",
            non_white_percentage: 18.16,
            state_compactness: 0.5057399201604464,
            white_pop: 5022939,
            hispanic_pop: 264334,
            asian_pop: 127154,
            black_pop: 703058,
            male_pop: 3008169,
            female_pop: 3129259,
            MOV: 11.9933,
            candidatevotes: 123,
            totalvotes: 12341,
        },
        {
            state_name: "Montana",
            state_abbrev: "MT",
            total_pop: 1068778,
            sq_miles: 145550,
            census_region: "Northwest",
            party_detailed: "Republican",
            state_compactness: 0.648384065104728,
            non_white_percentage: 12.01,
            white_pop: 940423,
            hispanic_pop: 40328,
            asian_pop: 8927,
            black_pop: 0, // undefined
            male_pop: 537170,
            female_pop: 531608,
            MOV: 13.6812,
            candidatevotes: 123,
            totalvotes: 12341,
        },
    ],

    meta: {
        total_items: 80,
        page_num: 10,
        page_size: 9,
        first_index: 1,
        last_index: 9,
    },
};
