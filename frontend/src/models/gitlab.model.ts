export interface Contributors {
    name: string;
    email: string;
    commits: number;
    additions: number;
    deletions: number;
}

export interface IssuesStatistics {
    statistics: {
        counts: {
            all: number;
            closed: number;
            opened: number;
        };
    };
}

export interface GitlabStat {
    userId: string;
    name: string[];
    commits: number;
    issues: number;
    openIssue: number;
    closeIssue: number;
    imagePath: string;
    unitTests: number;
    desc: string;
    type: string;
}
