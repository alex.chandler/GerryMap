import { PaginationResponseModel } from "../utils/pagination.model";

export interface PoliticianModel {
    name: string;
    state_abbrev: string;
    district_num: number;
    committees: string;
    party_detailed: string;
    office_addr: string;
    age: number;
    gender: string;
    years_in_office: number;
    missed_votes_pct: number;
    votes_with_party_pct: number;
    photo_url: string;
}

export interface PoliticianDetailModel {
    contact_site: string;
    phone_number: string;
    website: string;
    first_elected: string;
    bioguide_id: string;
    votesmart_id?: string;
    feccandid: string;
    twitter_id: string;
    youtube_id: string;
    facebook_id: string;
    birthdate: string;
}

export interface PoliticianInstanceModel
    extends PoliticianModel,
        PoliticianDetailModel {}

export interface PoliticianResponseModel {
    data: PoliticianInstanceModel[];
    meta: PaginationResponseModel;
}

export const politicians: PoliticianResponseModel = {
    data: [
        {
            age: 88,
            bioguide_id: "Y000033",
            birthdate: "1933-06-09",
            committees:
                "['Committee on Natural Resources', 'Committee on Transportation and Infrastructure']",
            contact_site: "https://donyoung.house.gov/forms/writeyourrep/",
            district_num: 0,
            facebook_id: "RepDonYoung",
            feccandid: "H6AK00045",
            first_elected: "1973",
            gender: "M",
            missed_votes_pct: 7.92,
            name: "Don Young",
            office_addr: "2314 Rayburn House Office Building",
            party_detailed: "REPUBLICAN",
            phone_number: "202-225-5765",
            photo_url: "",
            state_abbrev: "AK",
            //total_cash: 557662,
            twitter_id: "RepDonYoung",
            votes_with_party_pct: 87.92,
            //votesmart_id: "26717.0",
            website: "https://donyoung.house.gov",
            years_in_office: 49,
            youtube_id: "RepDonYoung",
        },
        {
            age: 2015,
            bioguide_id: "C001054",
            birthdate: "1958-06-17",
            committees:
                "['Committee on Natural Resources', 'Committee on Armed Services']",
            contact_site: "",
            district_num: 1,
            facebook_id: "",
            feccandid: "",
            first_elected: "2020",
            gender: "M",
            missed_votes_pct: 0.58,
            name: "Jerry Carl",
            office_addr:
                "1330 Longworth House Office Building, Washington, DC 20515",
            party_detailed: "REPUBLICAN",
            phone_number: "202-225-4931",
            photo_url: "",
            state_abbrev: "AL",
            //total_cash: 486262,
            twitter_id: "RepJerryCarl",
            votes_with_party_pct: 96.66,
            //votesmart_id: "",
            website: "https://carl.house.gov/",
            years_in_office: 2,
            youtube_id: "",
        },
    ],
    meta: {
        total_items: 200,
        page_num: 10,
        page_size: 9,
        first_index: 1,
        last_index: 10,
    },
};
