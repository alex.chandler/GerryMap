import { DemographicDetailModel } from "../utils/Charts";
import { PaginationResponseModel } from "../utils/pagination.model";

export interface DistrictModel {
    state_abbrev: string;
    district_num: number;
    representative: string;
    sq_miles: number;
    party_detailed: string;
    non_white_percentage: number;
    district_compactness: number;
    total_pop: number;
    MOV: number;
    candidatevotes: number;
    totalvotes: number;
    // voter_turnout?: number;
}

export interface DistrictInstanceModel
    extends DistrictModel,
        DemographicDetailModel {}

export interface DistrictResponseModel {
    data: DistrictInstanceModel[];
    meta: PaginationResponseModel;
}
export const districts: DistrictResponseModel = {
    data: [
        {
            state_abbrev: "CA",
            district_num: 3,
            representative: "N00030856",
            total_pop: 755811,
            sq_miles: 6184,
            party_detailed: "Republican",
            non_white_percentage: 31.32,
            district_compactness: 0.38935877243469896,
            white_pop: 519095,
            hispanic_pop: 228566,
            asian_pop: 85141,
            black_pop: 47499,
            male_pop: 377848,
            female_pop: 377963,
            MOV: 18.6951,
            candidatevotes: 123,
            totalvotes: 1234,
        },
        {
            state_abbrev: "CA",
            district_num: 21,
            representative: "N00033367",
            total_pop: 755811,
            sq_miles: 6730,
            party_detailed: "Republican",
            non_white_percentage: 28.9,
            district_compactness: 0.43730386745212113,
            white_pop: 519095,
            hispanic_pop: 228566,
            asian_pop: 85141,
            black_pop: 47499,
            male_pop: 377848,
            female_pop: 377963,
            MOV: 18.6951,
            candidatevotes: 123,
            totalvotes: 1234,
        },
        {
            state_abbrev: "CA",
            district_num: 48,
            representative: "N00037015",
            total_pop: 755811,
            sq_miles: 145,
            party_detailed: "Republican",
            non_white_percentage: 37.91,
            district_compactness: 0.5034010882260861,
            white_pop: 519095,
            hispanic_pop: 228566,
            asian_pop: 85141,
            black_pop: 47499,
            male_pop: 377848,
            female_pop: 377963,
            MOV: 18.6951,
            candidatevotes: 123,
            totalvotes: 1234,
        },
    ],
    meta: {
        total_items: 200,
        page_num: 10,
        page_size: 9,
        first_index: 1,
        last_index: 10,
    },
};
