from distutils.debug import DEBUG
from unittest import main, TestCase
import mock
from utils.state_abbreviations import at_large_states
from app import *
import logging


class Tests(TestCase):
    @classmethod
    def setUpClass(cls):
        app.app_context().push()
        cls.log = logging.getLogger("unittest")

    # tests for /states
    def test_get_states(self):
        with app.test_request_context():
            r = get_states()
            assert r.status_code == 200

            data = r.json["data"]
            assert len(data) == NUM_PER_PAGE

            # no args: should return first page, 9 items per page
            assert data[0]["state_abbrev"] == "AK"
            assert data[-1]["state_abbrev"] == "FL"

            meta = r.json["meta"]
            assert meta["total_items"] == 50
            assert meta["page_num"] == 1
            assert meta["page_size"] == NUM_PER_PAGE
            assert meta["first_index"] == 1
            assert meta["last_index"] == 9

    def test_get_states_pagination(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"page": 2, "page_size": 25}
            with mock.patch("app.request", m):
                r = get_states()

                data = r.json["data"]
                assert len(data) == 25
                assert data[0]["state_abbrev"] == "MT"
                assert data[-1]["state_abbrev"] == "WY"

                meta = r.json["meta"]
                assert meta["total_items"] == 50
                assert meta["page_num"] == 2
                assert meta["page_size"] == 25
                assert meta["first_index"] == 26
                assert meta["last_index"] == 50

            m.args = {"page": -1}
            with mock.patch("app.request", m):
                r = get_states()
                assert r.status_code == 404
                assert "error" in r.json
                assert "Invalid page number specified" in r.json["error"]

            m.args = {"page_size": -1}
            with mock.patch("app.request", m):
                r = get_states()
                assert r.status_code == 404
                assert "error" in r.json
                assert "Invalid page size specified" in r.json["error"]

    def test_get_states_search_sort(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "Central", "sort": "state_abbrev", "ascending": "false"}
            with mock.patch("app.request", m):
                r = get_states()
                prev_state = "ZZ"
                for item in r.json["data"]:
                    assert "Central" in item["census_region"]
                    assert prev_state > item["state_abbrev"]
                    prev_state = item["state_abbrev"]

    def test_get_states_filter(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"party_detailed": "republican"}
            with mock.patch("app.request", m):
                r = get_states()
                for item in r.json["data"]:
                    assert item["party_detailed"] == "REPUBLICAN"

    # tests for /state/<state>
    def test_get_state(self):
        r = get_state("TX")
        assert r.status_code == 200
        assert r.json["state_abbrev"] == "TX"

    def test_get_state_notfound(self):
        r = get_state("ZZ")
        assert r.status_code == 404
        assert "error" in r.json
        assert "Invalid state abbreviation specified" in r.json["error"]

    # tests for /districts
    def test_get_districts(self):
        with app.test_request_context():
            r = get_districts()
            assert r.status_code == 200

            data = r.json["data"]
            assert len(data) == NUM_PER_PAGE

            # no args: should return first page, 9 items per page
            assert (data[0]["state_abbrev"], data[0]["district_num"]) == ("AK", 0)
            assert (data[-1]["state_abbrev"], data[-1]["district_num"]) == ("AR", 1)

            meta = r.json["meta"]
            assert meta["total_items"] == 434
            assert meta["page_num"] == 1
            assert meta["page_size"] == NUM_PER_PAGE
            assert meta["first_index"] == 1
            assert meta["last_index"] == 9

    def test_get_districts_pagination(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"page": 2, "page_size": 1}
            with mock.patch("app.request", m):
                r = get_districts()

                data = r.json["data"]
                assert len(data) == 1
                assert data[0]["state_abbrev"] == "AL"
                assert data[0]["district_num"] == 1

                meta = r.json["meta"]
                assert meta["total_items"] == 434
                assert meta["page_num"] == 2
                assert meta["page_size"] == 1
                assert meta["first_index"] == 2
                assert meta["last_index"] == 2

            m.args = {"page_size": -1}
            with mock.patch("app.request", m):
                r = get_districts()
                assert r.status_code == 404
                assert "error" in r.json
                assert "Invalid page size specified" in r.json["error"]

    def test_get_districts_filter_state(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"state": "TX"}
            with mock.patch("app.request", m):
                r = get_districts()
                data = r.json["data"]
                assert len(data) == NUM_PER_PAGE
                for item in data:
                    assert item["state_abbrev"] == "TX"

                meta = r.json["meta"]
                assert meta["total_items"] == 36
                assert meta["page_num"] == 1
                assert meta["page_size"] == NUM_PER_PAGE
                assert meta["first_index"] == 1
                assert meta["last_index"] == 9

            m.args = {"state": "ZZ"}
            with mock.patch("app.request", m):
                r = get_districts()
                data = r.json["data"]
                assert len(data) == 0

                meta = r.json["meta"]
                assert meta["total_items"] == 0
                assert meta["page_num"] == 1
                assert meta["page_size"] == NUM_PER_PAGE
                assert meta["first_index"] == 0
                assert meta["last_index"] == 0

    def test_get_districts_filter_pop(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"total_pop": "700000-799999"}
            with mock.patch("app.request", m):
                r = get_districts()
                for item in r.json["data"]:
                    assert 700000 <= item["total_pop"] <= 799999

    def test_get_districts_search(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "texas"}
            with mock.patch("app.request", m):
                r = get_districts()
                for item in r.json["data"]:
                    assert item["state_abbrev"] == "TX"

    # tests for /district/<district>
    def test_get_district(self):
        r = get_district("TX_1")
        assert r.status_code == 200
        assert r.json["state_abbrev"] == "TX"
        assert r.json["district_num"] == 1

    def test_get_district_badquery(self):
        r = get_district("TX1")
        assert r.status_code == 404
        assert "error" in r.json
        assert (
            r.json["error"]
            == "Please specify a district using the format <state_abbrev>_<district_number>."
        )

    def test_get_district_notfound(self):
        r = get_district("TX_100")
        assert r.status_code == 404
        assert "error" in r.json
        assert r.json["error"] == "District #100 in TX not found"

    # tests for /politicians/
    def test_get_politicians_pagination(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"page": 49}
            with mock.patch("app.request", m):
                r = get_politicians()
                assert r.status_code == 200

                data = r.json["data"]
                assert len(data) == 3
                assert (data[0]["state_abbrev"], data[0]["district_num"]) == ("WV", 2)
                assert (data[-1]["state_abbrev"], data[-1]["district_num"]) == ("WY", 0)

                meta = r.json["meta"]
                assert meta["total_items"] == 435
                assert meta["page_num"] == 49
                assert meta["page_size"] == NUM_PER_PAGE
                assert meta["first_index"] == 433
                assert meta["last_index"] == 435

            m.args = {"page": -1}
            with mock.patch("app.request", m):
                r = get_politicians()
                assert "error" in r.json
                assert "Invalid page number specified" in r.json["error"]

            m.args = {"page": 500}
            with mock.patch("app.request", m):
                r = get_politicians()
                assert r.status_code == 200

                data = r.json["data"]
                assert len(data) == 0

                meta = r.json["meta"]
                assert meta["total_items"] == 435
                assert meta["page_num"] == 500
                assert meta["page_size"] == NUM_PER_PAGE
                assert meta["first_index"] == 0
                assert meta["last_index"] == 0

    def test_get_politican_filter_state(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"state": "TX"}
            with mock.patch("app.request", m):
                r = get_politicians()
                assert r.status_code == 200

                data = r.json["data"]
                assert len(data) == NUM_PER_PAGE
                for item in data:
                    assert item["state_abbrev"] == "TX"

                meta = r.json["meta"]
                assert meta["total_items"] == 36
                assert meta["page_num"] == 1
                assert meta["page_size"] == NUM_PER_PAGE
                assert meta["first_index"] == 1
                assert meta["last_index"] == 9

            m.args = {"state": "ZZ"}
            with mock.patch("app.request", m):
                r = get_politicians()
                assert r.status_code == 200

                data = r.json["data"]
                assert len(data) == 0

                meta = r.json["meta"]
                assert meta["total_items"] == 0
                assert meta["page_num"] == 1
                assert meta["page_size"] == NUM_PER_PAGE
                assert meta["first_index"] == 0
                assert meta["last_index"] == 0

    def test_get_politicians_filter_pop(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"age": "44-46"}
            with mock.patch("app.request", m):
                r = get_politicians()
                for item in r.json["data"]:
                    assert 44 <= item["age"] <= 46

    def test_get_politicians_search(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "doug"}
            with mock.patch("app.request", m):
                r = get_politicians()
                expected_results = set(["Doug LaMalfa", "Doug Lamborn"])

                for item in r.json["data"]:
                    assert item["name"] in expected_results

    # tests for /politician/<politician>
    def test_get_politician(self):
        r = get_politician("TX_1")
        assert r.status_code == 200
        assert r.json["state_abbrev"] == "TX"
        assert r.json["district_num"] == 1
        assert r.json["name"] == "Louie Gohmert"

    def test_get_politician_badquery(self):
        r = get_politician("TX1")
        assert r.status_code == 404
        assert "error" in r.json
        assert (
            r.json["error"]
            == "Please specify a district using the format <state_abbrev>_<district_number>."
        )

    def test_get_politician_notfound(self):
        r = get_politician("TX_100")
        assert r.status_code == 404
        assert "error" in r.json
        assert r.json["error"] == "No politician found for district #100 in TX"

    # tests for searches
    def test_search_state_name(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "texas"}
            with mock.patch("app.request", m):
                r = search()
                assert r.status_code == 200

                assert "states" in r.json
                assert "districts" in r.json
                assert "politicians" in r.json

                states_data = r.json["states"]["data"]
                assert len(states_data) == 1
                assert states_data[0]["state_abbrev"] == "TX"

                districts_data = r.json["districts"]["data"]
                assert len(districts_data) == NUM_PER_PAGE
                for item in districts_data:
                    assert item["state_abbrev"] == "TX"

                politicians_data = r.json["politicians"]["data"]
                assert len(politicians_data) == NUM_PER_PAGE
                for item in politicians_data:
                    assert item["state_abbrev"] == "TX"

    def test_search_state_abbrev(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "ak"}
            with mock.patch("app.request", m):
                r = search()
                assert r.status_code == 200

                assert "states" in r.json
                assert "districts" in r.json
                assert "politicians" in r.json

                states_data = r.json["states"]["data"]
                assert len(states_data) == 1
                assert states_data[0]["state_abbrev"] == "AK"

                districts_data = r.json["districts"]["data"]
                assert len(districts_data) == 1
                for item in districts_data:
                    assert item["state_abbrev"] == "AK"

                politicians_data = r.json["politicians"]["data"]
                assert len(politicians_data) == 1
                for item in politicians_data:
                    assert item["state_abbrev"] == "AK"

    def test_search_keyword(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "doug"}
            with mock.patch("app.request", m):
                r = search()
                assert r.status_code == 200

                expected_results = set(["Doug LaMalfa", "Doug Lamborn"])

                assert "states" in r.json
                assert "districts" in r.json
                assert "politicians" in r.json

                states_data = r.json["states"]["data"]
                assert len(states_data) == 0

                districts_data = r.json["districts"]["data"]
                assert len(districts_data) == len(expected_results)
                for item in districts_data:
                    assert item["representative"] in expected_results

                politicians_data = r.json["politicians"]["data"]
                assert len(politicians_data) == len(expected_results)
                for item in politicians_data:
                    assert item["name"] in expected_results

    def test_search_number(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"query": "0"}
            with mock.patch("app.request", m):
                r = search()
                assert r.status_code == 200

                assert "states" in r.json
                assert "districts" in r.json
                assert "politicians" in r.json

                # integer keywords should only be used to query district number
                states_data = r.json["states"]["data"]
                assert len(states_data) == 0

                districts_data = r.json["districts"]["data"]
                assert len(districts_data) == len(at_large_states)
                for item in districts_data:
                    assert item["state_abbrev"] in at_large_states

                politicians_data = r.json["politicians"]["data"]
                assert len(politicians_data) == len(at_large_states)
                for item in politicians_data:
                    assert item["state_abbrev"] in at_large_states

    def test_search_pagination(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {
                "query": "republican",
                "state_page": 3,
                "state_page_size": 3,
                "politician_page": 20,
                "politician_page_size": 11,
            }
            with mock.patch("app.request", m):
                r = search()
                assert r.status_code == 200

                assert "states" in r.json
                assert "districts" in r.json
                assert "politicians" in r.json

                # checking states pagination
                states_data = r.json["states"]["data"]
                assert len(states_data) == 3
                assert states_data[0]["state_abbrev"] == "ID"
                assert states_data[-1]["state_abbrev"] == "KS"
                for item in states_data:
                    assert item["party_detailed"] == "REPUBLICAN"

                states_meta = r.json["states"]["meta"]
                assert states_meta["total_items"] == 27
                assert states_meta["page_num"] == 3
                assert states_meta["page_size"] == 3
                assert states_meta["first_index"] == 7
                assert states_meta["last_index"] == 9

                # checking districts pagination (should be default)
                districts_data = r.json["districts"]["data"]
                assert len(districts_data) == NUM_PER_PAGE
                for item in districts_data:
                    assert item["party_detailed"] == "REPUBLICAN"
                assert (
                    districts_data[0]["state_abbrev"],
                    districts_data[0]["district_num"],
                ) == ("AK", 0)
                assert (
                    districts_data[-1]["state_abbrev"],
                    districts_data[-1]["district_num"],
                ) == ("AR", 2)

                districts_meta = r.json["districts"]["meta"]
                assert districts_meta["total_items"] == 212
                assert districts_meta["page_num"] == 1
                assert districts_meta["page_size"] == NUM_PER_PAGE
                assert districts_meta["first_index"] == 1
                assert districts_meta["last_index"] == 9

                # checking politicians pagination
                politicians_data = r.json["politicians"]["data"]
                assert len(politicians_data) == 4
                for item in politicians_data:
                    assert item["party_detailed"] == "REPUBLICAN"
                assert (
                    politicians_data[0]["state_abbrev"],
                    politicians_data[0]["district_num"],
                ) == ("WV", 1)
                assert (
                    politicians_data[-1]["state_abbrev"],
                    politicians_data[-1]["district_num"],
                ) == ("WY", 0)

                politicians_meta = r.json["politicians"]["meta"]
                assert politicians_meta["total_items"] == 213
                assert politicians_meta["page_num"] == 20
                assert politicians_meta["page_size"] == 11
                assert politicians_meta["first_index"] == 210
                assert politicians_meta["last_index"] == 213

    # tests for /address
    def test_get_address_none(self):
        with app.test_request_context():
            r = search_address()
            assert r.status_code == 404
            assert "error" in r.json
            assert r.json["error"] == "Please specify an address."

    def test_get_address_utexas(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"address": "110 Inner Campus Drive, Austin, TX 78705"}
            with mock.patch("app.request", m):
                r = search_address()
                assert r.status_code == 200

                assert "state" in r.json
                assert r.json["state"]["state_abbrev"] == "TX"

                assert "district" in r.json
                assert r.json["district"]["state_abbrev"] == "TX"
                assert r.json["district"]["district_num"] == 25

                assert "politician" in r.json
                assert r.json["district"]["state_abbrev"] == "TX"
                assert r.json["district"]["district_num"] == 25

    def test_get_address_atlarge(self):
        with app.test_request_context():
            m = mock.MagicMock()
            m.args = {"address": "120 4th St, Juneau, AK 99801"}
            with mock.patch("app.request", m):
                r = search_address()
                assert r.status_code == 200

                assert "state" in r.json
                assert r.json["state"]["state_abbrev"] == "AK"

                assert "district" in r.json
                assert r.json["district"]["state_abbrev"] == "AK"
                assert r.json["district"]["district_num"] == 0

                assert "politician" in r.json
                assert r.json["district"]["state_abbrev"] == "AK"
                assert r.json["district"]["district_num"] == 0


if __name__ == "__main__":
    import sys

    logging.basicConfig(stream=sys.stderr)
    logging.getLogger("unittest").setLevel(logging.DEBUG)
    main()
