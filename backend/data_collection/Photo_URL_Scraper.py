from this import d
from selenium import webdriver
import time


class Photo_URL_Scraper:
    def __init__(self, debug=False):
        self.debug = debug
        self.chromedriver_path = "/Applications/chromedriver"
        self.start_browser()
        if self.debug:
            self.get_photo_url("Ted Cruz")

    def start_browser(self):
        self.driver = webdriver.Chrome(self.chromedriver_path)

    def get_photo_url(self, name):
        url = (
            "https://www.google.co.in/search?q="
            + name
            + " politician"
            + "&source=lnms&tbm=isch"
        )
        time.sleep(0.5)
        self.driver.get(url)
        x_path_first_image = "//div[@class='islrc']//div[1]//a[1]//div[1]//img[1]"
        attempt_cnt = 0
        while attempt_cnt < 10:
            try:
                elem = self.driver.find_element_by_xpath(x_path_first_image)
                break
            except Exception as e:
                time.sleep(20)
                print("error with finding element")
                attempt_cnt += 1
        if attempt_cnt == 10:
            print("Unable to find Politician URL")
            return ""
        data_image_src = elem.get_attribute("src")
        if self.debug:
            self.driver.get(data_image_src)
        return data_image_src


# Photo_URL_Scraper(debug=True)
