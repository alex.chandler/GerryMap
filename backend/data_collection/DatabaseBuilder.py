import time
import requests as r
from data_collection.GoogleGeocodeAPI import GoogleGeocodeAPI
from data_collection.CensusAPIs import CensusAPIs
from data_collection.GoogleCivicAPI import GoogleCivicAPI
from data_collection.CommonUtils import *
from backend.created_data.temp_files.state_abbreviations import state_to_abbrev
from itertools import islice
import json
from datetime import date


""" The DatabaseBuilder class combines the GoogleGeocodeAPI, CensusAPIS, and GoogleCivicAPI to all work together. 
    Instead of having to launch each API separately, the DatabaseBuilder class gets state statistics, 
    district statistics, civic data, and representative information all in one."""


class DatabaseBuilder:
    def __init__(self, read_district_to_address_from_file=False):
        self.read_district_to_address_from_file = read_district_to_address_from_file
        self.geocodeAPIInstance = GoogleGeocodeAPI()
        self.CensusAPIsInstance = CensusAPIs()
        self.googleCivicAPIInstance = GoogleCivicAPI()

    def combine_info_into_databases(self):
        # DB1: State DB
        # DB2: District DB
        # DB3: House Representative DB

        # State Data Below:
        # State Census Data
        # State Compactness Information
        # Harvard state data
        state_census_df = self.filter_local_census_data(
            "backend/created_data/temp_files/census_state_data_filtered.csv"
        )
        state_extra_info = self.get_state_size_and_region_dict(
            "backend/downloaded_Data/US-State_Size_Region_Description.txt"
        )
        state_compactness_data = self.read_compactness_data(
            "backend/created_data/temp_files/stateCompactness.txt"
        )
        harvard_state_data = self.read_harvard_data(
            "backend/downloaded_Data/harvard/1976-2020-senate.csv"
        )
        google_civic_info = self.read_google_civic_info(
            "backend/created_data/temp_files/districtCivicInfo.json"
        )
        state_db = self.combine_info_into_state_database(
            state_census_df,
            state_compactness_data,
            harvard_state_data,
            google_civic_info,
            state_extra_info,
        )
        state_db.to_csv("backend/created_data/data_for_backend_server/state_df.csv")

        # District Data Below:
        # District Census Data
        district_census_df = self.filter_local_census_data(
            "backend/created_data/temp_files/census_district_data_filtered.csv"
        )
        # District Compactness Information
        district_compactness_data = self.read_compactness_data(
            "backend/created_data/temp_files/districtCompactness.txt"
        )
        # Harvard District Data
        harvard_district_data = self.read_harvard_data(
            r"backend/downloaded_Data/harvard/1976-2020-house.csv", district_level=True
        )
        district_db = self.combine_info_into_district_database(
            district_census_df,
            district_compactness_data,
            harvard_district_data,
            google_civic_info,
        )
        district_db.to_csv(
            "backend/created_data/data_for_backend_server/district_df.csv"
        )

        # Representative Info Below:
        # OpenSecrets Data
        open_secrets_data = self.read_open_secrets_data(
            "backend/created_data/temp_files/OpenSecretsData.csv"
        )
        politicians_df = self.combine_info_into_politicians_database(open_secrets_data)
        politicians_df.to_csv(
            "backend/created_data/data_for_backend_server/politician_df.csv"
        )

        return [state_db, district_db, politicians_df]

    def get_state_size_and_region_dict(self, state_info_file_path):
        state_file = open(state_info_file_path, "r")
        state_info = {}
        lines = state_file.readlines()
        for line in lines:
            info = line.split("State Flag")[1]
            _, state_name, size_km, size_miles, census_region, extra_text = info.split(
                "\t"
            )
            state_info[state_name] = (size_miles, census_region)
        return state_info

    # Simple Regex that separates numbers and characters
    def split_chars_from_num(self, str):
        regex = re.compile("([a-zA-Z]+)([0-9]+)")
        return regex.match(str).groups()

    # Creates # DB3: House Representative DB
    def combine_info_into_politicians_database(self, open_secrets_data):
        open_secrets_columns = list(open_secrets_data.columns)
        open_secrets_as_list = open_secrets_data.values.tolist()
        desired_columns = [
            "name",
            "state",
            "district",
            "party",
            "age",
            "years_in_office",
            "phone_number",
            "total_cash",
            "twitter_id",
            "congress_office",
            "facebook_id",
            "website",
        ]
        nested_filtered_info = []
        # Iterate through all names in open_secrets_as_list and extract iportant information
        for row in open_secrets_as_list:
            politician_name = row[open_secrets_columns.index("firstlast")]
            politician_last_name = row[open_secrets_columns.index("lastname")]
            politician_party = row[open_secrets_columns.index("party")]
            first_elected = row[open_secrets_columns.index("first_elected")]
            office = row[
                open_secrets_columns.index("office")
            ]  # TODO: skip if not representative
            # Skip Senators since they are out of scope for the project
            is_senator = "S1" in office or "S2" in office
            if is_senator:
                continue
            # print(office)
            state, district = self.split_chars_from_num(office)
            total_years_in_office = date.today().year - first_elected
            phone_number = row[open_secrets_columns.index("phone")]
            total_cash = row[open_secrets_columns.index("total")]
            twitter_id = row[open_secrets_columns.index("twitter_id")]
            congress_office = row[open_secrets_columns.index("congress_office")]
            facebook_id = row[open_secrets_columns.index("facebook_id")]
            website = row[open_secrets_columns.index("website")]

            try:
                birthday = row[open_secrets_columns.index("birthdate")].replace(
                    "/", "-"
                )
                birth_day_year, birth_day_month, birth_day_date = [
                    int(x) for x in birthday.split("-")
                ]
                age = (
                    date.today().year
                    - birth_day_year
                    - (
                        (date.today().month, date.today().day)
                        < (birth_day_month, birth_day_date)
                    )
                )
            except Exception as e:
                age = "N/A"
            desired_info = [
                politician_name,
                state,
                district,
                politician_party,
                age,
                total_years_in_office,
                phone_number,
                total_cash,
                twitter_id,
                congress_office,
                facebook_id,
                website,
            ]
            nested_filtered_info.append(desired_info)
        df = pd.DataFrame(nested_filtered_info, columns=desired_columns)
        return df

    def read_open_secrets_data(self, file_path):
        df = pd.read_csv(file_path)
        return df

    def filter_local_census_data(self, file_path):
        df = pd.read_csv(file_path)
        df.columns = df.iloc[0]
        df = df.iloc[1 : len(df), :]
        return df

    def read_compactness_data(self, file_path):
        with open(file_path) as file:
            lines = [line.rstrip().split("\t") for line in file]
        state_to_compactness = {x[0]: x[1] for x in lines}
        return state_to_compactness

    # Calculates the Margin of Victory
    def calculate_MOV(self, winning_candidate, losing_candidate):
        win_cnt = winning_candidate["candidatevotes"]
        ls_cnt = losing_candidate["candidatevotes"]
        MOV = (win_cnt - ls_cnt) / ((win_cnt + ls_cnt) / 2) * 100
        return MOV

    def read_harvard_data(self, file_path, district_level=False):
        df = pd.read_csv(file_path, encoding="latin1")
        if district_level:
            most_recent_data = df[
                df["year"] == 2020
            ]  # TODO make sure that the year is the same as other databases including representative info
        else:
            most_recent_data = df[
                (df["year"] == 2018) | (df["year"] == 2020)
            ]  # TODO make sure that the year is the same as other databases including representative info
        if "party_detailed" in most_recent_data.keys():
            party_var = "party_detailed"
        else:
            party_var = "party"
        # Filter down df to desired columns
        most_recent_data = most_recent_data[
            [
                "year",
                "state",
                "state_po",
                "district",
                "candidate",
                party_var,
                "candidatevotes",
                "totalvotes",
            ]
        ]
        most_recent_data.columns = [
            "year",
            "state",
            "state_po",
            "district",
            "candidate",
            party_var,
            "candidatevotes",
            "totalvotes",
        ]

        #     most_recent_data = most_recent_data.drop(columns=['office','special','runoff','state_fips', 'state_cen','state_ic', 'state_cen','stage', 'state_cen','writein','mode','unofficial','version','fusion_ticket'])
        # Only picks candidates that have at least half the votes
        winning_candidates_df = pd.DataFrame()
        # losing_candidates_df = pd.DataFrame(columns=most_recent_data.columns + ['MOV'])
        states = list(most_recent_data["state"].unique())
        for state in states:
            if state == "DISTRICT OF COLUMBIA":
                continue
            state_candidates = most_recent_data[most_recent_data["state"] == state]
            # Calculate MOV for two different elections if we have two separate election data for 2018 and 2020
            if len(state_candidates["year"].unique()) > 1:
                # winning_candidate = state_candidates.loc[state_candidates['candidatevotes'].idxmax()]
                state_candidates_multiple_years = [
                    state_candidates[state_candidates["year"] == 2018],
                    state_candidates[state_candidates["year"] == 2020],
                ]
                avg_MOV = 0
                for state_candidates_specific_year in state_candidates_multiple_years:
                    candidate_votes_np = state_candidates_specific_year[
                        "candidatevotes"
                    ].to_numpy()
                    largest_indices = (-candidate_votes_np).argsort()[:2]
                    winning_candidate = state_candidates_specific_year.iloc[
                        largest_indices[0]
                    ]
                    losing_candidate = state_candidates_specific_year.iloc[
                        largest_indices[1]
                    ]
                    specific_MOV = self.calculate_MOV(
                        winning_candidate, losing_candidate
                    )
                    avg_MOV += specific_MOV
                avg_MOV /= 2
                winning_candidate_with_MOV = pd.Series(
                    list(winning_candidate) + [avg_MOV]
                )
            # Else Calculate MOV for only one election
            else:
                if district_level:
                    min_district = 1
                    max_district = state_candidates["district"].max()
                    for district in range(min_district, max_district + 1):
                        district_candidates = state_candidates[
                            state_candidates["district"] == district
                        ]
                        candidate_votes_np = district_candidates[
                            "candidatevotes"
                        ].to_numpy()
                        largest_indices = (-candidate_votes_np).argsort()[:2]
                        # Make MOV 100 if only one candidate runs
                        if len(largest_indices) == 1:
                            MOV = 100
                        else:
                            winning_candidate = district_candidates.iloc[
                                largest_indices[0]
                            ]
                            losing_candidate = district_candidates.iloc[
                                largest_indices[1]
                            ]
                            MOV = self.calculate_MOV(
                                winning_candidate, losing_candidate
                            )
                        winning_candidate_with_MOV = pd.Series(
                            list(winning_candidate) + [MOV]
                        )
                        winning_candidates_df = winning_candidates_df.append(
                            winning_candidate_with_MOV, ignore_index=True
                        )

                else:
                    # winning_candidate = state_candidates.loc[state_candidates['candidatevotes'].idxmax()]
                    candidate_votes_np = state_candidates["candidatevotes"].to_numpy()
                    largest_indices = (-candidate_votes_np).argsort()[:2]
                    winning_candidate = state_candidates.iloc[largest_indices[0]]
                    losing_candidate = state_candidates.iloc[largest_indices[1]]
                    MOV = self.calculate_MOV(winning_candidate, losing_candidate)
                    winning_candidate_with_MOV = pd.Series(
                        list(winning_candidate) + [MOV]
                    )
            # losing_candidates_df = losing_candidates_df.append(losing_candidate, ignore_index = True)
            if not district_level:
                winning_candidates_df = winning_candidates_df.append(
                    winning_candidate_with_MOV, ignore_index=True
                )
        winning_candidates_df.columns = list(most_recent_data.columns) + ["MOV"]
        # x = most_recent_data[most_recent_data['state']=="ALABAMA"]
        # x.loc[x['candidatevotes'].idxmax()]

        # most_recent_data_winners = most_recent_data[most_recent_data['candidatevotes']*2 > most_recent_data['totalvotes']]
        # winning_candidates_df['MOV'] = winning_candidates_df.apply (lambda row: self.calculate_MOV(row), axis=1)
        return winning_candidates_df

    def read_google_civic_info(self, file_path):
        f = open(file_path)
        data = json.load(f)
        return data

    def get_senator_names_from_civic_data(self, google_civic_data_one_district):
        senator_1 = google_civic_data_one_district["senator1"]["name"]
        senator_2 = google_civic_data_one_district["senator2"]["name"]
        return [senator_1, senator_2]

    def get_rep_name_from_civic_data(self, google_civic_data_one_district):
        rep = google_civic_data_one_district["representative"]["name"]
        return rep

    def combine_info_into_district_database(
        self,
        district_census_df,
        district_compactness_data,
        harvard_district_data,
        google_civic_info,
    ):
        state_abrevs = district_census_df["state_abbrev"].unique()
        desired_data_nested = []
        desired_columns = []
        missing_census_data_cnt = 0
        missing_harvard_district_cnt = 0
        missing_google_civic_district_cnt = 0
        missing_compactness_data = 0
        # Loop through each state
        for index, state_abbrev in enumerate(state_abrevs):
            # Skip Puerto Rico
            if state_abbrev == "PR":
                continue
            census_data_one_state = district_census_df.loc[
                district_census_df["state_abbrev"] == state_abbrev
            ]
            # Extract Desired data for each district in each state
            for district in range(1, len(census_data_one_state) + 1):
                census_district_info = census_data_one_state[
                    census_data_one_state["district_num"] == str(district)
                ]
                census_district_info = census_data_one_state[
                    census_data_one_state["district_num"] == str(district)
                ]
                if census_district_info.empty:
                    census_district_info = census_data_one_state[
                        census_data_one_state["district_num"] == str("(at")
                    ]
                    census_district_info["district_num"] = census_district_info[
                        "district_num"
                    ].replace(["(at"], "1")
                    if census_district_info.empty:
                        census_district_info = pd.DataFrame(
                            [[state_abbrev, district] + ["N/A"] * 8],
                            columns=list(census_district_info.columns),
                        )
                        missing_census_data_cnt += 1
                try:
                    district_compactness = district_compactness_data[
                        state_abbrev + "_" + str(district)
                    ]
                except Exception as e:
                    missing_compactness_data += 1
                    district_compactness = "N/A"
                harvard_district_info = harvard_district_data[
                    (harvard_district_data["district"] == district)
                    & (harvard_district_data["state_po"] == state_abbrev)
                ]
                try:
                    harvard_district_unique_data = list(
                        harvard_district_info.values[0][4:9]
                    )
                except Exception as e:
                    harvard_district_unique_data = ["N/A"] * 5
                    missing_harvard_district_cnt += 1
                try:
                    google_civic_data_one_district = google_civic_info[
                        state_abbrev + str(district)
                    ]
                    house_rep = self.get_rep_name_from_civic_data(
                        google_civic_data_one_district
                    )
                except Exception as e:
                    missing_google_civic_district_cnt += 1
                    house_rep = "N/A"
                if index == 0:
                    desired_columns = (
                        list(census_district_info.columns)
                        + ["state_compactness"]
                        + list(harvard_district_info)[4:9]
                        + ["Representative"]
                    )
                desired_data = (
                    list(census_district_info.values[0])
                    + [district_compactness]
                    + list(harvard_district_unique_data)
                    + [house_rep]
                )
                desired_data_nested.append(desired_data)
            # TODO: fix the size mismatch between columns and lists
            # print("DEBUG")
        district_df = pd.DataFrame(desired_data_nested, columns=desired_columns)
        return district_df

    def combine_info_into_state_database(
        self,
        state_census_df,
        state_compactness_data,
        harvard_state_data,
        google_civic_info,
        state_extra_info,
    ):
        state_abrevs = state_census_df["state_abbrev"].unique()
        desired_data_nested = []
        desired_columns = []
        missing_harvard_state_cnt = 0
        missing_google_civic_state_cnt = 0
        abbrev_to_state = {v: k for k, v in state_to_abbrev.items()}

        for index, state_abbrev in enumerate(state_abrevs):
            # Skip Puerto Rico
            if state_abbrev == "PR":
                continue
            state_census_data_one_state = state_census_df.loc[
                state_census_df["state_abbrev"] == state_abbrev
            ]
            state_compactness = state_compactness_data[state_abbrev]
            harvard_state_data_one_state = harvard_state_data.loc[
                harvard_state_data["state_po"] == state_abbrev
            ]
            try:
                # TODO: confirm that 0 index is always right
                harvard_state_one_state_unique_data = [
                    harvard_state_data_one_state.values[0][1]
                ] + list(harvard_state_data_one_state.values[0][5:9])
            except Exception as e:
                harvard_state_one_state_unique_data = ["N/A"] * 5
                missing_harvard_state_cnt += 1
            # try:
            #     google_civic_data_one_district = google_civic_info[state_abbrev+"1"]
            #     senators = self.get_senator_names_from_civic_data(google_civic_data_one_district)
            # except Exception as e:
            #     missing_google_civic_state_cnt +=1
            #     senators = ["N/A"] * 2
            if index == 0:
                #     desired_columns =   list(state_census_data_one_state.columns) +     ['state_compactness'] + ["State Name"] + list(harvard_state_data_one_state)[5:9] +  ["Senators"]
                # desired_data =      list(state_census_data_one_state.values[0]) +   [state_compactness]   +   list(harvard_state_one_state_unique_data) +                 [senators]
                desired_columns = (
                    list(state_census_data_one_state.columns)
                    + ["state_compactness"]
                    + ["State Name"]
                    + list(harvard_state_data_one_state)[5:9]
                    + ["Size Miles", "Census Region"]
                )
            state_name = abbrev_to_state[state_abbrev]
            size_miles, census_region = state_extra_info[state_name]
            desired_data = (
                list(state_census_data_one_state.values[0])
                + [state_compactness]
                + list(harvard_state_one_state_unique_data)
                + [size_miles, census_region]
            )
            desired_data_nested.append(desired_data)
            # TODO: fix the size mismatch between columns and lists
            # print("DEBUG")
        state_df = pd.DataFrame(desired_data_nested, columns=desired_columns)

        return state_df

    # Performs all desired census API data collection
    def run_census_api(self):
        self.stateCensusInfo = self.CensusAPIsInstance.get_state_statistics()
        self.congressionalCensusInfo = self.CensusAPIsInstance.get_district_statistics()

    def obtain_civic_info_from_geocoordinates(self):
        self.geo_coords_for_districts = self.read_geo_coord_file(
            "backend/created_data/temp_files/districtcoords.json"
        )
        # Read results from file if district to address mapping already created  and stored locally
        if self.read_district_to_address_from_file:
            self.districts_with_addresses = self.read_district_to_address_from_file()
        else:
            self.districts_with_addresses = self.get_all_addresses(
                self.geo_coords_for_districts, debug_mode=False
            )
        # Call on Google API to info for all addresses
        self.districts_info_Civic = self.get_google_civic_info_for_all_addresses()

    def read_district_to_address_from_file(self):
        with open(
            "backend/created_data/temp_files/district_to_address.csv", newline=""
        ) as f:
            reader = csv.reader(f)
            data = list(reader)
            data = data[1::]  # clip off index
            data = [item for sublist in data for item in sublist]  # flattten list
            return {
                data[i]: data[i + 1] for i in range(0, len(data), 2)
            }  # convert to dictionary

    def get_google_civic_info_for_all_addresses(self):
        districts_info_Civic = {}
        api_call_cnt = 0
        for district, address in self.districts_with_addresses.items():
            api_call_cnt += 1
            district_str = district[0] + district[1]
            address_info = self.googleCivicAPIInstance.get_representative_info_by_address(
                address
            )
            # district_to_leg_info = dict(zip([district], [address_info]))
            districts_info_Civic[district_str] = address_info
            # TODO: check if need to add district as part of list or if it is already in the list
            # districts_info_Civic.append(district_to_leg_info)
        # Using a JSON string
        with open(
            "backend/created_data/temp_files/districtCivicInfo.json", "w"
        ) as outfile:
            outfile.write(json.dumps(districts_info_Civic))
        # districts_info_Civic_df = pd.DataFrame(districts_info_Civic)
        # TODO check if 'a' or 'w'
        # write_to_file("backend/created_data/temp_files/districts_info_Civic.csv", districts_info_Civic_df, append_mode = 'a', index=False, header=True)
        return districts_info_Civic

    def get_all_addresses(self, geo_coords_for_districts, debug_mode=True):
        # Take sample of geo_coords_for_districts for debugging purpose to avoid high API cost
        debug_size = 2
        district_to_address = {}
        if debug_mode:
            geo_coords_for_districts = dict(
                list(islice(geo_coords_for_districts.items(), debug_size))
            )

        for state_district_key in geo_coords_for_districts:
            skipped_keys = ["DC", "PR", "AS", "MP", "VI", "UM", "GU"]
            # Skip address calculations for non voting region
            if state_district_key[0:2] in skipped_keys:
                continue
            state, district_num = state_district_key.split("_")
            lat = geo_coords_for_districts[state_district_key]["lat"]
            lng = geo_coords_for_districts[state_district_key]["lng"]
            address = self.geocodeAPIInstance.get_address_for_geo_coord(lat, lng)
            # combined_info = (state, district_num, lat, lng, address)
            key = (state, district_num)
            district_to_address[key] = address
            # districts_with_addresses.append(combined_info)

        district_to_address_df = pd.DataFrame(
            list(district_to_address.items())
        )  # or list(d.items())
        write_to_file(
            "backend/created_data/temp_files/district_to_address.csv",
            district_to_address_df,
            append_mode="w",
            index=False,
            header=True,
        )
        return district_to_address

    def read_geo_coord_file(self, file_path):
        with open(file_path) as json_file:
            geo_coords_for_districts = json.load(json_file)
        return geo_coords_for_districts


DatabaseBuilderInstance = DatabaseBuilder()
DatabaseBuilderInstance.combine_info_into_databases()
# DatabaseBuilder = DatabaseBuilder(read_district_to_address_from_file = True)
# DatabaseBuilderInstance.run_census_api()
# DatabaseBuilderInstance.obtain_civic_info_from_geocoordinates()
