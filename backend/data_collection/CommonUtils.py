from dotenv import load_dotenv
from pathlib import Path
import os
import pandas as pd
import csv
import re
from Environment_tokens import *


def initialize_environment():
    env_path = Path(".") / ".env"
    load_dotenv(dotenv_path=env_path)


# Methods to write to local database
def write_to_file(filename, stuff_to_write, append_mode="w", index=False, header=True):
    pd.DataFrame(stuff_to_write).to_csv(
        filename, mode=append_mode, index=index, header=True
    )


def append_to_file_with_csv(filename, stuff_to_append):
    file = open(filename, "a+", newline="")
    # writing the data into the file
    with file:
        write = csv.writer(file)
        write.writerows(stuff_to_append)


def append_to_file(filename, stuff_to_append):
    f = open(filename, "a")
    f.write(stuff_to_append)
    f.close()


# Extract the last digits from a string with a regular expression
def get_trailing_nums(string):
    res = re.search(r"\d+$", string)
    if res:
        return res.group()
    else:
        raise ValueError("Unable to find trailing number")
