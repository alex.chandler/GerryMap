from CommonUtils import *
import time
import requests as r
from backend.created_data.temp_files.state_abbreviations import state_to_abbrev
from os.path import exists
from funcy import project  # pip install funcy

# A wrapper class designed to extract key information political information,
# notably personal, and financial information about each candidate.
class OpenSecretsAPI:
    def __init__(self, api_key=1):
        initialize_environment()
        if api_key == 1:
            self.API_KEY = get_open_secrets_API_token_1()
        elif api_key == 2:
            self.API_KEY = get_open_secrets_API_token_2()
        elif api_key == 3:
            self.API_KEY = get_open_secrets_API_token_3()
        else:
            self.API_KEY = get_open_secrets_API_token_4()
        self.getCandSummary_error_cnt = 0
        self.getCandContrib_error_cnt = 0
        self.getCandContrib_success_cnt = 0

    def print_debugging_stats(self):
        print("getCandSummary_error_cnt %d" % self.getCandSummary_error_cnt)
        print("getCandContrib_error_cnt %d" % self.getCandContrib_error_cnt)
        print("getCandContrib_success_cnt %d" % self.getCandContrib_success_cnt)

    def get_all_info(self, debugMode=True):
        state_abbrevs = list(state_to_abbrev.values())
        state_abbrevs = state_abbrevs[0:50]  # gets rid of non states
        state_abbrevs = state_abbrevs[::-1]  # reverses the states
        if debugMode:
            state_abbrevs = ["RI", "WY"]
        # df_keys = ['cid', 'firstlast', 'lastname', 'party', 'office', 'gender', 'first_elected', 'exit_code', 'comments', 'phone', 'fax', 'website', 'webform', 'congress_office', 'bioguide_id', 'votesmart_id', 'feccandid', 'twitter_id', 'youtube_url', 'facebook_id', 'birthdate', 'first_elected', 'next_election', 'total', 'spent', 'cash_on_hand', 'debt', 'origin', 'source', 'last_updated', 'contrib_info']
        for state in state_abbrevs:
            leg_keys, leg_nested_info_for_state = self.getLegInfoForState(state)
            file_path = "backend/created_data/tempfiles/OpenSecretsData.csv"
            if not exists(file_path):
                df = pd.DataFrame(leg_keys)
                df2 = pd.DataFrame(leg_nested_info_for_state)
                df = df.append(df2)
                write_to_file(file_path, df, append_mode="w")
            else:
                df = pd.read_csv(file_path)
                # check if indexing is first element
                # index_included = df.iloc[0][0] == "0" or df.iloc[0][0] == 0
                # if index_included:
                if df.columns[0] == "0":
                    new_header = df.iloc[0]  # grab the first row for the header
                    df = df[1:]  # take the data less the header row
                    df.columns = new_header  # set the header row as the df header
                for i in leg_nested_info_for_state:
                    a_series = pd.Series(i, index=df.columns)
                    df = df.append(a_series, ignore_index=True)
                write_to_file(file_path, df, append_mode="w")
        return df

    def getLegInfoForState(self, state_code):
        api_call = "http://www.opensecrets.org/api/?method=getLegislators&id={0}&output={1}&apikey={2}".format(
            state_code, "json", self.API_KEY
        )
        resp = r.get(api_call)
        resp_json = resp.json()
        leg_list = resp_json["response"]["legislator"]
        leg_nested_info = []
        leg_keys = []
        itr = 0

        for leg in leg_list:
            leg_info = leg["@attributes"]
            api_info_1 = list(leg_info.values())
            # desired_keys = ['cid', 'firstlast', 'lastname', 'party', 'office', 'gender', 'first_elected', 'exit_code', 'comments', 'phone', 'fax', 'website', 'webform', 'congress_office', 'bioguide_id', 'votesmart_id', 'feccandid', 'twitter_id', 'youtube_url', 'facebook_id', 'birthdate']
            leg_cid = leg_info["cid"]
            cand_Summary_keys, cand_summary = self.getCandSummary(leg_cid)
            candContribKeys, candContrib = self.getCandContrib(leg_cid)
            # leg_info.pop('exit_code', None)
            combined_info = api_info_1 + cand_summary + candContrib
            leg_nested_info.append(combined_info)
            # Note: for Texas, there are two TX)6 because one died in office
            if itr == 0:
                leg_keys = list(leg_info.keys()) + cand_Summary_keys + candContribKeys
            itr += 1

        return [leg_keys], leg_nested_info

    def getCandSummary(self, cid):
        time.sleep(3)
        api_call = "http://www.opensecrets.org/api/?method=candSummary&cid={0}&output={1}&apikey={2}".format(
            cid, "json", self.API_KEY
        )
        resp = r.get(api_call)
        desired_info_keys = [
            "first_elected",
            "next_election",
            "total",
            "spent",
            "cash_on_hand",
            "debt",
            "origin",
            "source",
            "last_updated",
        ]

        try:
            resp_json = resp.json()
            info = resp_json["response"]["summary"]["@attributes"]
            dsired_dict = project(info, desired_info_keys)
            extra_info_values = list(dsired_dict.values())
            assert len(extra_info_values) == len(desired_info_keys)
            return desired_info_keys, extra_info_values
        except Exception as e:
            self.getCandSummary_error_cnt += 1
            print("Error getting cand summary for cid %s " % cid)
            return desired_info_keys, [None] * len(desired_info_keys)

    def getCandContrib(self, cid):
        time.sleep(3)
        api_call = "https://www.opensecrets.org/api/?method=candContrib&cid={0}&output={1}&apikey={2}".format(
            cid, "json", self.API_KEY
        )
        resp = r.get(api_call)
        if "call limit" in resp.text:
            raise ValueError("Exceeded number of free calls")
        try:
            resp_json = resp.json()
            # candidate_name = resp_json['response']['contributors']['@attributes']['cand_name']
            contrib_info = resp_json["response"]["contributors"]["contributor"]
            self.getCandContrib_success_cnt += 1
            # info_in_list = [cid, candidate_name, contrib_info]
            return ["contrib_info"], [contrib_info]
        except Exception as e:
            self.getCandContrib_error_cnt += 1
            print("Error getting getCandContrib summary for cid %s " % cid)
            print(str(e))
            return ["contrib_info"], [[None]]
