from dotenv import load_dotenv
from pathlib import Path
import os


def initialize_environment():
    env_path = Path(".") / ".env"
    load_dotenv(dotenv_path=env_path)


# API access methods
def get_google_API_token():
    return os.environ["GOOGLE_GERRY_MAP_API"]


def get_twitter_API_token():
    return os.environ["TWITTER_API_TOKEN"]


def get_open_secrets_API_token_1():
    return os.environ["OPEN_SECRETS_API_TOKEN_1"]


def get_open_secrets_API_token_2():
    return os.environ["OPEN_SECRETS_API_TOKEN_2"]


def get_open_secrets_API_token_3():
    return os.environ["OPEN_SECRETS_API_TOKEN_3"]


def get_open_secrets_API_token_4():
    return os.environ["OPEN_SECRETS_API_TOKEN_4"]


def get_census_API_token():
    return os.environ["CENSUS_API_TOKEN"]


def get_mysql_password():
    return os.environ["MY_SQL_PASSWORD"]
