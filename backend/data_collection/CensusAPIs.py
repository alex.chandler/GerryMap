from data_collection.CommonUtils import *
import time
import requests as r
from backend.created_data.temp_files.state_abbreviations import state_to_abbrev

# Wrapper Class to the APIs listed on Census.gov
class CensusAPIs:
    def __init__(self):
        initialize_environment()
        self.API_KEY = get_census_API_token()
        # self.base_url_pop_estimates = "https://api.census.gov/data/2019/pep/charage?get=NAME,POP&for=state:*&key={0}".format(self.API_KEY)
        self.base_url_pop_detailed_estimates = "https://api.census.gov/data/2019/pep/charage?get=NAME,POP,RACE&for=state:*&key={0}".format(
            self.API_KEY
        )
        # B01003_001E = Population
        # B01003_001E	Estimate!!Total	TOTAL POPULATION
        # B02001_001E	Estimate!!Total	RACE
        # B02001_002E	Estimate!!Total!!White alone	RACE
        # B01001B_001E  Estimate!!Total	(Black)
        # B01001_026E   Estimate!!Total!!Female
        # B01001D_001E  Estimate!!Total	(ASIAN ALONE)
        # B01001H_001E  Estimate!!Total	 (WHITE ALONE, NOT HISPANIC OR LATINO)
        # B02001_002E   Estimate!!Total!!White alone
        self.desired_statistics = [
            "NAME",
            "B02001_001E",
            "B02001_002E",
            "B01001I_001E",
            "B01001B_001E",
            "B01001_002E",
            "B01001_026E",
            "B01001D_001E",
            "B01001H_001E",
            "B02001_002E",
        ]
        self.base_url_ACS_states = "https://api.census.gov/data/2019/acs/acs1?get={0},{1},{2},{3},{4},{5},{6},{7},{8},{9}&for=state:*&key={10}".format(
            *self.desired_statistics, self.API_KEY
        )

        self.base_url_ACS_district = "https://api.census.gov/data/2019/acs/acs1?get={0},{1},{2},{3},{4},{5},{6},{7},{8},{9}&for=congressional%20district:*&key={10}".format(
            *self.desired_statistics, self.API_KEY
        )

    """
    @Return: Desired statistics for each state (ex: Population, Size (Physical), % minority/non-white, ...)
    """

    def get_state_statistics(self):
        res = r.get(self.base_url_ACS_states)
        res_json = res.json()
        write_to_file(
            "backend/created_data/tempfiles/census_state_data_raw.csv", res_json
        )
        res_list = list(res_json)
        con_districts_des = res_list[0]
        index_state_name = con_districts_des.index("NAME")
        index_district_total_race = con_districts_des.index("B02001_001E")
        index_district_total_white = con_districts_des.index("B02001_002E")
        index_district_total_hispanic = con_districts_des.index("B01001I_001E")
        index_district_total_black = con_districts_des.index("B01001B_001E")
        index_district_total_male = con_districts_des.index("B01001_002E")
        index_district_total_female = con_districts_des.index("B01001_026E")
        index_district_total_asian = con_districts_des.index("B01001D_001E")
        index_district_total_white_alone = con_districts_des.index(
            "B01001H_001E"
        )  # SEX Category
        index_district_total_white_alone_2 = con_districts_des.index(
            "B02001_002E"
        )  # RACE Category
        res_list_congress_districts = res_list[1 : len(res_list) - 1]
        desired_statistics = []
        desired_statistics.append(
            (
                (
                    "state_abbrev",
                    "total_pop",
                    "white_pop",
                    "hispanic_pop",
                    "asian_pop",
                    "black_pop",
                    "male_pop",
                    "female_pop",
                    "non_white_percentage",
                )
            )
        )
        for cong_district in res_list_congress_districts:
            # Location Stats
            state_name = cong_district[index_state_name]
            state_abbrev = state_to_abbrev[state_name]

            # Population and Race Statistics
            total_pop = (
                int(cong_district[index_district_total_race])
                if cong_district[index_district_total_race]
                else "N/A"
            )
            white_pop = (
                int(cong_district[index_district_total_white])
                if cong_district[index_district_total_white]
                else "N/A"
            )
            hispanic_pop = (
                int(cong_district[index_district_total_hispanic])
                if cong_district[index_district_total_hispanic]
                else "N/A"
            )
            asian_pop = (
                int(cong_district[index_district_total_asian])
                if cong_district[index_district_total_asian]
                else "N/A"
            )
            # white_alone_pop = int(cong_district[index_district_total_white_alone]) if cong_district[index_district_total_white_alone] else "N/A"
            white_alone_pop_2 = (
                int(cong_district[index_district_total_white_alone_2])
                if cong_district[index_district_total_white_alone_2]
                else "N/A"
            )
            # ensure white_alone_pop and white_alone_pop_2 are close to one another
            # assert abs(white_alone_pop - white_alone_pop_2) * 10 < white_alone_pop
            # assert white_alone_pop == white_alone_pop_2
            black_pop = (
                int(cong_district[index_district_total_black])
                if cong_district[index_district_total_black]
                else "N/A"
            )
            # Gender Stats
            male_pop = int(cong_district[index_district_total_male])
            female_pop = int(cong_district[index_district_total_female])

            # Calculated Statistics
            non_white_pop = total_pop - white_alone_pop_2
            non_white_proportion = non_white_pop / total_pop
            non_white_percentage = "{:.2%}".format(non_white_proportion)
            cong_district_stats = (
                state_abbrev,
                total_pop,
                white_pop,
                hispanic_pop,
                asian_pop,
                black_pop,
                male_pop,
                female_pop,
                non_white_percentage,
            )
            desired_statistics.append(cong_district_stats)
        # Write statistics locally
        write_to_file(
            "backend/created_data/tempfiles/census_state_data_filtered.csv",
            desired_statistics,
        )
        return desired_statistics

    def get_state_populations(self):
        res = r.get(self.base_url_pop_detailed_estimates)
        res_json = res.json()
        print(res_json)
        return res_json

    # Note: Census does not contain voting data for districts, must use other information
    """
    @Return: Desired statistics for each district (ex: Population, Size (Physical), % minority/non-white, ...)
    """

    def get_district_statistics(self):
        res = r.get(self.base_url_ACS_district)
        res_json = res.json()
        write_to_file(
            "backend/created_data/tempfiles/census_district_data_raw.csv", res_json
        )
        res_list = list(res_json)
        con_districts_des = res_list[0]
        index_district_name = con_districts_des.index("NAME")
        index_district_total_race = con_districts_des.index("B02001_001E")
        index_district_total_white = con_districts_des.index("B02001_002E")
        index_district_total_hispanic = con_districts_des.index("B01001I_001E")
        index_district_total_black = con_districts_des.index("B01001B_001E")
        index_district_total_male = con_districts_des.index("B01001_002E")
        index_district_total_female = con_districts_des.index("B01001_026E")
        index_district_total_asian = con_districts_des.index("B01001D_001E")
        index_district_total_white_alone = con_districts_des.index("B01001H_001E")
        index_district_total_white_alone_2 = con_districts_des.index("B02001_002E")
        res_list_congress_districts = res_list[1 : len(res_list) - 1]
        desired_statistics = []
        desired_statistics.append(
            (
                (
                    "state_abbrev",
                    "district_num",
                    "total_pop",
                    "white_pop",
                    "hispanic_pop",
                    "asian_pop",
                    "black_pop",
                    "male_pop",
                    "female_pop",
                    "non_white_percentage",
                )
            )
        )
        for cong_district in res_list_congress_districts:
            # Location Stats
            district_description = cong_district[index_district_name]
            # Ignore Dadta for Non-Congressional Districts
            if "Congressional District" not in district_description:
                continue
            # Do not calculate statistics for District of Columbia because their Delegate does not vote
            if "District of Columbia" in district_description:
                continue
            district_num = district_description.split("Congressional District ")[
                1
            ].split()[0]
            state = district_description.split(", ")[-1]
            state_abbrev = state_to_abbrev[state]

            # Population and Race Statistics
            total_pop = (
                int(cong_district[index_district_total_race])
                if cong_district[index_district_total_race]
                else "N/A"
            )
            white_pop = (
                int(cong_district[index_district_total_white])
                if cong_district[index_district_total_white]
                else "N/A"
            )
            hispanic_pop = (
                int(cong_district[index_district_total_hispanic])
                if cong_district[index_district_total_hispanic]
                else "N/A"
            )
            asian_pop = (
                int(cong_district[index_district_total_asian])
                if cong_district[index_district_total_asian]
                else "N/A"
            )
            # white_alone_pop = cong_district[index_district_total_white_alone] if cong_district[index_district_total_white_alone] else "N/A"
            white_alone_pop_2 = (
                int(cong_district[index_district_total_white_alone_2])
                if cong_district[index_district_total_white_alone_2]
                else "N/A"
            )
            # ensure white_alone_pop and white_alone_pop_2 are close to one another
            # assert abs(white_alone_pop - white_alone_pop_2) * 10 < white_alone_pop
            black_pop = (
                int(cong_district[index_district_total_black])
                if cong_district[index_district_total_black]
                else "N/A"
            )

            # Gender Stats
            male_pop = int(cong_district[index_district_total_male])
            female_pop = int(cong_district[index_district_total_female])

            # Calculated Statistics
            non_white_pop = total_pop - white_alone_pop_2
            non_white_proportion = non_white_pop / total_pop
            non_white_percentage = "{:.2%}".format(non_white_proportion)
            cong_district_stats = (
                state_abbrev,
                district_num,
                total_pop,
                white_pop,
                hispanic_pop,
                asian_pop,
                black_pop,
                male_pop,
                female_pop,
                non_white_percentage,
            )
            desired_statistics.append(cong_district_stats)
        # Write the statistics for districts locally
        write_to_file(
            "backend/created_data/tempfiles/census_district_data_filtered.csv",
            desired_statistics,
        )
        return desired_statistics
