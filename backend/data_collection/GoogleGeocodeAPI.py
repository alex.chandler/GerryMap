from data_collection.CommonUtils import *
import time
import requests as r

# A Class designed to convert geolofical coordinates into addresses that can be used by the GoogleCivicAPI
class GoogleGeocodeAPI:
    def __init__(self):
        initialize_environment()
        self.API_KEY = get_google_API_token()
        self.base_url = "https://maps.googleapis.com/maps/api/geocode"

    # Note: All latitude and longitude values are °N and °W as purpose of project for United States only
    def get_address_for_geo_coord(self, lat, long):
        time.sleep(0.5)
        request_url = self.base_url + "/json?latlng={0},{1}&key={2}".format(
            lat, long, self.API_KEY
        )
        resp = r.get(request_url)
        if resp.status_code != 200:
            raise ValueError("Invalid Response from Geocode API")
        resp_json = resp.json()
        # Get first address for the given latitude and longitude
        formatted_address = resp_json["results"][0]["formatted_address"]
        cnt = 0
        # If the address cannot be determined, select a bigger area unit until "USA" is part of the address
        while "USA" not in formatted_address:
            formatted_address = resp_json["results"][cnt]["formatted_address"]
            cnt += 1
        return formatted_address


# GoogleGeocodeAPIInstance = GoogleGeocodeAPI()
# GoogleGeocodeAPIInstance.get_address_for_geo_coord(30.2842087489618, -97.7366949605631)
