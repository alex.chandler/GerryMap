from data_collection.CommonUtils import *
import time
import requests as r

# A class designed to get political information about representatives and senators of each district and state, respectively.
class GoogleCivicAPI:
    def __init__(self):
        initialize_environment()
        self.API_KEY = get_google_API_token()
        self.base_url = "https://www.googleapis.com/civicinfo/v2"
        self.address_query_failure_cnt = 0
        self.address_query_attempt_cnt = 0

    # For debugging purposes
    def report_metricts(self):
        print("self.address_query_attempt_cnt  %d" % self.address_query_attempt_cnt)
        print("self.address_query_failure_cnt  %d" % self.address_query_failure_cnt)

    def get_representative_info_by_address(self, address):
        rep_api_url = self.base_url + "/representatives?address=" + str(address)
        params = {"key": self.API_KEY}
        time.sleep(3)
        res = r.get(rep_api_url, params=params)
        if res.status_code == 200:
            resp_dict = res.json()
            desired_info = self.extract_info_from_address_query(resp_dict)
            return desired_info
        else:
            raise Exception("Response status code: %d" % res.status_code)

    def extract_info_from_address_query(self, resp_dict):
        self.address_query_attempt_cnt += 1
        state = resp_dict["normalizedInput"]["state"]
        try:
            senators_position_info = resp_dict["offices"][2]
            senator_indices = senators_position_info["officialIndices"]
            assert senators_position_info["name"] == "U.S. Senator"
            # First Senator
            senator_1_personal_info = resp_dict["officials"][senator_indices[0]]
            senator_1_personal_info_filtered = self.filter_rep_info(
                senator_1_personal_info, state
            )
            # Second Senator
            senator_2_personal_info = resp_dict["officials"][senator_indices[1]]
            senator_2_personal_info_filtered = self.filter_rep_info(
                senator_2_personal_info, state
            )
        except Exception as e:
            print("Error 3: finding senator information")
            self.address_query_failure_cnt += 1
            senator_1_personal_info_filtered = None
            senator_2_personal_info_filtered = None

        try:
            rep_position_info_1 = resp_dict["offices"][3]
            rep_indices = rep_position_info_1["officialIndices"]
            county_number = int(get_trailing_nums(rep_position_info_1["divisionId"]))
            assert (
                type(county_number) == int and county_number > 0 and county_number < 60
            )
            rep_personal_info = resp_dict["officials"][rep_indices[0]]
            rep_personal_info_filtered = self.filter_rep_info(rep_personal_info, state)
        except Exception as e1:
            print(
                "ERROR 2 in finding representative info in extract_info_from_address_query"
            )
            self.address_query_failure_cnt += 1
            rep_personal_info_filtered = None

        pol_info_for_address = {
            "representative": rep_personal_info_filtered,
            "senator1": senator_1_personal_info_filtered,
            "senator2": senator_2_personal_info_filtered,
        }

        return pol_info_for_address

    def filter_rep_info(self, rep_info, state):
        senator_name = rep_info["name"]
        senator_party = rep_info["party"]
        senator_contact = rep_info.get("phones", None)[0]
        senator_url = rep_info.get("urls", None)[0]
        photo_url = rep_info.get("photoUrl", None)
        # senator_facebook = Not needed as information is already in the OpenSecrets API
        # senator_twitter  Not needed as information is already in the OpenSecrets API
        #            'facebook' : senator_facebook,
        #    'senator_twitter' : senator_twitter,
        senator_personal_info_filtered = {
            "name": senator_name,
            "state": state,
            "party": senator_party,
            "contact": senator_contact,
            "website": senator_url,
            "photo_url": photo_url,
        }

        return senator_personal_info_filtered
