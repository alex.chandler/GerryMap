from sqlalchemy import *
from flask_sqlalchemy import SQLAlchemy
from marshmallow import Schema

db = SQLAlchemy()


class Politician(db.Model):
    name = db.Column("name", String(50))
    state_abbrev = db.Column("state_abbrev", String(2), primary_key=True)
    district_num = db.Column("district_num", Integer, primary_key=True)
    party_detailed = db.Column("party_detailed", String(15))
    age = db.Column("age", Integer)
    years_in_office = db.Column("years_in_office", Integer)
    phone_number = db.Column("phone_number", String(15))
    total_cash = db.Column("total_cash", Integer)
    twitter_id = db.Column("twitter_id", String(15))
    committees = db.Column("committees", String(200))
    first_elected = db.Column("first_elected", String(15))
    missed_votes_pct = db.Column("missed_votes_pct", Float)
    facebook_id = db.Column("facebook_id", String(50))
    bioguide_id = db.Column("bioguide_id", String(50))
    contact_site = db.Column("contact_site", String(75))
    youtube_id = db.Column("youtube_id", String(50))
    votes_with_party_pct = db.Column("votes_with_party_pct", Float)
    votesmart_id = db.Column("votesmart_id", String(10))
    birthdate = db.Column("birthdate", String(10))
    website = db.Column("website", String(50))
    photo_url = db.Column("photo_url", Text())
    gender = db.Column("gender", String(50))
    feccandid = db.Column("feccandid", String(10))
    office_addr = db.Column("office_addr", String(75))

    def __init__(
        self,
        name="NaN",
        state_abbrev="NaN",
        district_num=-1,
        party_detailed="NaN",
        age=-1,
        years_in_office=0,
        phone_number="NaN",
        total_cash=0.0,
        twitter_id="NaN",
        committees="NaN",
        first_elected="NaN",
        missed_votes_pct="NaN",
        facebook_id="NaN",
        bioguide_id="NaN",
        contact_site="NaN",
        youtube_id="NaN",
        votes_with_party_pct=0.0,
        votesmart_id="NaN",
        birthdate="NaN",
        website="NaN",
        photo_url="NaN",
        gender="NaN",
        feccandid="NaN",
        office_addr="NaN",
    ):
        self.name = (name,)
        self.state_abbrev = (state_abbrev,)
        self.district_num = (district_num,)
        self.party_detailed = (party_detailed,)
        self.age = (age,)
        self.years_in_office = (years_in_office,)
        self.phone_number = (phone_number,)
        self.total_cash = (total_cash,)
        self.twitter_id = (twitter_id,)
        self.committees = (committees,)
        self.first_elected = (first_elected,)
        self.missed_votes_pct = (missed_votes_pct,)
        self.facebook_id = (facebook_id,)
        self.bioguide_id = (bioguide_id,)
        self.contact_site = (contact_site,)
        self.youtube_id = (youtube_id,)
        self.votes_with_party_pct = (votes_with_party_pct,)
        self.votesmart_id = (votesmart_id,)
        self.birthdate = (birthdate,)
        self.website = (website,)
        self.photo_url = (photo_url,)
        self.gender = (gender,)
        self.feccandid = (feccandid,)
        self.office_addr = office_addr


class District(db.Model):
    state_abbrev = db.Column("state_abbrev", String(2), primary_key=True)
    district_num = db.Column("district_num", Integer, primary_key=True)
    total_pop = db.Column("total_pop", Integer)
    white_pop = db.Column("white_pop", Integer)
    hispanic_pop = db.Column("hispanic_pop", Integer)
    asian_pop = db.Column("asian_pop", Integer)
    black_pop = db.Column("black_pop", Integer)
    male_pop = db.Column("male_pop", Integer)
    female_pop = db.Column("female_pop", Integer)
    non_white_percentage = db.Column("non_white_percentage", Float)
    district_compactness = db.Column(
        "district_compactness", Float
    )  # TODO: confirm integrity of this variable in databse, make sure compactness score is not for state unlike columbn name suggests
    representative = db.Column("representative", String(50))
    party_detailed = db.Column("party_detailed", String(15))
    candidatevotes = db.Column("candidatevotes", Integer)
    totalvotes = db.Column("totalvotes", Integer)
    MOV = db.Column("MOV", Float)
    sq_miles = db.Column("sq_miles", Integer)

    def __init__(
        self,
        state_abbrev="NaN",
        district_num=-1,
        total_pop=-1,
        white_pop=-1,
        hispanic_pop=-1,
        asian_pop=-1,
        black_pop=-1,
        male_pop=-1,
        female_pop=-1,
        non_white_percentage=0.0,
        district_compactness=0.0,
        representative="NaN",
        party_detailed="NaN",
        candidatevotes=-1,
        totalvotes=-1,
        MOV=0.0,
        sq_miles=-1,
    ):
        self.state_abbrev = (state_abbrev,)
        self.district_num = (district_num,)
        self.total_pop = (total_pop,)
        self.white_pop = (white_pop,)
        self.hispanic_pop = (hispanic_pop,)
        self.asian_pop = (asian_pop,)
        self.black_pop = (black_pop,)
        self.male_pop = (male_pop,)
        self.female_pop = (female_pop,)
        self.non_white_percentage = (non_white_percentage,)
        self.district_compactness = (district_compactness,)
        self.representative = (representative,)
        self.party_detailed = (party_detailed,)
        self.candidatevotes = (candidatevotes,)
        self.totalvotes = (totalvotes,)
        self.MOV = (MOV,)
        self.sq_miles = (sq_miles,)


class State(db.Model):
    state_abbrev = db.Column("state_abbrev", String(2), primary_key=True)
    total_pop = db.Column("total_pop", Integer)
    white_pop = db.Column("white_pop", Integer)
    hispanic_pop = db.Column("hispanic_pop", Integer)
    asian_pop = db.Column("asian_pop", Integer)
    black_pop = db.Column("black_pop", Integer)
    male_pop = db.Column("male_pop", Integer)
    female_pop = db.Column("female_pop", Integer)
    non_white_percentage = db.Column("non_white_percentage", Float)
    state_compactness = db.Column("state_compactness", Float)
    state_name = db.Column(
        "state_name", String(20)
    )  # TODO change var state_name to diff name
    party_detailed = db.Column("party_detailed", String(15))
    candidatevotes = db.Column("candidatevotes", Integer)
    totalvotes = db.Column("totalvotes", Integer)
    MOV = db.Column("MOV", Float)
    sq_miles = db.Column("sq_miles", Integer)
    census_region = db.Column("census_region", String(20))
    princeton_report = db.Column("photo_url", Text())

    def __init__(
        self,
        state_abbrev="NaN",
        total_pop=-1,
        white_pop=-1,
        hispanic_pop=-1,
        asian_pop=-1,
        black_pop=-1,
        male_pop=-1,
        female_pop=-1,
        non_white_percentage=0.0,
        state_compactness=0.0,
        state_name="NaN",
        party_detailed="NaN",
        candidatevotes=-1,
        totalvotes=-1,
        MOV=0.0,
        sq_miles=-1,
        census_region="NaN",
        princeton_report="{}",
    ):
        self.state_abbrev = state_abbrev
        self.total_pop = total_pop
        self.white_pop = white_pop
        self.hispanic_pop = hispanic_pop
        self.asian_pop = asian_pop
        self.black_pop = black_pop
        self.male_pop = male_pop
        self.female_pop = female_pop
        self.non_white_percentage = non_white_percentage
        self.state_compactness = state_compactness
        self.state_name = state_name
        self.party_detailed = party_detailed
        self.candidatevotes = candidatevotes
        self.totalvotes = totalvotes
        self.MOV = MOV
        self.sq_miles = sq_miles
        self.census_region = census_region
        self.princeton_report = princeton_report


class StateSchema(Schema):
    class Meta:
        fields = (
            "state_abbrev",
            "total_pop",
            "white_pop",
            "hispanic_pop",
            "asian_pop",
            "black_pop",
            "male_pop",
            "female_pop",
            "non_white_percentage",
            "state_compactness",
            "state_name",
            "party_detailed",
            "candidatevotes",
            "totalvotes",
            "MOV",
            "sq_miles",
            "census_region",
            "princeton_report",
        )
        ordered = True


class DistrictSchema(Schema):
    class Meta:
        fields = (
            "state_abbrev",
            "district_num",
            "total_pop",
            "white_pop",
            "hispanic_pop",
            "asian_pop",
            "black_pop",
            "male_pop",
            "female_pop",
            "non_white_percentage",
            "district_compactness",
            "representative",
            "party_detailed",
            "candidatevotes",
            "totalvotes",
            "MOV",
            "sq_miles",
        )
        ordered = True


class PoliticianSchema(Schema):
    class Meta:
        fields = (
            "name",
            "state_abbrev",
            "district_num",
            "party_detailed",
            "age",
            "years_in_office",
            "phone_number",
            "total_cash",
            "twitter_id",
            "committees",
            "first_elected",
            "missed_votes_pct",
            "facebook_id",
            "bioguide_id",
            "contact_site",
            "youtube_id",
            "votes_with_party_pct",
            "votesmart_id",
            "birthdate",
            "website",
            "photo_url",
            "gender",
            "feccandid",
            "office_addr",
        )
        ordered = True
