# import os
# import sys
# import inspect
# currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# parentdir = os.path.dirname(currentdir)
# sys.path.insert(0, parentdir)
# sys.path.append('../backend')

# from os import path
import sys
from cmath import nan
import pandas as pd
import json
import os

current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
parent_parent = os.path.dirname(parent)
sys.path.append(parent_parent)
# from data_collection.Photo_URL_Scraper import Photo_URL_Scraper
import math
import requests as r

at_large = set(["ND", "AK", "WY", "DE", "SD", "VT", "MT"])
import time
from backend.utils.state_abbreviations import state_to_abbrev

# Returns a dictionary of state to princeton gerrymandering statistics
def extract_princeton_report_card_data():
    from backend.downloaded_Data.princeton_state_too_data import state_to_data_url

    all_states = state_to_abbrev.keys()
    states_with_data = state_to_data_url.keys()
    desired_stat_keys = [
        "geographicScore",
        "minorityCompositionScore",
        "competitivenessScore",
        "partisanScore",
        "partisanAdvantage",
        "overallGrade",
    ]
    missing_states = list(set(all_states) - set(states_with_data))
    state_to_data = {}
    for state, data_url in state_to_data_url.items():
        time.sleep(0.3)
        assert state in state_to_abbrev.keys()
        data_json = r.get(data_url).json()
        try:
            report_card = data_json["report_card"]
            desired_stat_values = [report_card[i] for i in desired_stat_keys]
            desired_data = {
                key: value
                for (key, value) in zip(desired_stat_keys, desired_stat_values)
            }
            state_to_data[state] = desired_data
        except:
            state_to_data[state] = {
                key: value
                for (key, value) in zip(
                    desired_stat_keys, [None] * (len(desired_stat_keys))
                )
            }
    for state in missing_states:
        state_to_data[state] = {
            key: value
            for (key, value) in zip(
                desired_stat_keys, [None] * (len(desired_stat_keys))
            )
        }
    return state_to_data


def add_princeton_data_to_df():
    princeton_data = extract_princeton_report_card_data()
    state_df = pd.read_csv(r"backend/created_data/data_for_backend_server/state_df.csv")
    abbrev_to_state = {v: k for k, v in state_to_abbrev.items()}

    data_in_order = []
    for row in state_df.iterrows():
        state_abbrev = row[1]["state_abbrev"]
        state = abbrev_to_state[state_abbrev]
        data_for_state = princeton_data[state]
        data_in_order.append(data_for_state)

    state_df["Princeton Data"] = data_in_order
    state_df.to_csv("backend/created_data/data_for_backend_server/state_df.csv")


def process_districts():
    district_df = pd.read_csv(
        r"backend/created_data/data_for_backend_server/district_df.csv"
    )
    politicians_df = pd.read_csv(
        r"backend/created_data/data_for_backend_server/politician_df.csv"
    )
    district_reps = {}
    for index, row in politicians_df.iterrows():
        state = row["state"]
        district_num = row["district"]
        if state in at_large:
            district_num = 0
        district_reps[(state, district_num)] = row["name"]

    district_size = {}

    district_size_file = open("districtArea.txt", "r")

    for line in district_size_file:
        p = line.strip().split()
        n = p[0].split("_")
        district_name = (n[0], int(n[1]))
        area = int(p[1])
        district_size[district_name] = area

    district_compactness = {}
    district_compactness_file = open("districtCompactness.txt")
    for line in district_compactness_file:
        p = line.strip().split()
        n = p[0].split("_")
        district_name = (n[0], int(n[1]))
        compactness = float(p[1])
        district_compactness[district_name] = compactness

    def get_area(row):
        state_abbrev = row["state_abbrev"]
        district_number = row["district_num"]
        n = (state_abbrev, district_number)
        if n in district_size:
            return district_size[n]
        else:
            print(n, "NOT FOUND")

    def get_compactness(row):
        state_abbrev = row["state_abbrev"]
        district_number = row["district_num"]
        n = (state_abbrev, district_number)
        if n in district_compactness:
            return district_compactness[n]
        else:
            print(n, "NOT FOUND")

    def standardize_number(row):
        district_number = row["district_num"]
        state_abbrev = row["state_abbrev"]
        if state_abbrev in at_large and district_number == 1:
            return 0
        return district_number

    def get_politicians(row):
        # candidate = row['candidate']
        # representative = row['Representative']

        # if pd.isna(candidate):
        #     if not pd.isna(representative):
        #         candidate = representative.upper()
        #     else:
        #         print(row['state_abbrev'], row['district_num'])
        district_number = row["district_num"]
        state_abbrev = row["state_abbrev"]

        return district_reps[(state_abbrev, district_number)]

    # adds column for land area
    # convert district-at-large to district number 0
    # minority pct -> float (so it can be filtered/sorted)

    district_df["district_num"] = district_df.apply(standardize_number, axis=1)

    district_df["sq_miles"] = district_df.apply(get_area, axis=1)
    district_df["state_compactness"] = district_df.apply(get_compactness, axis=1)
    district_df["Representative"] = district_df.apply(get_politicians, axis=1)

    remove_columns = [column for column in district_df.columns if "Unnamed" in column]
    district_df.drop(remove_columns, axis=1, inplace=True)
    print(district_df.head())
    district_df.to_csv("backend/created_data/data_for_backend_server/district_df.csv")


def process_states():
    states_df = pd.read_csv(
        r"backend/created_data/data_for_backend_server/state_df.csv"
    )

    state_size = {}
    state_size_file = open("backend/created_data/temp_files/stateArea.txt", "r")
    for line in state_size_file:
        p = line.strip().split()
        state_name = p[0]
        area = int(p[1])
        state_size[state_name] = area
        print(state_name, area)

    def get_area(row):
        state_abbrev = row["state_abbrev"]
        if state_abbrev in state_size:
            return state_size[state_abbrev]
        else:
            print(state_abbrev, "NOT FOUND")

    def standardize_area(row):
        phone = row["Size Miles"]
        if not pd.isna(phone) and phone != "N/A":
            new_phone = ""
            for c in phone:
                if c.isdigit():
                    new_phone += c
            return int(new_phone)
        return 0

    # add column for land area
    # todo: minority pct -> float

    # states_df.drop("size", axis=1, inplace=True)
    states_df["Size Miles"] = states_df.apply(standardize_area, axis=1)

    remove_columns = [column for column in states_df.columns if "Unnamed" in column]
    states_df.drop(remove_columns, axis=1, inplace=True)
    print(states_df.head())
    # TODO check if need do df = df.reset_index()  # make sure indexes pair with number of rows
    states_df.to_csv("backend/created_data/data_for_backend_server/state_df.csv")


def check_missing():
    states_df = pd.read_csv(r"../data_for_backend_server/state_df.csv")
    print("MISSING STATE INFO")
    state_cols = states_df.columns

    state_missing = {}

    for col in state_cols:

        def missing_states(row):
            if row["state_abbrev"] in state_missing:
                m = state_missing[row["state_abbrev"]]
            else:
                m = []
            if pd.isna(row[col]):
                m.append(col)
                state_missing[row["state_abbrev"]] = m
                return "N/A"
            return row[col]

        states_df[col] = states_df.apply(missing_states, axis=1)

    for state in state_missing:
        print(state, state_missing[state])

    remove_columns = [column for column in states_df.columns if "Unnamed" in column]
    states_df.drop(remove_columns, axis=1, inplace=True)
    print()

    district_df = pd.read_csv(r"../data_for_backend_server/district_df.csv")
    print("MISSING DISTRICT INFO")
    district_missing = {}
    district_cols = district_df.columns

    for col in district_cols:

        def missing_districts(row):
            key = (row["state_abbrev"], row["district_num"])
            if key in district_missing:
                m = district_missing[key]
            else:
                m = []
            if pd.isna(row[col]):
                if col != "candidate":
                    m.append(col)
                    district_missing[key] = m
                return "N/A"
            return row[col]

        district_df[col] = district_df.apply(missing_districts, axis=1)

    for district in district_missing:
        print(district, district_missing[district])

    remove_columns = [column for column in district_df.columns if "Unnamed" in column]
    district_df.drop(remove_columns, axis=1, inplace=True)

    rep_df = pd.read_csv(r"../data_for_backend_server/politician_df.csv")
    print("MISSING REP INFO")
    rep_cols = rep_df.columns

    rep_missing = {}
    rep_district = {}
    missing_cols = {}

    for col in rep_cols:

        def missing_reps(row):
            if row["name"] in rep_missing:
                m = rep_missing[row["name"]]
            else:
                m = []
                rep_district[row["name"]] = (row["state"], row["district"])
            if pd.isna(row[col]) or row[col] == "N/A":
                m.append(col)
                if col in missing_cols:
                    missing_cols[col] += 1
                else:
                    missing_cols[col] = 1
                rep_missing[row["name"]] = m
                return "N/A"
            return row[col]

        rep_df[col] = rep_df.apply(missing_reps, axis=1)

    for rep in rep_missing:
        print(rep_district[rep], rep_missing[rep])
    print("missing cols:")
    print(missing_cols)

    remove_columns = [column for column in rep_df.columns if "Unnamed" in column]
    rep_df.drop(remove_columns, axis=1, inplace=True)
    print()

    # states_df.to_csv('../data_for_backend_server/state_df_tmp.csv')
    # district_df.to_csv('../data_for_backend_server/district_df_tmp.csv')
    # rep_df.to_csv('../data_for_backend_server/politician_df_tmp.csv')


def curlRepData():
    return

    import requests

    url = "https://api.propublica.org/congress/v1/117/house/members.json"
    headers = {"X-API-Key": "your-key-here"}
    api_request = requests.get(url, headers=headers)
    response = json.loads(api_request.content)

    results = None
    for item in response["results"]:
        results = item
        break

    for item in results["members"]:
        api_uri = item["api_uri"]
        state = item["state"]
        if item["at_large"]:
            district = 0
        else:
            district = int(item["district"])
        fname = state + "_" + str(district) + ".json"

        rep_request = requests.get(api_uri, headers=headers)
        rep_response = json.loads(rep_request.content)
        f = open("politicians_detailed/" + fname, "w")
        json.dump(rep_response, f)
        f.close()


def age(d):
    from datetime import date

    d = d.split("-")
    birthdate = date(int(d[0]), int(d[1]), int(d[2]))
    today = date.today()
    age = (
        today.year
        - birthdate.year
        - ((today.month, today.day) < (birthdate.month, birthdate.day))
    )
    return age


def processRepData():

    json_path = "backend/downloaded_Data/politicians_detailed/"
    files = os.listdir(json_path)

    politicians_df = pd.read_csv(
        r"backend/created_data/data_for_backend_server/politician_df.csv"
    )

    have_data = {}
    print(politicians_df.shape)
    for index, row in politicians_df.iterrows():
        state = row["state"]
        district_num = row["district"]
        if state in at_large:
            district_num = 0
        if (state, district_num) in have_data:
            print("dupe:", state, district_num)
        else:
            have_data[state, district_num] = row

    politicians_df_new = pd.DataFrame(columns=politicians_df.columns)
    i = 0
    for key in have_data:
        politicians_df_new.loc[i] = pd.Series(have_data[key])
        i += 1

    print(politicians_df_new.columns)
    print(politicians_df_new.shape)
    print(politicians_df_new.head())
    politicians_df = politicians_df_new

    propublica_columns = set(
        [
            "birthdate",
            "phone_number",
            "age",
            "gender",
            "twitter_id",
            "facebook_id",
            "youtube_id",
            "website",
            "committees",
            "missed_votes_pct",
            "votes_with_party_pct",
        ]
    )
    rep_data = {}
    # get info from propublica api
    for fname in files:
        district_name = fname.split(".")[0].split("_")
        state = district_name[0]
        district_num = int(district_name[1])

        f = open(json_path + fname, "r")
        resp = json.load(f)["results"][0]
        f.close()

        data = {}
        data["propublica_name"] = resp["first_name"]
        if resp["middle_name"] != None:
            data["propublica_name"] += " " + resp["middle_name"]
        data["propublica_name"] += " " + resp["last_name"]

        data["birthdate"] = resp["date_of_birth"]
        data["age"] = age(data["birthdate"])
        data["gender"] = resp["gender"]
        data["twitter_id"] = resp["twitter_account"]
        data["facebook_id"] = resp["facebook_account"]
        data["youtube_id"] = resp["youtube_account"]
        data["website"] = resp["url"]

        current_role = None
        for item in resp["roles"]:
            if item["congress"] == "117":
                current_role = item
                break

        data["committees"] = [
            committee["name"] for committee in current_role["committees"]
        ]
        data["missed_votes_pct"] = current_role["missed_votes_pct"]
        data["votes_with_party_pct"] = current_role["votes_with_party_pct"]
        data["phone_number"] = current_role["phone"]

        district_id = (state, district_num)

        if district_id not in have_data:
            # us territories
            pass
        else:
            rep_data[district_id] = data

    f = open("backend/created_data/temp_files/districtCivicInfo.json", "r")

    # Code to import pickled data containing url's if don't want to use selenium
    import pickle

    temp_file_name = "rep_data.pkl"
    # with open(temp_file_name, 'wb') as handle:
    #     pickle.dump(rep_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(temp_file_name, "rb") as handle:
        rep_data = pickle.load(handle)

    civicdata = json.load(f)
    civic_columns = set(["photo_url"])
    photo_url_scraper = Photo_URL_Scraper()
    # get image url info from civic info api
    for key in rep_data:
        civic_key = key[0] + str(key[1])
        if civic_key not in civicdata:
            print(civic_key + " not found")
            continue
        district_rep = civicdata[civic_key]["representative"]
        rep_data[key]["civic_name"] = "none"
        if district_rep is None:
            # query names from pandas database
            name = politicians_df[
                (politicians_df["state"] == key[0])
                & (politicians_df["district"] == key[1])
            ].values[0][1]
            new_photo_url = photo_url_scraper.get_photo_url(name)
            rep_data[key]["photo_url"] = new_photo_url
        district_rep["photo_url"] = new_photo_url
        rep_data[key]["photo_url"] = district_rep["photo_url"]
        rep_data[key]["civic_name"] = district_rep["name"]

    # get additional fields from open secrets api
    open_secrets_cols = set(
        [
            "office_addr",
            "contact_site",
            "bioguide_id",
            "votesmart_id",
            "feccandid",
            "first_elected",
        ]
    )
    open_secrets = pd.read_csv("backend/created_data/temp_files/OpenSecretsData.csv")
    for index, row in open_secrets.iterrows():

        state = row["office"][0:2]
        try:

            district_num = int(row["office"][2:])
        except:
            continue
        if state in at_large:
            district_num = 0
        os_key = (state, district_num)
        if os_key not in rep_data:
            print(os_key + " not found in rep data")
            continue
        data = rep_data[os_key]
        data["office_addr"] = row["congress_office"]
        data["contact_site"] = row["webform"]
        data["bioguide_id"] = row["bioguide_id"]
        data["votesmart_id"] = row["votesmart_id"]
        data["feccandid"] = row["feccandid"]
        data["first_elected"] = row["first_elected"]
        data["os_name"] = row["firstlast"]

    new_cols = set.union(propublica_columns, civic_columns, open_secrets_cols)

    def standardize_number(row):
        state = row["state"]
        district_num = row["district"]
        if state in at_large:
            district_num = 0
        return district_num

    politicians_df["district"] = politicians_df.apply(standardize_number, axis=1)
    longest_str = {}

    for column in new_cols:

        def get_value(row):
            state = row["state"]
            district_num = row["district"]
            n = (state, district_num)
            if column == "photo_url":
                return rep_data[n][column]
            if (
                column in politicians_df.columns
                and not pd.isna(row[column])
                and row[column] != "N/A"
            ):
                if column in longest_str:
                    longest_str[column] = max(
                        len(str(row[column])), longest_str[column]
                    )
                else:
                    longest_str[column] = len(str(row[column]))
                return row[column]
            return rep_data[n][column]

        politicians_df[column] = politicians_df.apply(get_value, axis=1)

    def standardize_phone(row):
        phone = row["phone_number"]
        if not pd.isna(phone) and phone != "N/A":
            new_phone = ""
            for c in phone:
                if c.isdigit():
                    new_phone += c
            return new_phone[0:3] + "-" + new_phone[3:6] + "-" + new_phone[6:]
        return None

    politicians_df["phone_number"] = politicians_df.apply(standardize_phone, axis=1)

    def standardize_twitter(row):
        twitter = row["twitter_id"]
        if not pd.isna(twitter) and twitter[0] == "@":
            return twitter[1:]
        return twitter

    politicians_df["twitter_id"] = politicians_df.apply(standardize_twitter, axis=1)

    remove_columns = [
        column for column in politicians_df.columns if "Unnamed" in column
    ]
    politicians_df.drop(remove_columns, axis=1, inplace=True)
    print(longest_str)
    politicians_df.to_csv(
        "backend/created_data/data_for_backend_server/politician_df.csv"
    )


def calculateMOV(c, t):
    return (c - (t - c)) / ((c + (t - c)) / 2) * 100


def missingDistrictData():
    district = ("AR", 1)
    party = "REPUBLICAN"
    candidatevotes = 138757
    totalvotes = 201245
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("WY", 0)
    party = "REPUBLICAN"
    candidatevotes = 127963
    totalvotes = 201245
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("MS", 4)
    party = "REPUBLICAN"
    candidatevotes = 152633
    totalvotes = 223732
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("MT", 0)
    party = "REPUBLICAN"
    candidatevotes = 339169
    totalvotes = 601509
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("FL", 25)
    party = "REPUBLICAN"
    candidatevotes = 128672
    totalvotes = 212845
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("NC", 12)
    party = "DEMOCRAT"
    candidatevotes = 203974
    totalvotes = 279138
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("ND", 0)
    party = "REPUBLICAN"
    candidatevotes = 193568
    totalvotes = 321532
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("AK", 0)
    party = "REPUBLICAN"
    candidatevotes = 192126
    totalvotes = 353165
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("DE", 0)
    party = "DEMOCRAT"
    candidatevotes = 227353
    totalvotes = 352737
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("SD", 0)
    party = "REPUBLICAN"
    candidatevotes = 202446
    totalvotes = 335471
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)

    district = ("VT", 0)
    party = "DEMOCRAT"
    candidatevotes = 188547
    totalvotes = 272451
    mov = calculateMOV(candidatevotes, totalvotes)
    print(district, party, candidatevotes, totalvotes, mov)


def compareDiffs(name):
    print()
    df = pd.read_csv(
        "/backend/created_data/data_for_backend_server/" + name + "_df.csv"
    )
    longest_length = 0
    rows = {}
    state_name = "state" if "state" in df.columns else "state_abbrev"
    district_name = "district" if "district" in df.columns else "district_num"
    for index, row in df.iterrows():
        state = row[state_name]
        district_num = row[district_name]
        if state in at_large:
            district_num = 0
        rows[(state, district_num)] = row

    tmp_df = pd.read_csv(
        "/backend/created_data/data_for_backend_server/" + name + "_df_tmp.csv"
    )
    tmp_rows = {}
    for index, row in tmp_df.iterrows():
        state = row[state_name]
        district_num = row[district_name]
        if state in at_large:
            district_num = 0
        tmp_rows[(state, district_num)] = row
    cols = df.columns
    tmp_cols = tmp_df.columns
    columns = []
    print("checking " + name + " columns...")
    for c in cols:
        if c not in tmp_cols:
            print(c + " in " + name + " df but not in tmp")
        else:
            columns.append(c)
    for c in tmp_cols:
        if c not in cols:
            print(c + " in tmp but not in " + name + " df")
    print("checking " + name + " diffs...")
    diffs = {}
    for key in rows:
        if key in tmp_rows:
            row = rows[key]
            row_tmp = tmp_rows[key]
            row_diff = {}
            for c in columns:
                row_val = str(row[c])
                tmp_val = str(row_tmp[c])
                if row_val != tmp_val:
                    longest_length = max(longest_length, len(row_val))
                    longest_length = max(longest_length, len(tmp_val))
                    if len(row_val) > 20:
                        row_val = row_val[0:20] + "..."
                    if len(tmp_val) > 20:
                        tmp_val = tmp_val[0:20] + "..."
                    row_diff[c] = (row_val, tmp_val)
            if len(row_diff) > 0:
                diffs[key] = row_diff
            tmp_rows[key] = None
        else:
            print(str(key) + " in " + name + " df but not in tmp")
    for key in tmp_rows:
        if tmp_rows[key] != None:
            print(str(key) + " in tmp but not in " + name + " df")
    print("printing diffs for " + name)
    for key in diffs:
        print(str(key) + ":")
        for item in diffs[key]:
            print("\t" + item + ": " + str(diffs[key][item]))
    print("longest item:", longest_length)
    print()
    print()


def fixAges():
    df = pd.read_csv("/backend/created_data/data_for_backend_server/politician_df.csv")

    def fix_age(row):
        birthday = row["birthdate"]
        assert len(birthday.split("-")) == 3
        return age(birthday)

    df["age"] = df.apply(fix_age, axis=1)
    remove_columns = [column for column in df.columns if "Unnamed" in column]
    df.drop(remove_columns, axis=1, inplace=True)
    df.to_csv("/backend/created_data/data_for_backend_server/politician_df.csv")


# compareDiffs('district')
# compareDiffs("politician")
# process_districts()
# process_states()

# check_missing()
# processRepData()
# missingDistrictData()
fixAges()
