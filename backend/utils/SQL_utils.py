import csv
from models import *


def convert_csv_to_list(relative_path):
    with open(relative_path) as f:
        reader = csv.reader(f)
        data = list(reader)
    return data


def convert_letter_to_party(letter):
    if letter == "R":
        return "REPUBLICAN"
    elif letter == "D":
        return "DEMOCRAT"
    else:
        return "THIRD PARTY"


def write_Politician_to_mySQL_table(session, data, debug=True):
    # note: add this line to models.py, after models, in order to
    # create the tables in the db, if necessary
    # db.create_all()

    rows = data[1::]
    if debug:
        rows = rows[0:2]
    session.query(Politician).delete()
    for row in rows:
        politician_instance = Politician(
            name=row[1],
            state_abbrev=row[2],
            district_num=int(row[3]),
            party_detailed=convert_letter_to_party(row[4]),
            age=int(float(row[5]))
            if row[5] != "" and row[5] != "N/A" and row[5] != ""
            else None,  # TODO: might need to make unknown values a Null value instead of 0
            years_in_office=int(row[6])
            if row[6] != "N/A" and row[6] != ""
            else None,  # TODO: might need to make unknown values a Null value instead of 0,
            phone_number=row[7],
            total_cash=int(float(row[8]))
            if row[8] != "N/A" and row[8] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            twitter_id=row[
                9
            ],  # TODO: might need to make unknown values a Null value instead of 0
            committees=row[10],
            first_elected=row[11],
            missed_votes_pct=float(row[12])
            if row[12] != "N/A" and row[12] != ""
            else 0,
            facebook_id=row[13],
            bioguide_id=row[14],
            contact_site=row[15],
            youtube_id=row[16],
            votes_with_party_pct=float(row[17])
            if row[17] != "N/A" and row[17] != ""
            else 0,
            votesmart_id=row[18],
            birthdate=row[19],
            website=row[20],
            photo_url=row[21],
            gender=row[22],
            feccandid=row[23],
            office_addr=row[24],
        )
        exists = session.query(
            Politician.query.filter_by(
                state_abbrev=row[2], district_num=int(row[3])
            ).exists()
        ).scalar()
        if exists:
            continue
        if not exists:
            session.add(politician_instance)
            session.commit()
    return "Completed Adding Politicians to Politician Table\n"


def write_District_to_mySQL_table(session, data, debug=True):
    rows = data[1::]
    if debug:
        rows = rows[0:2]
    session.query(District).delete()  # TODO: check if line actually does anything
    for row in rows:
        district_instance = District(
            state_abbrev=row[1],
            district_num=int(row[2]),
            total_pop=int(float(row[3]))
            if row[3] != "" and row[3] != "N/A" and row[3]
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            white_pop=int(float(row[4]))
            if row[4] != "" and row[4] != "N/A"
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            hispanic_pop=int(float(row[5]))
            if row[5] != "" and row[5] != "N/A"
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            asian_pop=int(float(row[6]))
            if row[6] != "" and row[6] != "N/A"
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            black_pop=int(float(row[7]))
            if row[7] != "" and row[7] != "N/A"
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            male_pop=int(float(row[8]))
            if row[8] != "" and row[8] != "N/A"
            else 0,  # TODO: might need to make unknown values a Null value instead of 0,
            female_pop=int(float(row[9]))
            if row[9] != "" and row[9] != "N/A"
            else 0,  # TODO: might need to make unknown values a Null value instead of 0,
            non_white_percentage=float(row[10][:-1])
            if row[10] != "" and row[10] != "N/A"
            else 0,
            district_compactness=float(row[11])
            if row[11] != "N/A" and row[11] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0,
            representative=row[17],
            party_detailed=row[13],
            candidatevotes=int(float(row[14]))
            if row[14] != "N/A" and row[14] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            totalvotes=int(float(row[15]))
            if row[15] != "N/A" and row[15] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            MOV=float(row[16])
            if row[16] != "N/A" and row[16] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            sq_miles=int(row[18]),
        )
        exists = session.query(
            session.query(District)
            .filter_by(state_abbrev=row[1], district_num=int(row[2]))
            .exists()
        ).scalar()
        if exists:
            continue
        if not exists:
            session.add(district_instance)
            session.flush()
    session.commit()
    return "Completed Adding Districts to District Table\n"


def write_State_to_mySQL_table(session, data, debug=True):
    rows = data[1::]
    if debug:
        rows = rows[0:2]
    session.query(State).delete()
    for row in rows:
        state_instance = State(
            state_abbrev=row[1],
            total_pop=int(float(row[2])),
            white_pop=int(float(row[3])),
            hispanic_pop=int(float(row[4]))
            if row[4] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            asian_pop=int(float(row[5]))
            if row[5] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            black_pop=int(float(row[6]))
            if row[6] != ""
            else 0,  # TODO: might need to make unknown values a Null value instead of 0
            male_pop=int(float(row[7])),
            female_pop=int(float(row[8])),
            non_white_percentage=float(row[9][:-1])
            if row[9] != "" and row[9] != "N/A"
            else 0,
            state_compactness=float(row[10]),
            state_name=row[11],
            party_detailed=row[12],
            candidatevotes=int(float(row[13])),
            totalvotes=int(float(row[14])),
            MOV=float(row[15]),
            sq_miles=int(row[16]),
            census_region=row[17],
            princeton_report=row[18],
        )
        exists = session.query(State).filter_by(state_abbrev=row[1]).first() is not None
        if exists:
            continue
        if not exists:
            session.add(state_instance)
            session.commit()
    session.commit()
    return "Completed Adding States to State Table\n"


def write_data_to_sql_database():
    district_data = convert_csv_to_list(
        "created_data/data_for_backend_server/district_df.csv"
    )
    politician_data = convert_csv_to_list(
        "created_data/data_for_backend_server/politician_df.csv"
    )
    state_data = convert_csv_to_list(
        "created_data/data_for_backend_server/state_df.csv"
    )

    status = ""
    status += write_Politician_to_mySQL_table(db.session, politician_data, debug=False)
    status += write_State_to_mySQL_table(db.session, state_data, debug=False)
    status += write_District_to_mySQL_table(db.session, district_data, debug=False)
    return status
