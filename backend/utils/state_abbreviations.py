state_to_abbrev = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New Hampshire": "NH",
    "New Jersey": "NJ",
    "New Mexico": "NM",
    "New York": "NY",
    "North Carolina": "NC",
    "North Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode Island": "RI",
    "South Carolina": "SC",
    "South Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
}

# "District of Columbia": "DC",
# "Guam": "GU",
# "Puerto Rico": "PR",
# "American Samoa": "AS",
# "Northern Mariana Islands": "MP",
# "U.S. Virgin Islands": "VI",
# "United States Minor Outlying Islands": "UM",

at_large_states = set(["ND", "AK", "WY", "DE", "SD", "VT", "MT"])


"""
Checks if a string is a substring of a state name and fetch its abbreviation if it is
"""


def get_state_abbrev(state_name):
    lowercase = state_name.lower()
    for item in state_to_abbrev:
        if lowercase in item.lower():
            return state_to_abbrev[item]
    return None


"""
Checks if a string is a valid state abbreviation
"""


def is_state_abbrev(state_abbrev):
    if len(state_abbrev) != 2:
        return False
    capitalized = state_abbrev.upper()
    for state in state_to_abbrev:
        if state_to_abbrev[state] == capitalized:
            return True
    return False
