import mysql.connector

# from CommonUtils import *
from sqlalchemy import *
import pymysql
import pandas as pd
import csv

# from server import * # TODO check if issue here
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# TODO: find way so that global variables is not necessary
Base = declarative_base()
from sqlalchemy import inspect
from models import *


# BIG TODO: PARSING DOES NOT WORK CORRECTLY.


class SQL_Database:
    def __init__(self):
        self.engine, self.db_connection = self.connect_to_sql()
        self.base = Base
        Session = sessionmaker(self.engine)
        self.session = Session()

    # Basic Command To Create MYSQL DataBase
    def create_database(self, database_name):
        db = mysql.connector.connect(host="db", user="root", passwd="root")
        mycursor = db.cursor()
        mycursor.execute("CREATE DATABASE %s" % database_name)
        mycursor.execute("SHOW DATABASES")
        for x in mycursor:
            print(x)

    def write_df_to_mySQL_table(self, df, table_name):
        try:
            df.to_sql(table_name, self.db_connection, if_exists="fail")
        except Exception as e:
            print("FAILED creating table: %s" % table_name)
            print(e)

    """
    @return: connection to sql engine
    """

    def connect_to_sql(self):
        # sql_engine = create_engine("mysql+pymysql:{0}:{1}@localhost/{2}".format("root", "GerryMap926!", "GerryMap"))
        url = "mysql+pymysql://root:root@db:3306/flask"
        sql_engine = create_engine(url)
        # sql_engine = create_engine("mysql+pymysql:///{0}:{1}@localhost/{2}".format("root", "GerryMap926!", "GerryMap"))
        db_connection = sql_engine.connect()
        return sql_engine, db_connection

    def get_state_table_name(self):
        state_table_2 = inspect(State).local_table
        return str(state_table_2)

    def get_state_columns(self):
        state_table_2 = inspect(State).local_table.columns
        columns = []
        for column in state_table_2:
            columns += [column.name]
        return columns

    def get_district_columns(self):
        state_table_2 = inspect(District).local_table.columns
        columns = []
        for column in state_table_2:
            columns += [column.name]
        return columns

    def get_politician_columns(self):
        state_table_2 = inspect(Politician).local_table.columns
        columns = []
        for column in state_table_2:
            columns += [column.name]
        return columns

    def delete_state_table(self):
        self.base.metadata.drop_all(bind=self.engine, tables=[State.__tablename__])

    def delete_District_table(self):
        self.base.metadata.drop_all(bind=self.engine, tables=[District.__tablename__])

    def delete_Politician_table(self):
        self.base.metadata.drop_all(bind=self.engine, tables=[Politician.__tablename__])

    def convert_letter_to_party(self, letter):
        if letter == "R":
            return "REPUBLICAN"
        elif letter == "D":
            return "DEMOCRAT"
        else:
            return "THIRD PARTY"

    def write_Politician_to_mySQL_table(
        self, data, debug=True, delete_prev_politiciantable=False
    ):
        rows = data[1::]
        if debug:
            rows = rows[0:2]
        if delete_prev_politiciantable:
            self.delete_Politician_table()
        Session = sessionmaker(self.engine)
        with Session() as session:
            session.query(Politician).delete()

            for row in rows:
                politician_instance = Politician(
                    name_=row[1],
                    state_abbrev_=row[2],
                    district_num_=int(row[3]),
                    party_detailed_=self.convert_letter_to_party(row[4]),
                    age_=int(float(row[5]))
                    if row[5] != "" and row[5] != "N/A" and row[5] != ""
                    else None,  # TODO: might need to make unknown values a Null value instead of 0
                    years_in_office_=int(row[6])
                    if row[6] != "N/A" and row[6] != ""
                    else None,  # TODO: might need to make unknown values a Null value instead of 0,
                    phone_number_=row[7],
                    total_cash_=int(float(row[8]))
                    if row[8] != "N/A" and row[8] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    twitter_id_=row[
                        9
                    ],  # TODO: might need to make unknown values a Null value instead of 0
                    committees_=row[10],
                    first_elected_=row[11],
                    missed_votes_pct_=float(row[12])
                    if row[12] != "N/A" and row[12] != ""
                    else 0,
                    facebook_id_=row[13],
                    bioguide_id_=row[14],
                    contact_site_=row[15],
                    youtube_id_=row[16],
                    votes_with_party_pct_=float(row[17])
                    if row[17] != "N/A" and row[17] != ""
                    else 0,
                    votesmart_id_=row[18],
                    birthdate_=row[19],
                    website_=row[20],
                    photo_url_=row[21],
                    gender_=row[22],
                    feccandid_=row[23],
                    office_addr_=row[24],
                )

                exists = session.query(
                    session.query(Politician)
                    .filter_by(state_abbrev_=row[2], district_num_=int(row[3]))
                    .exists()
                ).scalar()
                # return str(exists)
                # return politician_instance.__repr__()
                if exists:
                    continue
                if not exists:
                    session.add(politician_instance)
                    session.flush()
            session.commit()
            return "Completed Adding Politicians to Politician Table\n"

    def write_District_to_mySQL_table(
        self, data, debug=True, delete_prev_district_table=False
    ):
        rows = data[1::]
        if debug:
            rows = rows[0:2]
        if delete_prev_district_table:
            self.delete_District_table()
        Session = sessionmaker(self.engine)
        with Session() as session:
            session.query(
                District
            ).delete()  # TODO: check if line actually does anything
            for row in rows:
                district_instance = District(
                    state_abbrev_=row[1],
                    district_num_=int(row[2]),
                    total_pop_=int(float(row[3]))
                    if row[3] != "" and row[3] != "N/A" and row[3]
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    white_pop_=int(float(row[4]))
                    if row[4] != "" and row[4] != "N/A"
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    hispanic_pop_=int(float(row[5]))
                    if row[5] != "" and row[5] != "N/A"
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    asian_pop_=int(float(row[6]))
                    if row[6] != "" and row[6] != "N/A"
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    black_pop_=int(float(row[7]))
                    if row[7] != "" and row[7] != "N/A"
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    male_pop_=int(float(row[8]))
                    if row[8] != "" and row[8] != "N/A"
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0,
                    female_pop_=int(float(row[9]))
                    if row[9] != "" and row[9] != "N/A"
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0,
                    non_white_percentage_=float(row[10][:-1])
                    if row[10] != "" and row[10] != "N/A"
                    else 0,
                    district_compactness_=float(row[11])
                    if row[11] != "N/A" and row[11] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0,
                    representative_=row[17],
                    party_detailed_=row[13],
                    candidatevotes_=int(float(row[14]))
                    if row[14] != "N/A" and row[14] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    totalvotes_=int(float(row[15]))
                    if row[15] != "N/A" and row[15] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    MOV_=float(row[16])
                    if row[16] != "N/A" and row[16] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    sq_miles_=int(row[18]),
                )
                exists = session.query(
                    session.query(District)
                    .filter_by(state_abbrev_=row[1], district_num_=int(row[2]))
                    .exists()
                ).scalar()
                # return str(exists)
                # return district_instance.__repr__()
                if exists:
                    continue
                if not exists:
                    session.add(district_instance)
                    session.flush()
            session.commit()
            return "Completed Adding Districts to District Table\n"

    def write_State_to_mySQL_table(
        self, data, debug=True, delete_prev_state_table=False
    ):
        columns = data[0]

        rows = data[1::]
        if debug:
            rows = rows[0:2]
        if delete_prev_state_table:
            self.delete_state_table()
        Session = sessionmaker(self.engine)
        with Session() as session:
            # session.query(State).delete() # TODO: check if line actually does anything
            for row in rows:
                state_instance = State(
                    state_abbrev_=row[1],
                    total_pop_=int(float(row[2])),
                    white_pop_=int(float(row[3])),
                    hispanic_pop_=int(float(row[4]))
                    if row[4] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    asian_pop_=int(float(row[5]))
                    if row[5] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    black_pop_=int(float(row[6]))
                    if row[6] != ""
                    else 0,  # TODO: might need to make unknown values a Null value instead of 0
                    male_pop_=int(float(row[7])),
                    female_pop_=int(float(row[8])),
                    non_white_percentage_=float(row[9][:-1])
                    if row[9] != "" and row[9] != "N/A"
                    else 0,
                    state_compactness_=float(row[10]),
                    state_name_=row[11],
                    party_detailed_=row[12],
                    candidatevotes_=int(float(row[13])),
                    totalvotes_=int(float(row[14])),
                    MOV_=float(row[15]),
                    sq_miles_=int(row[16]),
                    census_region_=row[17],
                )
                # exists = session.query(State.state_name_)
                # exists = session.query(State.state_id_).filter_by(party_detailed_='REPUBLICAN').first() is not None
                exists = (
                    session.query(State).filter_by(state_abbrev_=row[1]).first()
                    is not None
                )
                # return str(exists)
                # return state_instance.__repr__()
                # exists = session.query(State.state_name_).filter_by(state_name_=row[11]).first() is not None
                # return self.get_state_columns()
                if exists:
                    continue
                # state_instance = State(*row)
                if not exists:
                    session.add(state_instance)
                    session.commit()
                    # return "BUG on below command" #TODO: what if issue is that table or State already has been defined with fewer columns and is not being proprly deleted?
                    # session.flush()
            session.commit()
            return "Completed Adding States to State Table\n"

    def get_table_description(self):
        table_description = ""
        for t in self.base.metadata.sorted_tables:
            table_description += t.name + "\n"
        return table_description

    def create_all_tables_in_metadata(self):
        # metadata_obj = MetaData()
        # district_table = Table(
        #     'state',
        #     metadata_obj,
        #     Column('state_id',Integer, primary_key=True),
        #     Column('state_abbrev', String),
        #     Column('white_pop', Integer),
        #     Column('hispanic_pop', Integer),
        #     Column('asian_pop', Integer),
        #     Column('black_pop', Integer),
        #     Column('male_pop', Integer),
        #     Column('female_pop', Integer),
        #     Column('non_white_percentage', Float),
        #     Column('state_compactness', Integer),
        #     Column('state', String)
        # )
        # metadata_obj.create_all(self.engine)
        self.base.metadata.create_all(self.engine)
        # return district_table

    def write_data_to_sql_database(self):
        district_data = self.convert_csv_to_list(
            "created_data/data_for_backend_server/district_df.csv"
        )
        politician_data = self.convert_csv_to_list(
            "created_data/data_for_backend_server/politician_df.csv"
        )
        state_data = self.convert_csv_to_list(
            "created_data/data_for_backend_server/state_df.csv"
        )

        self.create_all_tables_in_metadata()

        status = ""
        status += self.write_State_to_mySQL_table(state_data, debug=False)
        status += self.write_District_to_mySQL_table(
            district_data, debug=False, delete_prev_district_table=False
        )
        status += self.write_Politician_to_mySQL_table(politician_data, debug=False)
        # status+= self.get_table_description()

        # Old Obsolete Way of Adding to Database
        # district_df = pd.read_csv("created_data/data_for_backend_server/district_df.csv")
        # self.write_df_to_mySQL_table(district_df, "districts", db_connection)
        # politician_df = pd.read_csv("created_data/data_for_backend_server/politician_df.csv")
        # self.write_df_to_mySQL_table(politician_df, "politicians", db_connection)
        # state_db = pd.read_csv("created_data/data_for_backend_server/state_df.csv")
        # self.write_df_to_mySQL_table(state_db, "states", db_connection)
        return status

    def import_df(self, relative_path):
        df = pd.read_csv(relative_path)
        return df

    def convert_csv_to_list(self, relative_path):
        with open(relative_path) as f:
            reader = csv.reader(f)
            data = list(reader)
        return data


# SQL_Database_Instance = SQL_Database()
# df = SQL_Database_Instance.import_df('backend/created_data/data_for_backend_server/district_df.csv')
# SQL_Database_Instance.write_data_to_sql_database()
