from urllib import response

from numpy import compare_chararrays
from flask import Flask, jsonify, request, Response
from models import *  # these are just hard-coded placeholders for now
from flask_cors import CORS
from dotenv import load_dotenv
import os
import json
from sqlalchemy.orm.exc import NoResultFound
from search import *
import requests as r

# GLOBALS

app = Flask(__name__)
CORS(app)
app.debug = True

load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("SQLALCHEMY_DATABASE_URI")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "JSON_SORT_KEYS"
] = False  # See https://flask-marshmallow.readthedocs.io/en/latest/
NUM_PER_PAGE = 9

db.init_app(app)

# END GLOBALS


@app.route("/", methods=["GET"])
def home():
    return "Welcome to the GerryMap api!"


""" 
    Returns all states in the database upon call.
    @param page: Returns states (@TODO: sorted by abbrev) on a per-page basis.
        Page size can be seen in NUM_PER_PAGE global variable.
"""

from sqlalchemy import and_, or_
from sqlalchemy import asc, desc


"""
    This function takes in a list of attributes, 
    a model, and a query, and filters that query by
    that model's given attributes. Assumes that 
    the filter list is composed of actual
    class variables.
"""


def filter_keywords(query, model, filter_list):
    for attribute in filter_list:
        input = request.args.get(attribute)
        if attribute.lower() == "state":
            attribute = "state_abbrev"
        if input:
            input = input.upper()
            query = query.filter(getattr(model, attribute) == input)
    return query


"""
    This function takes in a list of attributes 
    and filters a query. Assumes that the filter 
    list is composed of strings of actual
    class variables, and the string is 
    formatted as "min-max".
"""


def filter_range(query, model, filter_list):
    for attribute in filter_list:
        input = request.args.get(attribute)
        if input:
            minimum, maximum = input.split("-")
            query = query.filter(getattr(model, attribute) >= minimum)
            query = query.filter(getattr(model, attribute) <= maximum)
    return query


"""
    This function takes in a model and a query 
    and sorts that query by an attribute. 
"""


def sort_keywords(query, model, check_party):
    sort = request.args.get("sort")
    if sort:
        order_method = asc
        ascending = request.args.get("ascending")
        if ascending:
            if ascending.lower() != "true":
                order_method = desc
        try:
            query = query.order_by(order_method(getattr(model, sort)))
            if check_party:
                if sort == "party_detailed":
                    query = query.order_by(
                        order_method(getattr(model, "party_detailed")),
                        order_method(getattr(model, "MOV")),
                    )
        except AttributeError as e:
            return error_response("Invalid sort specified: " + sort)
    return query


@app.route("/states", methods=["GET"])
def get_states():
    query = State.query

    keywords = request.args.get("query")
    if keywords != None and len(keywords) > 0:
        query = search_states(keywords.split(" "), query)

    query = filter_keywords(query, State, ["party_detailed"])

    query = filter_range(
        query,
        State,
        ["total_pop", "sq_miles", "non_white_percentage", "state_compactness"],
    )

    # End "min-max" queries.

    # assumption is only sorting by one thing
    query = sort_keywords(query, State, check_party=True)

    pagination = parse_pagination(request, "page", "page_size")
    if type(pagination) == Response:
        # returned an error response
        return pagination
    page_num, page_size = pagination

    states_schema = StateSchema(many=True)
    resp = execute_paginated_query(query, states_schema, page_num, page_size)
    return jsonify(resp)


"""
    Given a state's abbreviation, return the state's 
    information in the database.
"""


@app.route("/state/<state_abbrev_>", methods=["GET"])
def get_state(state_abbrev_):
    try:
        result = State.query.filter(State.state_abbrev == state_abbrev_).one()
    except NoResultFound:
        return error_response("Invalid state abbreviation specified: " + state_abbrev_)

    state_schema = StateSchema()
    return jsonify(state_schema.dump(result))


"""
    Returns all districts in the database upon call.
    @param page: Returns districts (@TODO: sorted by ??) on a per-page basis.
        Page size can be seen in NUM_PER_PAGE global variable.
"""


@app.route("/districts", methods=["GET"])
def get_districts():

    pagination = parse_pagination(request, "page", "page_size")
    if type(pagination) == Response:
        # returned an error response
        return pagination
    page_num, page_size = pagination

    query = District.query

    keywords = request.args.get("query")
    if keywords != None and len(keywords) > 0:
        query = search_districts(keywords.split(" "), query)

    # These queries filter by keywords.
    query = filter_keywords(query, District, ["state", "party_detailed"])

    # The following queries are in the format "minimum-maximum".
    query = filter_range(
        query,
        District,
        ["total_pop", "sq_miles", "non_white_percentage", "district_compactness"],
    )

    # Sort depending on arguments.
    query = sort_keywords(query, District, False)

    districts_schema = DistrictSchema(many=True)
    resp = execute_paginated_query(query, districts_schema, page_num, page_size)
    return jsonify(resp)


"""
    Given a district's state abbreviation and district number, return the district's 
    information in the database.
"""


@app.route("/district/<state_district>", methods=["GET"])
def get_district(state_district):
    try:
        parts = state_district.split("_")
        if len(parts) != 2:
            return error_response(
                "Please specify a district using the format <state_abbrev>_<district_number>."
            )
        state_abbrev = parts[0].upper()
        district_num = int(parts[1])
        result = District.query.filter(
            District.state_abbrev == state_abbrev, District.district_num == district_num
        ).one()
        district_schema = DistrictSchema()
        return jsonify(district_schema.dump(result))
    except:
        return error_response(
            "District #" + str(district_num) + " in " + state_abbrev + " not found"
        )


"""
    Returns all politicians in the database upon call.
    @param page: Returns politicians (@TODO: sorted by ??) on a per-page basis.
        Page size can be seen in NUM_PER_PAGE global variable.
"""


@app.route("/politicians", methods=["GET"])
def get_politicians():
    pagination = parse_pagination(request, "page", "page_size")
    if type(pagination) == Response:
        # returned an error response
        return pagination
    page_num, page_size = pagination

    query = Politician.query

    keywords = request.args.get("query")
    if keywords != None and len(keywords) > 0:
        query = search_politicians(keywords.split(" "), query)

    # filter by state
    query = filter_keywords(query, Politician, ["state", "party_detailed", "gender"])

    query = filter_range(
        query,
        Politician,
        ["age", "years_in_office", "missed_votes_pct", "votes_with_party_pct"],
    )

    query = sort_keywords(query, Politician, False)

    politicians_schema = PoliticianSchema(many=True)
    resp = execute_paginated_query(query, politicians_schema, page_num, page_size)
    return jsonify(resp)


"""
    Given a politician's ID (TODO: change), return the politician's 
    information in the database.
"""


@app.route("/politician/<state_district>", methods=["GET"])
def get_politician(state_district):
    try:
        parts = state_district.split("_")
        if len(parts) != 2:
            return error_response(
                "Please specify a district using the format <state_abbrev>_<district_number>."
            )
        state_abbrev = parts[0].upper()
        district_num = int(parts[1])
        result = Politician.query.filter(
            Politician.state_abbrev == state_abbrev,
            Politician.district_num == district_num,
        ).one()
        politician_schema = PoliticianSchema()
        return jsonify(politician_schema.dump(result))
    except NoResultFound:
        return error_response(
            "No politician found for district #"
            + str(district_num)
            + " in "
            + state_abbrev
        )


@app.route("/search", methods=["GET"])
def search():
    keywords = request.args.get("query")

    state_pagination = parse_pagination(request, "state_page", "state_page_size")
    if type(state_pagination) == Response:
        # returned an error response
        return state_pagination
    state_page_num, state_page_size = state_pagination

    district_pagination = parse_pagination(
        request, "district_page", "district_page_size"
    )
    if type(district_pagination) == Response:
        # returned an error response
        return district_pagination
    district_page_num, district_page_size = district_pagination

    rep_pagination = parse_pagination(
        request, "politician_page", "politician_page_size"
    )
    if type(rep_pagination) == Response:
        # returned an error response
        return rep_pagination
    rep_page_num, rep_page_size = rep_pagination

    state_schema = StateSchema(many=True)
    district_schema = DistrictSchema(many=True)
    politician_schema = PoliticianSchema(many=True)

    if keywords == None or len(keywords) == 0:
        return jsonify(
            {
                "states": execute_paginated_query(
                    State.query, state_schema, state_page_num, state_page_size
                ),
                "districts": execute_paginated_query(
                    District.query,
                    district_schema,
                    district_page_num,
                    district_page_size,
                ),
                "politicians": execute_paginated_query(
                    Politician.query, politician_schema, rep_page_num, rep_page_size
                ),
            }
        )

    keywords = keywords.split(" ")

    result = {
        "states": execute_paginated_query(
            search_states(keywords), state_schema, state_page_num, state_page_size
        ),
        "districts": execute_paginated_query(
            search_districts(keywords),
            district_schema,
            district_page_num,
            district_page_size,
        ),
        "politicians": execute_paginated_query(
            search_politicians(keywords), politician_schema, rep_page_num, rep_page_size
        ),
    }

    return jsonify(result)


@app.route("/address", methods=["GET"])
def search_address():
    from utils.state_abbreviations import at_large_states

    address = request.args.get("address")
    if address is None:
        return error_response("Please specify an address.")

    google_api_key = os.getenv("GOOGLE_CIVIC_API_KEY")
    rep_api_url = (
        "https://www.googleapis.com/civicinfo/v2/representatives?address="
        + str(address)
    )
    params = {"key": google_api_key}
    res = r.get(rep_api_url, params=params)

    if res.status_code == 200:
        # parsing response to extract state abbrev and district number
        resp_dict = res.json()
        try:
            state_abbrev = resp_dict["normalizedInput"]["state"]
            if state_abbrev in at_large_states:
                district_num = 0
            else:
                district_office_str = (
                    "ocd-division/country:us/state:" + state_abbrev.lower() + "/cd:"
                )
                district_office_names = list(
                    filter(
                        lambda division: district_office_str in division,
                        resp_dict["divisions"],
                    )
                )
                district_num = int(district_office_names[0][len(district_office_str) :])
        except:
            return error_response("Failed to parse rep data from google civic data API")
    else:
        return error_response("Failed to fetch rep data from google civic data API")

    # fetch the politician from the db using state and district number
    try:
        politician_obj = Politician.query.filter(
            Politician.state_abbrev == state_abbrev,
            Politician.district_num == district_num,
        ).one()
    except NoResultFound:
        return error_response(
            "No results found for district #"
            + str(district_num)
            + " in "
            + str(state_abbrev)
        )
    politician_schema = PoliticianSchema()

    # fetch the state from the db
    state_schema = StateSchema()
    state = State.query.filter(State.state_abbrev == state_abbrev).one()

    # fetch the district from the db
    district_schema = DistrictSchema()
    district = District.query.filter(
        District.state_abbrev == state_abbrev, District.district_num == district_num
    ).one()

    return jsonify(
        {
            "state": state_schema.dump(state),
            "politician": politician_schema.dump(politician_obj),
            "district": district_schema.dump(district),
        }
    )


"""
Return a 404 JSON response with the given error message
"""


def error_response(msg):
    resp = Response(json.dumps({"error": msg}), mimetype="application/json")
    resp.status_code = 404
    return resp


"""
Parses an int from the request. Threw this into a helper since this logic is reused a lot
"""


def parse_int(request, param_name, default, check_valid=lambda x: True):
    item = request.args.get(param_name)
    if item is not None:
        try:
            item_num = int(item)
            if not check_valid(item_num):
                raise Exception
        except:
            return None
    else:
        item_num = default
    return item_num


"""
Parses page number and page size parameteres from the request.
Returns a 404 Response object if failure, or a (page_num, page_size) tuple if successful.
"""


def parse_pagination(request, page_param_name, pagesize_param_name):
    page_num = parse_int(
        request, page_param_name, default=1, check_valid=lambda x: x >= 1
    )
    if page_num is None:
        return error_response(
            "Invalid page number specified: " + str(request.args.get("page"))
        )

    page_size = parse_int(
        request, pagesize_param_name, default=NUM_PER_PAGE, check_valid=lambda x: x >= 1
    )
    if page_size is None:
        return error_response(
            "Invalid page size specified: " + str(request.args.get("page_size"))
        )

    return page_num, page_size


"""
Executes a given query using the given page_num and page_size parameters and dumps the result using the given schema.
Returns the data along with the pagination metadata.
"""


def execute_paginated_query(query, schema, page_num=1, page_size=NUM_PER_PAGE):
    total = query.count()
    start_ind = (page_num - 1) * page_size

    result = query.limit(page_size).offset(start_ind)
    data = schema.dump(result)

    return {
        "data": data,
        "meta": {
            "total_items": total,
            "page_num": page_num,
            "page_size": page_size,
            "first_index": 0 if len(data) == 0 else start_ind + 1,  # 1-indexing
            "last_index": 0
            if len(data) == 0
            else start_ind + min(page_size, len(data)),  # 1-indexing, inclusive
        },
    }


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
