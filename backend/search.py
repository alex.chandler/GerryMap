# methods for searching each type of model
from models import *  # these are just hard-coded placeholders for now
from utils.state_abbreviations import get_state_abbrev, is_state_abbrev

"""
Search the respective table for entries that match the given keywords.
@param keywords: list of keyword strings
@param query: a query object to build off of. If none specified, creates a new one.

@return: a query object with the filters applied for the given keyword
"""


def search_states(keywords, query=None):
    if query == None:
        query = State.query

    search_queries = []
    for keyword in keywords:
        if is_state_abbrev(keyword):
            # if keyword is a state abbreviation, don't use it to search other fields
            search_queries.append(State.state_abbrev == keyword.upper())
        else:
            # search for state name only if len >= 4 (shortest state name is Ohio)
            if len(keyword) >= 4:
                search_queries.append(State.state_name.ilike("%" + keyword + "%"))

            # search by party and census region
            search_queries.append(State.party_detailed.ilike("%" + keyword + "%"))
            search_queries.append(State.census_region.ilike("%" + keyword + "%"))

    # return no results if no valid queries were made
    if len(search_queries) == 0:
        return query.filter(False)

    return query.filter(or_(*search_queries))


def search_districts(keywords, query=None):
    if query == None:
        query = District.query

    search_queries = []
    for keyword in keywords:

        # check match for district number
        try:
            int_key = int(keyword)
            search_queries.append(District.district_num == int_key)
            continue
        except ValueError:
            pass

        if is_state_abbrev(keyword):
            # if keyword is a state abbreviation, don't use it to search other fields
            search_queries.append(District.state_abbrev == keyword.upper())
        else:
            # check matches for state
            state_abbrev = get_state_abbrev(keyword)
            if state_abbrev is not None:
                # convert state name keywords to abbreviations, since state names are not in table
                search_queries.append(District.state_abbrev == state_abbrev)
            elif len(keyword) == 2:
                search_queries.append(District.state_abbrev.ilike("%" + keyword + "%"))

            # search by representative name or party
            search_queries.append(District.representative.ilike("%" + keyword + "%"))
            search_queries.append(District.party_detailed.ilike("%" + keyword + "%"))

    # return no results if no valid queries were made
    if len(search_queries) == 0:
        return query.filter(False)

    return query.filter(or_(*search_queries))


def search_politicians(keywords, query=None):
    if query == None:
        query = Politician.query

    search_queries = []
    for keyword in keywords:

        # check match for district number
        try:
            int_key = int(keyword)
            search_queries.append(Politician.district_num == int_key)
            continue
        except ValueError:
            pass

        # check matches for state
        if is_state_abbrev(keyword):
            # if keyword is a state abbreviation, don't use it to search other fields
            search_queries.append(Politician.state_abbrev == keyword.upper())
        else:
            state_abbrev = get_state_abbrev(keyword)
            if state_abbrev is not None:
                # convert state name keywords to abbreviations, since state names are not in table
                search_queries.append(Politician.state_abbrev == state_abbrev)
            elif len(keyword) == 2:
                search_queries.append(
                    Politician.state_abbrev.ilike("%" + keyword + "%")
                )

            # search by representative name or party
            search_queries.append(Politician.name.ilike("%" + keyword + "%"))
            search_queries.append(Politician.party_detailed.ilike("%" + keyword + "%"))

    # return no results if no valid queries were made
    if len(search_queries) == 0:
        return query.filter(False)

    return query.filter(or_(*search_queries))
