# run the app frontend in a docker container
docker:
	docker run --rm -i -t -p 3000:3000 -v $(PWD)/frontend:/frontend fanghelen/gerrymap

docker-backend:
	docker run --rm -i -t -p 5000:5000 -v $(PWD)/backend:/backend fanghelen/gerrymap-backend-dev

clean:
	rm -f  *.tmp
	rm -rf __pycache__

format:
	cd frontend && yarn prettier --write .
	cd backend && black .

eslint:
	cd frontend && yarn eslint *