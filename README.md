# GerryMap

1. Canvas / Discord group number : 10AM Group 10

2. Names of the team members: Alex Chandler (alc5729), Ting-Yin Changchien (tc34374),
   Helen Fang (hyf72), Joann Feng (jlf3774), Akshay Sharma (aas5772)

   GitLab usernames: @alex.chandler, @tingyincc, @silvercat5922, @joannfeng, @akshay-a-sharma

3. Name of the project: GerryMap (https://www.gerrymap.com/)

4. URL of the GitLab repo: https://gitlab.com/alex.chandler/GerryMap

5. GitLab Pipelines: https://gitlab.com/alex.chandler/GerryMap/-/pipelines/

6. Phase Leaders:

   a. Phase 1 Leader: Alex Chandler, who provided inspiration for the direction of the project.

   b. Phase 2 Leader: Helen Fang, who worked on getting a lot of the moving parts deployed.

   c. Phase 3 Leader: Ting-Yin Changchien, who worked on various use cases in the frontend for searching, filtering, and sorting.

   d. Phase 4 Leader: Akshay Sharma, who worked on refactoring the backend to make it much cleaner.

7. The proposed project
   **A website containing information about congressional districts, as well as identify, quantify, and visualize gerrymandering.** This project aims to consolidate demographic and political data for each district, as well as quantify gerrymandering based on the shape of each congressional district.

8. URLs of at least three disparate data sources that you will programmatically scrape using a RESTful API (be very sure about this)

- [Google’s Civic Information API](https://developers.google.com/civic-information)

- [Census Data API](https://www.census.gov/data/developers/data-sets/decennial-census.html)

- [Open Secrets API](https://www.opensecrets.org/open-data/api)

- [Twitter API](https://developer.twitter.com/en/docs/twitter-api/getting-started/about-twitter-api)

- [ProPublica Congress API](https://projects.propublica.org/api-docs/congress-api/members/)

8.  Create our own RESTful API using Postman (https://documenter.getpostman.com/view/19780402/UVkqsvRV)

9.  Git SHA: f7b192ec10ecc5032846bf784945e98996c2904a

10. Estimated Completion Time:

    Phase 1:
    Estimated 15 hours for all group members.

         Actual Completion Time:

         Helen Fang: 20 hours

         Alex Chandler: 20 hours

         Akshay Sharma: 17 hours

         Ting-Yin Changchien: 18 hours

         Joann Feng: 15 hours

    Phase 2:
    Estimated 20 hours for all group members.

    Actual Completion Time:

         Helen Fang: 45 hours

         Alex Chandler: 30 hours

         Akshay Sharma: 20 hours

         Ting-Yin Changchien: 30 hours

         Joann Feng: 23 hours

    Phase 3:
    Estimated 15 hours for all group members.

    Actual Completion Time:

         Helen Fang: 15 hours

         Alex Chandler: 15 hours

         Akshay Sharma: 15 hours

         Ting-Yin Changchien: 22 hours

         Joann Feng: 10 hours

    Phase 4:
    Estimated 10 hours for all group members.

    Actual Completion Time:

         Helen Fang: 15 hours

         Alex Chandler: 10 hours

         Akshay Sharma: 10 hours

         Ting-Yin Changchien: 12 hours

         Joann Feng: 10 hours

11. Demo Youtube Link: https://www.youtube.com/watch?v=gvnVAzqODg0

## Models

### States (50 instances)

Filterable/sortable attributes:

1. Political Leaning/ (Rep. or Dem.)
2. Population
3. Size (Physical)
4. Minority/Non-White
5. Average compactness of district

Searchable Attributes:

1. Name
2. Representatives
3. Region (of the US)
4. Reverse search of state from city/address/zip code
5. State abbreviation

Model Connections

- Include list of districts within the state
- include list of representatives from this state

Rich media

1. District map
2. Images of politicians from the state
3. Charts of demographic/socioeconomic data

### Congressional districts (435 instances)

Filterable/Sortable Attributes:

1. Political leaning/ (Rep. or Dem.)
2. Population
3. Size (Physical)
4. Minority/Non-White
5. Compactness Measurement

Searchable Attributes:

1. Name
2. State
3. Political representative
4. District number
5. Reverse search district by address/location/zip code

Model Connections

- Link to page for the state state it belongs to
- Link to page dedicated to district representative

Rich media

1. District map
2. Image of representative from the state
3. Charts of demographic/socioeconomic data

### Politicians (435 instances)

Filterable/Sortable Attributes:

1. Political Party
2. Age
3. Gender
4. Number of years in office (seniority)
5. Missed Votes Percentage
6. Voting with Party Percent

Searchable Attributes:

1. Name
2. State
3. Congressional District
4. Find representative by address/location/zip code
5. Committees

Model Connections

- Link to the state represented by this politician
- Link to the district represented by this politician

Rich Media

1. Image of politician
2. Embedded Facebook or Twitter feed
3. Chart of statistics (missed votes percentage, votes with party percentage…)

## What three questions will you answer due to doing this data synthesis on your site?

- For [x] congressional district or [x] state, what are some valuable things to know?
- How prevalent is gerrymandering in the US?
- What states have the most/least amount of gerrymandering? Is gerrymandering more common in blue or red states?

## Comments

Setting up a relational database with MySQL, SQLAlchemy, and Flask was very time consuming.
Figuring out how to run selenium in the GitLab CI was also kind of difficult.
